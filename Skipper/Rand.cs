﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper
{
    class Rand
    {
        private Random random;
        public static Rand Instance { get { return instance; } private set { } }
        private static Rand instance = new Rand();

        public Rand()
        {
            random = new Random((int)System.DateTime.Now.Ticks);
        }

        public int NextInt(int max)
        {
            return this.random.Next(max);
        }

        public int NextInt(int min, int max)
        {
            return this.random.Next(min, max);
        }

        public int Roll(int num, int sides)
        {
            int sum = 0;
            for (int i = 0; i < num; i++)
            {
                sum += this.NextInt(1, sides);
            }
            return sum;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="percent">percentage weighting to be true</param>
        /// <returns></returns>
        internal bool NextBool(double percent)
        {
            double d = NextDouble();
            d *= 100;
            return percent >= d;
        }

        public double NextDouble()
        {
            return this.random.NextDouble();
        }
    }
}
