﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;
using System.Xml.Linq;

namespace Skipper
{
    class AbstractCollectible : IPersistant
    {
        public const string X_NAME = "name";
        public const string X_AMOUNT = "amount";
        public const string X_VALUE = "value";
        public const string X_TYPE = "type";

        private int value = 0;
        public int Value { get { return value; } protected set { this.value = value; } }

        private string name;
        public string Name { get { return name; } }

        private int amount;
        public int Amount { get { return amount; } }

        public AbstractCollectible(XElement node)
        {
            this.name = node.Attribute(X_NAME).Value;
            this.value = int.Parse(node.Attribute(X_VALUE).Value);
            this.amount = int.Parse(node.Attribute(X_AMOUNT).Value);
        }

        public AbstractCollectible(string name, int amount)
        {
            this.name = name;
            this.amount = amount;
        }

        public void AddAmount(int amount)
        {
            this.amount += amount;
        }

        public void AddAmount(AbstractCollectible ac)
        {
            if (ac.Name == this.Name)
            {
                this.amount += ac.Amount;
            }
        }

        public void Serialize(ref System.Xml.Linq.XElement node)
        {
            node.Add(new XAttribute(X_NAME, name));
            node.Add(new XAttribute(X_AMOUNT, amount));
            node.Add(new XAttribute(X_VALUE, value));
            node.Add(new XAttribute(X_TYPE, this.GetType().Namespace + "." + this.GetType().Name));
        }
    }
}
