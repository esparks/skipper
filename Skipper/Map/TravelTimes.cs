﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Map
{
    class TravelTimes
    {
        public static double AU_TO_KILOMETERS = 1.496e+8;
        public static double ONE_GRAVITY_MS = 9.81;

        public static double GetConstantThrustTravelTimeAU(double au, double thrust, double initialVelocity)
        {
            double kilometers = au * AU_TO_KILOMETERS;
            return GetConstantThrustTravelTimeKM(kilometers, thrust, initialVelocity);
        }

        public static double GetConstantThrustTravelTimeKM(double kilometers, double thrust, double initialVelocity)
        {
            double distance = kilometers * 1000;
            double a = thrust * ONE_GRAVITY_MS;
            double b = initialVelocity;
            double c = -distance;
            double d = (b * b) * 2 - 4 * a * c;
            
            if (d < 0) return -1;

            if (d == 0)
            {
                return (-b + Math.Sqrt(d))/(2*a);
            } else {
                // return the positive value - what if both are positive????
                double res1 = (-b + Math.Sqrt(d))/(2*a);
                double res2 = (-b + Math.Sqrt(d))/(2*a);
                if (res1 > 0) return res1;
                return res2;
            }
                
        }

    }
}
