﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Skipper.Items
{
    class ItemFactory //CollectableFactory
    {
        public static AbstractCollectible Instantiate(XElement item)
        {
            string typename = item.Attribute(AbstractCollectible.X_TYPE).Value;
            Type t = Type.GetType(typename);
            System.Reflection.ConstructorInfo ctor = t.GetConstructor(new Type[] { typeof(XElement) });
            object o = ctor.Invoke(new object[] { item });
            AbstractCollectible ac = o as AbstractCollectible;
            return ac;
        }
    }
}
