﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Items
{
    class AbstractItem : AbstractCollectible
    {
        public AbstractItem(string name, int amount)
            : base(name, amount)
        {

        }
    }
}
