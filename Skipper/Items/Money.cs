﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Items
{
    class Money : AbstractItem
    {
        public const string DollarSign = "Cr.";

        public Money(int amount)
            : base("Credits", amount)
        {
            this.Value = 1;
        }

    }
}
