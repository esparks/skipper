﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using Skipper.Creatures.Crew;

namespace Skipper.Encounters
{
    class SpaceStationBarEncounter : AbstractEncounter, IHireEncounter
    {
        private static string[] barNames = new string[] {"The peacock inn", "The Old Fashioned", "Hops and Rocks", "H2Overdrive"};
        private static string[] barTypes = new string[] {"dive", "diner", "lounge"};
        
        private string type;

        // generate some crew to hire:
        private IList<AbstractCreature> hires;

        // drink, hire crew,
        // other random events- bar fight!
        // etc....
        public SpaceStationBarEncounter(AbstractEncounter parent)
            : base(parent)
        {
            // generate a bar name
            this.Name = parent.Name + " >> " + barNames[Rand.Instance.NextInt(barNames.Count())];
            this.type = barTypes[Rand.Instance.NextInt(barTypes.Count())];
            this.Description = "You are sitting in the space station bar. Customers huddle over their drinks at the tables around you.";

            GenerateAvailableHires();
        }

        private void GenerateAvailableHires()
        {
            int numcrew = Rand.Instance.NextInt(2, 4);
            hires = new List<AbstractCreature>();
            for (int j = 0; j < numcrew; j++)
            {
                hires.Add(WorldHub.Instance.CrewFactory.GenerateCrewMember(Rand.Instance.NextInt(1, 4)));
            }
            RefreshOptions();
        }

        private void RefreshOptions()
        {
            this.ClearOptions();
            this.AddOption("Order a drink.", OrderDrink);
            foreach (AbstractCreature ac in hires)
            {
                string name = ac.Name;
                this.AddOption("Talk to " + name, (out string text) => { return AttemptHire(name, out text); });
            }
            this.AddOption("Leave", Leave);
        }

        private AbstractEncounter Leave(out string text)
        {
            text = "You leave the fug and din of the bar behind and return to your ship.";
            return parent;
        }


        private AbstractEncounter OrderDrink(out string text)
        {
            text = "You sit down with a beer and wait. People come and go.";
            GenerateAvailableHires();
            return this;
        }

        private AbstractEncounter AttemptHire(string name, out string text)
        {
            AbstractCreature crew = null;
            foreach(AbstractCreature c in hires)
            {
                if (c.Name == name)
                {                    
                    text = "You strike up a conversation with " + name;
                    return new ViewCrewStatsEncounter(this, (CrewMember)c);
                }
            }
            text = "Error: couldn't find " + name + " to talk to.";
            return this;
        }

        public bool Embark(Ship ship, CrewMember crew)
        {
            if (hires.Contains(crew))
            {
                hires.Remove(crew);
                this.RefreshOptions();
                return true;
            }
            return false;
        }

        public void Disembark(Ship ship, CrewMember crew)
        {
            hires.Add(crew);
            this.RefreshOptions();
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            //TODO:
        }
    }
}
