﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cards;
using Skipper.Data;
using System.Xml.Linq;

namespace Skipper.Encounters
{
    delegate AbstractEncounter EncounterFunction(out string text);

    abstract class AbstractEncounter : IPersistant
    {
        protected AbstractEncounter parent;
        private string name;
        private string description;
        private string[] options;
        private IDictionary<char, EncounterFunction> optionCalls;
        private IDictionary<char, string> optionMap;
        private IDictionary<string, char> presetKeys;
        private IDictionary<string, EncounterFunction> presetFunctions;
        private IDictionary<string, EncounterFunction> optionTable;

        public string Name { get { return name; } protected set { name = value; } }
        public string Description { get { return description; } protected set { description = value; } }
        public string[] Options { get { SetOptions(); return options; } private set { } }

        private ActiveCardsPool activePool;
        public ActiveCardsPool ActiveCards { get { return activePool; } }

        public AbstractEncounter(string name, string description, AbstractEncounter parent)
        {
            this.name = name;
            this.description = description;
            this.parent = parent;
            this.activePool = new ActiveCardsPool();
            optionTable = new Dictionary<string, EncounterFunction>();
            optionMap = new Dictionary<char, string>();
            presetKeys = new Dictionary<string, char>();
            presetFunctions = new Dictionary<string, EncounterFunction>();
        }

        public AbstractEncounter(AbstractEncounter parent)
            : this("UNSET NAME", "UNSET DESC", parent)
        {
        }

        public AbstractEncounter()
            : this("UNSET NAME", "UNSET DESC", null)
        {
        }

        public AbstractEncounter(XElement node, AbstractEncounter parent)
            : this(node.Attribute("name").Value, node.Attribute("description").Value, parent)
        {

        }

        public AbstractEncounter Update(char key, out string text)
        {
            EncounterFunction ef = null;
            if (optionCalls.TryGetValue(key, out ef))
            {
                return ef(out text);
            }

            text = "";
            return this;
        }

        // auto options thingy
        public bool ValidOption(char c)
        {
            return optionCalls.ContainsKey(c);
        }

        public int OptionPosition(char c)
        {
            string opt;
            if (optionMap.TryGetValue(c, out opt))
            {
                for (int i = 0; i < options.Length; i++)
                {
                    if (options[i] == opt)
                        return i;
                }
            }
            return -1;
        }

        protected void ClearOptions()
        {
            optionTable.Clear();
            presetKeys.Clear();
            presetFunctions.Clear();
        }

        protected void AddOption(char key, string option, EncounterFunction function)
        {
            presetFunctions.Add(option, function);
            presetKeys.Add(option, key);
        }

        protected void AddOption(string option, EncounterFunction function)
        {
            optionTable.Add(option, function);
        }

        private void SetOptions()
        {
            IList<string> olist = optionTable.Keys.ToList();
            options = IListToOptions(optionTable).ToArray();
        }

        public virtual void NotifyIncompleteChild(AbstractEncounter child)
        {
            throw new NotImplementedException("Attempted to notify an uninterested parent.");
        }

        protected virtual AbstractEncounter NotImplemented(out string text)
        {
            text = "Not Implemented!";
            return this;
        }

        private IList<string> IListToOptions(IDictionary<string, EncounterFunction> opts)
        {
            IList<string> output = new List<string>();
            ISet<char> used = new SortedSet<char>();
            optionCalls = new Dictionary<char, EncounterFunction>();
            optionMap.Clear();
            //cheapo way
            int idx = 1;
            foreach (string s in opts.Keys)
            {
                char input = ("" + idx)[0];
                string construct = idx + ": " + s;
                output.Add(construct);
                optionCalls.Add(input, opts[s]);
                optionMap.Add(input, construct);
                idx++;
            }

            foreach (string s in presetKeys.Keys)
            {
                string construct = presetKeys[s] + ": " + s;
                optionCalls.Add(presetKeys[s], presetFunctions[s]);
                optionMap.Add(presetKeys[s], construct);
                output.Add(construct);
            }

            return output;
        }

        public AbstractEncounter End()
        {
            return parent;
        }

        public void Serialize(ref XElement node)
        {
            // do my stuff and then
            node.SetAttributeValue("name", name);
            node.SetAttributeValue("description", description);
            node.Add(new XAttribute("type", this.GetType().Namespace + "." + this.GetType().Name));
            SerializeImpl(ref node);
        }

        protected abstract void SerializeImpl(ref XElement node);
    }
}
