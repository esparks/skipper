﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cargo;
using System.Xml.Linq;
using Skipper.Items;

namespace Skipper.Encounters
{
    class CargoEncounter : AbstractEncounter
    {

        private IList<AbstractCargo> cargo;

        public CargoEncounter(XElement node, AbstractEncounter parent)
            : base(node, parent)
        {
            cargo = new List<AbstractCargo>();
            foreach (XElement cargoElement in node.Elements("cargo"))
            {
                // should do away with this cargo etc.
                cargo.Add(ItemFactory.Instantiate(cargoElement) as AbstractCargo);
            }
        }

        public CargoEncounter(string name, string description, IList<AbstractCargo> cargo, AbstractEncounter parent)
            :base(name, description, parent)
        {
            this.cargo = cargo;
            this.AddOption("Take everything", TakeAll);
        }

        private AbstractEncounter TakeAll(out string text)
        {
            text = "You gather up all the artefacts and leave in a hurry.";
            foreach(AbstractCargo ac in cargo)
                WorldHub.Instance.PlayerInfo.Ship.AddCargo(ac);

            return parent;
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            foreach (AbstractCargo c in cargo)
            {
                XElement cargoElement = new XElement("cargo");
                c.Serialize(ref cargoElement);
                node.Add(cargoElement);
            }
        }
    }
}
