﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters
{
    class BasicEncounter : AbstractEncounter
    {
        public BasicEncounter(System.Xml.Linq.XElement node, AbstractEncounter parent)
            : base(node, parent)
        {
            throw new Exception("Do Not Save Basic Encounters??!");
        }

        private string continueText;
        public BasicEncounter(string name, string description, string continueText, AbstractEncounter parent)
            : base(name, description, parent)
        {
            this.continueText = continueText;
            this.ClearOptions();
            this.AddOption("Continue", Continue);
        }

        private AbstractEncounter Continue(out string text)
        {
            text = continueText;
            return parent;
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            // ignore
        }
    }
}
