﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures.Crew;

namespace Skipper.Encounters
{
    class ViewCrewStatsEncounter : AbstractEncounter
    {
        // one of these should let you hire and fire, maybe a seperate view for that?
        // this should have a new view that lets you navigate but this will do for now.

        private CrewMember member;

        public ViewCrewStatsEncounter(AbstractEncounter parent, CrewMember crew)
            : base(parent)
        {
            member = crew;
            this.Name = "STATS > " + crew.Name;
            string desc = "";
            string pad = "    ";

            /// remove /// unused ///
            desc += pad + "Name: " + crew.Name + "\nAge: " + crew.Age + " Occupation: " + crew.Class + '\n';            
            desc += pad + "Level: " + crew.Level + '\n';
            desc += pad + "HP : " + crew.GetStat(Stats.HP) + '\n';
            desc += pad + "STR: " + crew.GetStat(Stats.STRENGTH) + '\n';
            desc += pad + "DEX: " + crew.GetStat(Stats.DEXTERITY) + '\n';
            desc += pad + "AIM: " + crew.GetStat(Stats.AIM) + '\n';
            desc += pad + "INT: " + crew.GetStat(Stats.INTELLIGENCE) + '\n';
            desc += pad + "WLP: " + crew.GetStat(Stats.WILLPOWER) + '\n';
            desc += pad + "CHA: " + crew.GetStat(Stats.CHARISMA) + '\n';

            this.Description = desc;
            if (parent is IHireEncounter && !WorldHub.Instance.PlayerInfo.Ship.Crew.Contains(member))
                this.AddOption("Hire", HireMe);
            
            this.AddOption("Exit", (out string text) => { text = "";  return parent; });
        }

        private AbstractEncounter HireMe(out string text)
        {
            IHireEncounter hire = (IHireEncounter)parent;
            Ship ship = WorldHub.Instance.PlayerInfo.Ship;
            if (ship.AddCrew(member))
            {
                text = member.Name + " joins your crew.";
                hire.Embark(ship, member);
                return parent;
            }
            else
            {
                text = "You do not have enough room onboard.";
                return this;
            }
        }

        public CrewMember GetMember()
        {
            return member;
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            // ignore
        }
    }
}
