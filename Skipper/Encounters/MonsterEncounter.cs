﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using System.Xml.Linq;

namespace Skipper.Encounters
{
    class MonsterEncounter : AbstractEncounter
    {
        private IList<AbstractCreature> monsters;
        //public AbstractCreature Monsters { get { return monster; } }
        private EncounterFunction next;

        public MonsterEncounter(XElement node, AbstractEncounter parent)
            : base(node, parent)
        {
            monsters = new List<AbstractCreature>();
            //
        }


        public MonsterEncounter(IList<AbstractCreature> monsters, AbstractEncounter parent)
            : base("overwritethis", "overwritethis", parent)
        {
            this.monsters = monsters;
            this.Name = "Combat encounter!";
            this.Description = "Suddenly you encounter a rampaging " + monsters.First().Name + "!";
            this.AddOption("Attack", Attack);
            this.AddOption("Flee", Flee);
        }

        private AbstractEncounter Attack(out string text)
        {
            text = "";
            return new PersonalCombatEncounter(monsters, this.parent);
        }
        
        private AbstractEncounter Flee(out string text)
        {
            text = "You attempt to flee the fight!";
            int coin = Rand.Instance.NextInt(2);
            if (coin > 0)
            {
                text += " You cannot escape!";
                return this;
            }
            text += " You manage to escape the beast.";
            parent.NotifyIncompleteChild(this);
            return parent;
        }

        // may be a better way, could spawn a treasure encounter with my parent as its parent.
        // effectively removing myself from the chain.
        public void FinishCombat()
        {
            this.Description = "All monsters are dead.";
            this.ClearOptions();
            this.AddOption("Search bodies", Search);
            this.AddOption("leave", Leave);
        }

        private AbstractEncounter Search(out string text)
        {
            text = "You gather up all the goodies (not really)";
            return parent;
        }

        private AbstractEncounter Leave(out string text)
        {
            text = "";
            return parent;
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            //__TODO__//
            
            // serialize list of monsters
            // etc.
        }
    }
}
