﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Skipper.Encounters
{
    class EncounterFactory
    {

        public static AbstractEncounter InstantiateEncounter(XElement encounter, AbstractEncounter parent)
        {
            string typename = encounter.Attribute("type").Value;
            if (typename != "none")
            {
                Console.WriteLine("Deserializing encounter type " + typename);
                Type t = Type.GetType(typename);
                System.Reflection.ConstructorInfo constructor = t.GetConstructor(new Type[] { typeof(XElement), typeof(AbstractEncounter) });
                if (constructor == null)
                {
                    throw new Exception("Cannot find deserializing constructor for type " + typename);
                }
                object o = constructor.Invoke(new object[] { encounter, parent });
                AbstractEncounter ae = o as AbstractEncounter;
                return ae;
            }
            else
            {
                return null;
            }
        }

    }
}
