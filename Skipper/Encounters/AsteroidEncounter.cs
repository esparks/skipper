﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cargo;
using System.Xml.Linq;

namespace Skipper.Encounters
{
    class AsteroidEncounter : AbstractEncounter
    {
        private IList<AbstractCargo> materials;

        public AsteroidEncounter(XElement node, AbstractEncounter parent)
            : base(node, parent)
        {
            materials = new List<AbstractCargo>();
            this.Name = node.Attribute("name").Value;
            this.Description = node.Attribute("description").Value;
            foreach (XElement mat in node.Elements("material"))
            {
                this.materials.Add(new AbstractCargo(mat));
            }
        }

        public AsteroidEncounter(string name, string description, AbstractEncounter parent)
            : base(name, description, parent)
        {
            MakeDefaultOptions();
            GenerateMaterials();
        }

        private void GenerateMaterials()
        {
            materials = new List<AbstractCargo>();
            // make more procedural!
            int amount = Rand.Instance.NextInt(3, 6);
            amount *= 10;
            while (amount > 0)
            {
                int chunk = Rand.Instance.NextInt(1, amount);
                int coin = Rand.Instance.NextInt(100);
                AbstractCargo ac;
                if (coin < 15)
                    ac = new GoldOre(chunk);
                else
                    ac = new IronOre(chunk);

                materials.Add(ac);
                amount -= chunk;
            }
        }

        private void MakeDefaultOptions()
        {
            this.ClearOptions();
            this.AddOption("Mine", Mine);
            this.AddOption("Continue onwards", Continue);
        }

        private AbstractEncounter Mine(out string text)
        {
            // chance for pirates etc. while mining
            if (materials.Count > 0)
            {
                int coin = Rand.Instance.NextInt(100);
                if (coin < 25)
                {
                    text = "Pirates attack while you are mining!";
                    return new BasicEncounter("pirates in asteroid field", "fill it out", "you win, ok.", this);
                }
                else
                {
                    int mat = Rand.Instance.NextInt(materials.Count);
                    AbstractCargo cargo = materials.ElementAt(mat);
                    materials.RemoveAt(mat);
                    WorldHub.Instance.PlayerInfo.Ship.AddCargo(cargo);
                    text = "You extract a pocket of " + cargo.Name.ToLower();
                    return this;
                }
            }
            else
            {
                text = "Your scanners do not detect any more useful resources here.";
                return this;
            }
        }

        private AbstractEncounter Continue(out string text)
        {
            text = "You turn away and soon the asteroid field is far behind you.";
            return parent;
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            foreach(AbstractCargo ac in materials)
            {
                XElement item = new XElement("material");
                ac.Serialize(ref item);
                node.Add(item);
            }
        }

    }
}
