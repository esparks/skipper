﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters
{
    class SpaceCombatEncounter : AbstractEncounter
    {
        //TODO: make real drone!
        public struct Drone
        {
            public char icon;
            public int x;
            public int y;
            public long id;
            public static long idcount;
        }
        
        private IList<Drone> drones;

        public SpaceCombatEncounter(AbstractEncounter parent)
        : base(parent)
        {
            this.Name = "Space Combat";
            this.Description = "Space Combat Encounter";
            drones = new List<Drone>();
            // create some drones!
            for (int i = 0; i < 15; i++)
            {
                bool evil = Rand.Instance.NextBool(50);
                Drone d = new Drone();
                if (evil)
                {
                    d.icon = '<';
                    d.x = Rand.Instance.NextInt(35) + 35;
                }
                else
                {
                    d.icon = '>';
                    d.x = Rand.Instance.NextInt(40);
                }

                d.id = Drone.idcount++;
                d.y = Rand.Instance.NextInt(14);
                drones.Add(d);
            }
        }

        // TODO: create containers to hold enemies, functions to update etc.
        
        // genericise this out to all enemies, seperate drone drawing from class
        public IList<Drone> GetDrones()
        {
            return drones;
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            // TODO: Serialize to allow saving in battle
        }

    }
}
