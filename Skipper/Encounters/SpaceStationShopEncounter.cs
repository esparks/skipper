﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Skipper.Encounters
{
    class SpaceStationShopEncounter : AbstractEncounter
    {

        public SpaceStationShopEncounter(XElement node, AbstractEncounter parent)
            : base(node, parent)
        {
            //
            this.Name = "Stores";
            this.Description = "You are wandering around the central market";
            GenerateOptions();
        }

        public SpaceStationShopEncounter(AbstractEncounter parent)
            : base(parent)
        {
            this.Name = "Stores";
            this.Description = "You are wandering around the central market";
            GenerateOptions();
        }

        private void GenerateOptions()
        {
            this.AddOption("Leave", (out string text) => { text = "You return to the station hub."; return parent; });
        }

        protected override void SerializeImpl(ref XElement node)
        {
            //
        }
    }
}
