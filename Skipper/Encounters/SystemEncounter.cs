﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;
using System.Xml.Linq;

namespace Skipper.Encounters
{
    class SystemEncounter : AbstractEncounter
    {
        private IDictionary<string, EncounterFunction> opts;
        private int numPlanets;
        private IList<AbstractEncounter> planets;

        public SystemEncounter(XElement node, AbstractEncounter parent)
            : base(node, parent)
        {
            planets = new List<AbstractEncounter>();
            int i= 0;
            foreach (XElement planet in node.Elements("planet"))
            {
                int index = i;
                PlanetEncounter pe = new PlanetEncounter(planet, this);
                PlanetData pd = pe.Data;
                this.AddOption("Land on the " + pd.GetDescription() + " planet", (out string text) => { return ExplorePlanet(index, out text); });
                planets.Add(pe);
                i++;
            }
        }

        public SystemEncounter(string name, string description, AbstractEncounter parent)
            : base(name, description, parent)
        {
            planets = new List<AbstractEncounter>();
            // generate a bunch of planets.
            numPlanets = Rand.Instance.NextInt(3 + 1);
            IList<PlanetData> planTemp = new List<PlanetData>();
            for (int i = 0; i < numPlanets; i++)
            {
                PlanetData pd;
                bool check;
                // regenerate until no match with existing planets
                do {
                    check = false;
                    pd = PlanetData.NewPlanet();
                    foreach(PlanetData ex in planTemp)
                    {
                        if (ex.GetDescription() == pd.GetDescription())
                        {
                            check = true;
                        }
                    }
                } while (check == true);
                planTemp.Add(pd);
            }

            for (int i = 0; i < planTemp.Count; i++)
            {
                PlanetData pd = planTemp.ElementAt(i);
                planets.Add(new PlanetEncounter(pd, this));
                int index = i;
                this.AddOption("Land on the " + pd.GetDescription() + " planet", (out string text) => { return ExplorePlanet(index, out text); });
            }

            if (numPlanets > 1)
                this.Description = this.Description + "\nThere are several planets here to explore.";
            else if (numPlanets > 0)
                this.Description = this.Description + "\nThere is one planet to explore.";
            else
                this.Description = this.Description + "\nThe system has no planets.";

            this.AddOption("Continue", Continue);
        }

        private AbstractEncounter ExplorePlanet(int index, out string text)
        {
            text = "You dive down towards the surface of planet " + planets.ElementAt(index).Name;
            return planets.ElementAt(index);
        }

        private AbstractEncounter Continue(out string text)
        {
            text = "You leave the system.";
            return parent;
        }

        protected override void SerializeImpl(ref XElement node)
        {
            foreach (AbstractEncounter p in planets)
            {
                XElement pnode = new XElement("planet");
                pnode.SetAttributeValue("name", p.Name);
                p.Serialize(ref pnode);
                node.Add(pnode);
                //todo serialize p
                //p.serialize(ref pnode);
            }
        }
    }
}
