﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures.Crew;

namespace Skipper.Encounters
{
    interface IHireEncounter
    {
        bool Embark(Ship ship, CrewMember crew); // joins your crew
        void Disembark(Ship ship, CrewMember crew); // leaves your cre
    }
}
