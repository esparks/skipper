﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Skipper.Encounters
{
    class SpaceStationEncounter : AbstractEncounter
    {
        private bool docked;
        private AbstractEncounter bar;
        private AbstractEncounter shop;

        public SpaceStationEncounter(XElement node, AbstractEncounter parent)
            : base(node, parent)
        {
            this.docked = bool.Parse(node.Attribute("docked").Value);

        }

        public SpaceStationEncounter()
            : base()
        {
            this.docked = false;
            this.Name = GenerateSSName();
            this.Description = SetDescription();
            this.bar = new SpaceStationBarEncounter(this);
            this.shop = new SpaceStationShopEncounter(this);
            GenerateOptions();
        }

        // might be derelict, or not..
        public SpaceStationEncounter(AbstractEncounter parent)
            : base (parent)
        {
            this.docked = false;
            this.Name = GenerateSSName();
            this.Description = SetDescription();
            this.bar = new SpaceStationBarEncounter(this);
            GenerateOptions();
        }

        private string SetDescription()
        {
            string text = "UNSET";
            if (!docked)
            {
                text = "You are floating a small distance from a small outpost station. From this distance the only sign of activity is ";
                text += "the docking bay lights winking on and off.";
            }
            else
            {
                text = "You are docked in one of the space-station's bays. It's noisy down here as it's half landing-bay, half-workshop.";
            }
            return text;
        }

        private void GenerateOptions()
        {
            this.ClearOptions();
            if (docked)
            {
                this.AddOption("Visit Bar", (out string text) => { text = "You head off to the station's only bar."; return bar; });
                this.AddOption("Visit Stores", (out string text) => { text = "You pay a visit to the local merchants."; return shop; });
                this.AddOption("Undock", Undock);                
            }
            else
            {
                this.AddOption("Dock", Dock);
                this.AddOption("Depart", Depart);
            }

        }

        // move to a SpaceStation DATA object, like planets..
        private string GenerateSSName()
        {
            // pick some things from dict
            return "ISS Sarsparilla";
        }

        private AbstractEncounter Dock(out string text)
        {
            text = "You are docked...";
            docked = true;
            this.Description = SetDescription();
            GenerateOptions();
            return this;
        }

        private AbstractEncounter Undock(out string text)
        {
            text = "you undock from the station.";
            docked = false;
            GenerateOptions();
            this.Description = SetDescription();
            return this;
        }

        private AbstractEncounter Depart(out string text)
        {
            text = "You continue onwards.";
            return parent;
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            // TODO: Serialize name, desc, bar encounter etc.
            node.SetAttributeValue("station_name", Name);
            node.SetAttributeValue("docked", docked.ToString());
            node.SetAttributeValue("description", Description);
            XElement barNode = new XElement("bar");
            bar.Serialize(ref barNode);
        }

    }
}
