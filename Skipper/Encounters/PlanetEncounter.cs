﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;
using Skipper.Cargo;
using Skipper.Creatures;
using System.Xml.Linq;

namespace Skipper.Encounters
{
    class PlanetEncounter : AbstractEncounter
    {
        private string type;
        private Stack<AbstractEncounter> encounters;
        private PlanetData data;
        public PlanetData Data { get { return data; } private set { data = value; } }

        public PlanetEncounter(XElement node, AbstractEncounter parent)
            : base(node, parent)
        {
            encounters = new Stack<AbstractEncounter>();
            XElement pdata = node.Element("data");
            data = new PlanetData(pdata);
            this.Name = data.GetName();
            this.Description = data.GetDescription();
            foreach (XElement enc in node.Elements("encounter"))
            {
                encounters.Push(EncounterFactory.InstantiateEncounter(enc, this));
            }
        }

        public PlanetEncounter(PlanetData data, AbstractEncounter parent)
            : base(parent)
        {
            this.data = data;
            this.Name = "Planet " + data.GetName();
            this.Description = "You have landed on the surface of the " + data.GetDescription() + " planet.";

            encounters = GenerateEncounters();
            this.AddOption("Explore", Explore);
            this.AddOption("Launch", Launch);
        }

        private Stack<AbstractEncounter> GenerateEncounters()
        {
            Stack<AbstractEncounter> enc = new Stack<AbstractEncounter>();
            int num = Rand.Instance.NextInt(1, 4);
            for (int i = 0; i < num; i++)
            {
                // treasure or monster?
                int coin = Rand.Instance.NextInt(2);
                switch (coin)
                {
                    case 0:
                        //enc.Push(new MonsterEncounter(new Wildebeeste(), this));
                        // make a list of 2 - 3 insectoids
                        IList<AbstractCreature> monsters = new List<AbstractCreature>() { CreatureFactory.SpawnInsectoid(Rand.Instance.NextInt(1, 3)), CreatureFactory.SpawnWildebeeste(Rand.Instance.NextInt(1,3)) };
                        enc.Push(new MonsterEncounter(monsters, this));
                        break;
                    case 1:
                        IList<AbstractCargo> cargo = new List<AbstractCargo> { new AlienArtifacts(Rand.Instance.NextInt(5, 25)) };
                        enc.Push(new CargoEncounter("Cache", "You find a cache of artefacts", cargo, this));
                        break;
                    default:
                        break;
                }
            }
            return enc;
        }

        private AbstractEncounter Launch(out string text)
        {
            text = "You leave the planet.";
            return parent;
        }

        private AbstractEncounter Explore(out string text)
        {
            text = "You leave your ship to explore the planet.";
            AbstractEncounter ae = null;
            if (encounters.Count > 0)
                ae = encounters.Pop();
            if (ae == null)
            {
                text += " You find nothing of interest however.";
                return this;
            }
            return ae;
        }

        public override void NotifyIncompleteChild(AbstractEncounter child)
        {
            // in this case we shuffle it back in.
            IList<AbstractEncounter> rand = new List<AbstractEncounter>();
            rand.Add(child);
            while (encounters.Count > 0)
            {
                rand.Add(encounters.Pop());
            }
            while (rand.Count > 0)
            {
                int sel = Rand.Instance.NextInt(rand.Count);
                encounters.Push(rand.ElementAt(sel));
                rand.RemoveAt(sel);
            }
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            //serialize planetdata
            XElement pdata = new XElement("data");
            data.Serialize(ref pdata);
            node.Add(pdata);
            foreach (AbstractEncounter encounter in encounters.ToList())
            {
                XElement xenc = new XElement("encounter");
                encounter.Serialize(ref xenc);
                node.Add(xenc);
            }
        }

    }
}
