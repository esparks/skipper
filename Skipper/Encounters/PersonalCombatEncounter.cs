﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using Skipper.Data;

namespace Skipper.Encounters
{
    class PersonalCombatEncounter : AbstractEncounter
    {
        private IList<AbstractCreature> enemies;
        private IList<AbstractCreature> dead;
        private Queue<AbstractCreature> attackQueue;
        public AbstractCreature[] Enemies { get { return enemies.ToArray(); } }

        private Queue<TurnEvent> combatEvents;

        private bool roundStarted = false;
        private bool roundFinished = false;
        private bool triggerEndOfRoundActions = false;
        //private int selection;

        public PersonalCombatEncounter(IList<AbstractCreature> enemies, AbstractEncounter parent)
            : base(parent)
        {
            this.Name = parent.Name + " >> " + "COMBAT";
            /// don't use a desc, use a special view //
            this.Description = "USE THE RIGHT VIEW";
            combatEvents = new Queue<TurnEvent>();
            this.enemies = enemies;
            dead = new List<AbstractCreature>();
            attackQueue = new Queue<AbstractCreature>();
        }

        //TODO: only remove when all queue processed..//no combat results..
        //
        public void UpdateOpponents()
        {
            IList<AbstractCreature> clean = new List<AbstractCreature>();
            foreach (AbstractCreature c in enemies)
            {
                if (c.GetStat(Stats.HP) > 0)
                {
                    clean.Add(c);
                }
                else
                {
                    dead.Add(c);
                    // stick a message on the combat results HEAD
                    Queue<TurnEvent> nl = new Queue<TurnEvent>();
                    nl.Enqueue(new TurnMessageEvent("tttt", c.Name + " is killed."));
                    while(combatEvents.Count() > 0)
                    {
                        nl.Enqueue(combatEvents.Dequeue());
                    }
                    combatEvents = nl;
                }
            }
            enemies = clean;

            if (enemies.Count() <= 0)
            {
                //hm.
            }
        }

        public AbstractEncounter Cheat()
        {
            dead.Clear();
            enemies.Clear();
            return parent;
        }

        public void ExecuteCombat()
        {
            // default should be lets sort in order of initiative..?
            // start with sort by dex..
            roundStarted = true;
            roundFinished = false;
            IList<AbstractCreature> total = new List<AbstractCreature>();
            total = total.Concat(enemies).ToList();
            total = total.Concat(WorldHub.Instance.PlayerInfo.AwayTeam).ToList();
            IList<AbstractCreature> sorted = total.OrderByDescending(o => o.GetStat(Stats.DEXTERITY)).ToList();
            foreach (AbstractCreature ac in sorted)
            {
                attackQueue.Enqueue(ac);
            }
        }

        private bool ProcessAttackQueue()
        {
            while (attackQueue.Count != 0)
            {
                AbstractCreature attacker = attackQueue.Dequeue();
                if (attacker is Skipper.Creatures.Crew.CrewMember)
                {
                    int a = 3;
                }
                if (attacker.GetStat(Stats.HP) <= 0 || dead.Contains(attacker))
                {
                    return ProcessAttackQueue();
                    //return;
                }

                CombatResult result = attacker.PerformCombatAction(this);
                //result.Defender.ApplyCombatResult(result);
                
                IList<CombatResult> triggered = (IList<CombatResult>)ActiveCards.ResolveOnEventEffects(result);
                
                combatEvents.Enqueue(result);

                if (triggered != null)
                {
                    foreach (CombatResult cr in triggered)
                    {
                        //cr.Defender.ApplyCombatResult(cr);
                        combatEvents.Enqueue(cr);
                    }
                }
                return true;
            }

            return false;
        }

        public AbstractEncounter AdvanceEncounter()
        {
            if (combatEvents.Count() > 0)
            {
                return this;
            }

            if (attackQueue.Count > 0 )
            {
                bool didAttack = ProcessAttackQueue();
                if (didAttack)
                {
                    //return this;
                }
                else // no more creatures could attack (ie. in queue but dead)
                {
                    UpdateOpponents();
                    //return this;
                }

                if (attackQueue.Count < 1)
                    roundFinished = true;
            }
            else if (enemies.Count > 0)
            {
                UpdateOpponents();
                
                if (roundFinished == true)
                {
                    roundFinished = false;
                    roundStarted = false;
                    ActiveCards.TimeStep();
                    // pull all the messages in combatresults...
                    string[] message = ActiveCards.GetAllMessages();

                    foreach (string s in message)
                    {
                        TurnEvent te = new TurnMessageEvent("fixtitlePCE", s);
                        combatEvents.Enqueue(te);
                    }
                    WorldHub.Instance.PlayerInfo.Player.Deck.FillHand();
                }

                //return this;
            }
            else
            {
                // should .. return new TreasureEncounter(dead, this.Parent);
                // do here or somewhere else???
                WorldHub.Instance.PlayerInfo.Player.Deck.FillHand();
                return parent;
            }

            return this;
        }

        // queue up the combat results for each Attack..
        public TurnEvent GetNextEvent()
        {
            if (combatEvents.Count > 0)
            {
                TurnEvent current = combatEvents.Dequeue();
                current.ApplyAnyEffects();
                UpdateOpponents();
                return current;
            }
            return null;
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            //TODO: Complete
        }
    
    }
}
