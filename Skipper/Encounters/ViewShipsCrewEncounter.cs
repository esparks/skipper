﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures.Crew;

namespace Skipper.Encounters
{
    class ViewShipsCrewEncounter : AbstractEncounter
    {
        public ViewShipsCrewEncounter(AbstractEncounter parent)
            : base(parent)
        {
            foreach (CrewMember cm in WorldHub.Instance.PlayerInfo.Ship.Crew)
            {
                CrewMember member = cm;
                AddOption(cm.Name, (out string text) => { text = ""; return new ViewCrewStatsEncounter(this, member); });

            }
            AddOption("Exit", (out string text) => { text = ""; return parent; });
            
        }

        public AbstractEncounter Quit()
        {
            return parent;
        }

        public void ViewMember(string name)
        {

        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            //throw new NotImplementedException();
        }
    }
}
