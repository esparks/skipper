﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters
{
    class NavigationEncounter : AbstractEncounter
    {
        // handles moving etc. 
        // should the nav display even be an encounter? 
        // i kind of think.... not..?

        public NavigationEncounter(System.Xml.Linq.XElement node, AbstractEncounter parent)
            : this()
        {

        }

        public NavigationEncounter()
            : base()
        {
            this.Name = "NAVIGATION";
            this.Description = "UNUSED";
            // uses AsciiNavigationView ICustomView and inputs.
            
            // options will be hidden.

        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            //
        }
    }
}
