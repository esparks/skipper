﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters.Ruins
{
    class BasicMazeGenerator : IMazeGenerator
    {
        private struct Box
        {
            public int sizeX;
            public int sizeY;
            public int posX;
            public int posY;
        }

        public Maze Generate(int sizeX, int sizeY, int seed)
        {
            MazeTile[,] tiles = new MazeTile[sizeX, sizeY];
            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    tiles[x, y] = new MazeTile(new Vector2i(x, y), true, 0);
                }
            }
            Console.WriteLine("Generating maze...");
            //Console.ReadKey();
            //TODO: Fix this -1 nonsense. Or place a wall all the way around anyway
            IList<Box> boxes = DoSplit(0, 0, sizeX - 1, sizeY - 1);
            int boxIndex = 1;
            for (int j = 0; j < boxes.Count; j++)
            {
                Box box = boxes.ElementAt(j);
                //Console.WriteLine("box: {0} {1}, {2} {3}", b.posX, b.posY, b.sizeX, b.sizeY );
                Box b = ShrinkBox(box, 1);
                for (int x = b.posX; x < (b.posX + b.sizeX); x++)
                {
                    for (int y = b.posY; y < (b.posY + b.sizeY); y++)
                    {
                        //if (tiles[x, y].code != 0)
                          //  throw new Exception("COLLISION");

                        tiles[x, y] = new MazeTile(new Vector2i(x, y), false, boxIndex);

                        // connect to next
                        if (j < boxes.Count - 1)
                        {
                            Box source = b;
                            Box target = ShrinkBox(boxes.ElementAt(j + 1), 1);
                            // find the closes point to each other.
                            int sx = source.posX + (source.sizeX / 2);
                            int sy = source.posY + (source.sizeY / 2);

                            int tx = target.posX + (target.sizeX / 2);
                            int ty = target.posY + (target.sizeY / 2);

                            int cx = sx;
                            int cy = sy;

                            //
                            
                            do
                            {
                                if (Math.Abs(ty - cy) == 0)
                                {
                                    if (tx > cx)
                                    {
                                        cx++;
                                    }
                                    else if (tx < cx)
                                    {
                                        cx--;
                                    }
                                }
                                else 
                                {
                                    if (ty > cy)
                                    {
                                        cy++;
                                    }
                                    else if (ty < cy)
                                    {
                                        cy--;
                                    }
                                }
                                // shortest first?
                                /*if (Math.Abs(tx - cx) > Math.Abs(ty - cy))
                                {
                                    if (tx > cx) 
                                    {
                                        cx++;
                                    }
                                    else if (tx < cx)
                                    {
                                        cx--;
                                    }
                                }
                                else
                                {
                                    if (ty > cy)
                                    {
                                        cy++;
                                    } 
                                    else if (ty < cy)
                                    {
                                        cy--;
                                    }
                                }*/
                                
                                if (tiles[cx, cy].IsWall)
                                    tiles[cx, cy] = new MazeTile(new Vector2i(cx, cy), false, 99);

                            } while (cx != tx || cy != ty);
                        }
                    }
                }
                boxIndex++;
                //DrawDebug(sizeX, sizeY, tiles);
            }

            return new Maze(sizeX, sizeY, tiles, new List<MazeTile>(), new List<MapItem>());
        }

        private void DrawDebug(int sizeX, int sizeY, MazeTile[,] tiles)
        {

            for (int y = 0; y < sizeY; y++)
            {
                for (int x = 0; x < sizeX; x++)
                {
                    if (tiles[x, y].IsWall)
                        Console.Write(' ');
                    else
                        Console.Write((char)(tiles[x, y].code + 97));
                }
                Console.Write('\n');
            }
            //Console.ReadKey();
        }

        private IList<Box> DoSplit(int posX, int posY, int sizeX, int sizeY)
        {
            int min = 5;
 
            // pick split axis
            bool hSplit;
            int splitX = 0;
            int splitY = 0;

            if (sizeY <= min && sizeX <= min)
            {
                Box b = new Box();
                b.posX = posX;
                b.posY = posY;
                b.sizeX = sizeX;
                b.sizeY = sizeY;
                return new List<Box> { b };
            }
            if ((sizeX < min && sizeY >= min) || sizeX == 1)
            {
                hSplit = false;
                splitY = Rand.Instance.NextInt((sizeY - min )) + min - 1;
            }
            else if((sizeY < min && sizeX >= min) || sizeY == 1)
            {
                hSplit = true;
                splitX = Rand.Instance.NextInt((sizeX - min )) + min - 1;
            }
            else
            {
                //hSplit =  Rand.Instance.NextBool(50);
                if (sizeX > sizeY)
                    hSplit = true;
                else
                    hSplit = false;

                splitY = Rand.Instance.NextInt((sizeY - min)) + min - 1;
                splitX = Rand.Instance.NextInt((sizeX - min)) + min - 1;

            }

            if (splitX >= sizeX)
            {
                splitX = sizeX - 1;
            }

            if (splitY >= sizeY)
            {
                splitY = sizeY - 1;
            }
            
            // gen two boxes
            Box box1 = new Box();
            Box box2 = new Box();

            if (hSplit)
            {
                if (splitX == 0) throw new Exception("WRONG");
                box1.posX = posX;
                box1.posY = posY;
                box1.sizeX = splitX;
                box1.sizeY = sizeY;

                box2.posX = posX + splitX;
                box2.posY = posY;
                box2.sizeX = sizeX - splitX;
                box2.sizeY = sizeY;            }
            else
            {
                if (splitY == 0) throw new Exception("WRONG");
                box1.posX = posX;
                box1.posY = posY;
                box1.sizeX = sizeX;
                box1.sizeY = splitY;

                box2.posX = posX;
                box2.posY = posY + splitY;
                box2.sizeX = sizeX;
                box2.sizeY = sizeY - splitY;
            }

            //shrink!
            //box1 = ShrinkBox(box1, 1, min);
            //box2 = ShrinkBox(box2, 1, min);

            IList<Box> boxes = new List<Box>();

            if ((box1.sizeX > min || box1.sizeY > min))
            {
          //      Console.WriteLine("box1 size = " + box1.sizeX + " : " + box1.sizeY);
            //    Console.ReadKey();
                //boxes.Concat(DoSplit(box1.posX, box1.posY, box1.sizeX, box1.sizeY));
                IList<Box> extra = DoSplit(box1.posX, box1.posY, box1.sizeX, box1.sizeY);
                foreach (Box b in extra) { boxes.Add(b); }
            }
            else
            {
               // if (box1.sizeX >= 3 && box1.sizeY >= 3)
                    boxes.Add(box1);
            }

            if ((box2.sizeX > min || box2.sizeY > min))
            {
                IList<Box> extra = DoSplit(box2.posX, box2.posY, box2.sizeX, box2.sizeY);
                foreach (Box b in extra) { boxes.Add(b); }
            }
            else
            {
               // if (box2.sizeX >= 3 && box2.sizeY >= 3)
                    boxes.Add(box2);
            }
            //Console.WriteLine("RET BOX");
            return boxes;
        }

        private Box ShrinkBox(Box b, int amount)
        {
            b.sizeX -= amount;
            b.sizeY -= amount;
            b.posX += amount;
            b.posY += amount;

            b.sizeX = Math.Max(b.sizeX, 1);
            b.sizeY = Math.Max(b.sizeY, 1);
            b.posX = Math.Max(b.posX, 0);
            b.posY = Math.Max(b.posY, 0);

            return b;
        }

    }
}
