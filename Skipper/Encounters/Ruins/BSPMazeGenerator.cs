﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cargo;

namespace Skipper.Encounters.Ruins
{
    class BSPMazeGenerator
    {
    
        private enum Split { VERT, HOR };
        private class Node
        {
            public bool isLeaf;
            public int level;
            public Box room;
            public bool locked;
            public Node left;
            public Node right;
            public Split split;
        }

        private struct Box
        {
            public int sizeX;
            public int sizeY;
            public int posX;
            public int posY; 
        }

        int maxLevel = 0;

        public Maze Generate(int sizeX, int sizeY, int seed, AbstractEncounter exit)
        {
            MazeTile[,] tiles = new MazeTile[sizeX, sizeY];
            IList<MazeTile> exits = new List<MazeTile>();
            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    tiles[x, y] = new MazeTile(new Vector2i(x, y), true, 0);
                }
            }
            Node root = new Node();
            root.level = 0;
            root.isLeaf = false;
            root.locked = false;
            root.room = new Box();
            root.room.posX = 0;
            root.room.posY = 0;
            root.room.sizeX = sizeX;
            root.room.sizeY = sizeY;
            root.split = Rand.Instance.NextBool(50) ? Split.HOR : Split.VERT;
            Node result = Explore(root);

            // do a dfs/bfs expansion and write all pieces to tiles.
            IList<Box> rooms = new List<Box>();
            IList<MapItem> items = new List<MapItem>();
            Expand(ref rooms, ref items, result);
            bool entry = false;
            int stairCount = 0;
            
            for (int i = 0; i < rooms.Count; i++)
            {
                Box b = rooms.ElementAt(i);
                for (int x = b.posX; x < (b.posX + b.sizeX); x++)
                {
                    for (int y = b.posY; y < (b.posY + b.sizeY); y++)
                    {
                        if (entry)
                        {
                            tiles[x, y] = new MazeTile(new Vector2i(x, y), false, i);
                        }
                        else
                        {
                            tiles[x, y] = new StairTile(new Vector2i(x, y), false, i, exit);
                            exits.Add(tiles[x, y]);
                            entry = true;
                        }

                        
                        if (i < rooms.Count - 1)
                        {
                            Connect(b, rooms.ElementAt(i + 1), ref tiles);
                        }
                    }
                }
            }

            //items...
            foreach (MapItem mi in items)
            {
                MapTileEvent mte = tiles[mi.location.x, mi.location.y].tileEvent;
                if (mte == null)
                {
                    mte = new MapItemStack();
                    tiles[mi.location.x, mi.location.y].tileEvent = mte;
                }

                if (mte is MapItemStack)
                {
                    ((MapItemStack)mte).items.Add(mi);
                }
                else
                {
                    Console.WriteLine("putting items on a door or similar, FIX THIS PLEASE.");
                }
            }

            return new Maze(sizeX, sizeY, tiles, exits, items);
        }

        private void Connect(Box source, Box target, ref MazeTile[,] tiles)
        {
            //Box source = b;
            //Box target = ShrinkBox(boxes.ElementAt(j + 1), 1);
            // find the closes point to each other.
            int sx = source.posX + (source.sizeX / 2);
            int sy = source.posY + (source.sizeY / 2);

            int tx = target.posX + (target.sizeX / 2);
            int ty = target.posY + (target.sizeY / 2);

            int cx = sx;
            int cy = sy;

            do
            {
                
                if (Math.Abs(ty - cy) == 0)
                {
                    if (tx > cx)
                    {
                        cx++;
                    }
                    else if (tx < cx)
                    {
                        cx--;
                    }
                }
                else
                {
                    if (ty > cy)
                    {
                        cy++;
                    }
                    else if (ty < cy)
                    {
                        cy--;
                    }
                }
                if (tiles[cx, cy].IsWall)
                    tiles[cx, cy] = new MazeTile(new Vector2i(cx, cy), false, 99);
                // lock?

                //TODO: don't do this here. have another algorithm.
                if (Rand.Instance.NextBool(90))
                {
                    tiles[cx, cy].tileEvent = new MapLockedDoor();    
                }
            } while (cx != tx || cy != ty);
        }

        private void Expand(ref IList<Box> boxes, ref IList<MapItem> items, Node root)
        {
            if (root == null) return;

            if (root.isLeaf)
            {
                Box b = root.room;
                b = ShrinkBox(b, 1);
                boxes.Add(b);
                
                // random treasure?
                MapItem mi = new MapItem();
                int mx = Rand.Instance.NextInt(b.posX, b.posX + b.sizeX);
                int my = Rand.Instance.NextInt(b.posY, b.posY + b.sizeY);

                mi.location = new Vector2i(mx, my);
                mi.item = new AlienArtifacts(1);
                items.Add(mi);
            }
            else
            {
                if (Rand.Instance.NextBool(50))
                {
                    Expand(ref boxes, ref items, root.left);
                    Expand(ref boxes, ref items, root.right);
                }
                else
                {
                    Expand(ref boxes, ref items, root.right);
                    Expand(ref boxes, ref items, root.left);
                }
            }
        }

        private Node Explore(Node root)
        {
            int min = 3;
            int minlevel = 3;
            double roomChance = 33;
            double emptyChance = 33;
            if (root.isLeaf) return root;

            maxLevel = Math.Max(maxLevel, root.level);

            if (root.room.sizeX <= min * 2 && root.room.sizeY <= min * 2 || root.room.sizeX <= 1 || root.room.sizeY <= 1)
            {
                //Console.WriteLine("root.room.sizeX = " + root.room.sizeX + " |X| root.room.sizeY = " + root.room.sizeY);
                root.isLeaf = true;
                return root;
            }

            if (root.level >= minlevel)
            {
                if (Rand.Instance.NextBool(roomChance))
                {
                    root.isLeaf = true;
                    return root;
                }

                if (Rand.Instance.NextBool(emptyChance))
                {
                    return null;
                }
            }

            // split into two and explore each
            Node leftNode = new Node();
            Node rightNode = new Node();

            if (root.split == Split.HOR)
            {
                int splitPoint = Rand.Instance.NextInt(root.room.posX + (root.room.sizeX / 3), root.room.posX + (root.room.sizeX - (root.room.sizeX / 3)) - 1);
                //Console.WriteLine("H split point = " + splitPoint);
                //Console.ReadKey();
                leftNode.room.posX = root.room.posX;
                leftNode.room.posY = root.room.posY;
                leftNode.room.sizeX = splitPoint - root.room.posX;//*root.room.sizeX - (splitPoint - root.room.posX);*/
                leftNode.room.sizeY = root.room.sizeY;

                rightNode.room.posX = splitPoint;
                rightNode.room.posY = root.room.posY;
                rightNode.room.sizeX = root.room.sizeX - leftNode.room.sizeX;
                rightNode.room.sizeY = root.room.sizeY;

                if (leftNode.room.sizeX < 0)
                {
                    //Console.WriteLine("node sizeX = " + rightNode.room.sizeX + ", parent sizeX = " + root.room.sizeX + " parent posX = " + root.room.posX + " splitpoint = " + splitPoint);
                }
            }
            else
            {
                int splitPoint = Rand.Instance.NextInt(root.room.posY + (root.room.sizeY / 3), root.room.posY + (root.room.sizeY - (root.room.sizeY / 3)) - 1);
                //Console.WriteLine("v split point = " + splitPoint);
                //Console.ReadKey();
                leftNode.room.posX = root.room.posX;
                leftNode.room.posY = root.room.posY;
                leftNode.room.sizeX = root.room.sizeX;
                leftNode.room.sizeY = splitPoint - root.room.posY;// root.room.sizeY - splitPoint; 

                rightNode.room.posX = root.room.posX;
                rightNode.room.posY = splitPoint;
                rightNode.room.sizeX = root.room.sizeX;
                rightNode.room.sizeY = root.room.sizeY - leftNode.room.sizeY;

                if (leftNode.room.sizeX < 0)
                {
                    //Console.WriteLine("node sizeX = " + rightNode.room.sizeX + ", parent sizeX = " + root.room.sizeX + " parent posX = " + root.room.posX + " splitpoint = " + splitPoint);
                }
            }

            leftNode.isLeaf = false;
            rightNode.isLeaf = false;
            leftNode.level = root.level + 1;
            rightNode.level = root.level + 1;

            if (root.split == Split.HOR)
            {
                leftNode.split = Split.VERT;
                rightNode.split = Split.VERT;
            }
            else
            {
                leftNode.split = Split.HOR;
                rightNode.split = Split.HOR;
            }

            root.left = leftNode;
            root.right = rightNode;

            if (Rand.Instance.NextBool(50))
            {
                Explore(leftNode);
                Explore(rightNode);
            }
            else
            {
                Explore(rightNode);
                Explore(leftNode);
            }
            return root;
        }

        private void DebugNode(Node root)
        {
            Console.WriteLine("===root===");
            PrintNode(root);
            Console.WriteLine("===left===");
            PrintNode(root.left);
            Console.WriteLine("===right===");
            PrintNode(root.right);
        }

        private void PrintNode(Node root)
        {
            if (root == null)
            {
                Console.WriteLine("NULL");
                return;
            }

            Console.WriteLine("room.posX: " + root.room.posX);
            Console.WriteLine("room.posY: " + root.room.posY);
            Console.WriteLine("room.sizeX: " + root.room.sizeX);
            Console.WriteLine("room.sizeY: " + root.room.sizeY);
            Console.WriteLine("   .isLeaf: " + root.isLeaf);
            Console.WriteLine("    .level: " + root.level);
        }

        private Box ShrinkBox(Box b, int amount)
        {
            b.sizeX -= (amount * 2);
            b.sizeY -= (amount * 2);
            b.posX += amount;
            b.posY += amount;

            if (b.sizeX <= 0)
                b.sizeX = 1;

            if (b.sizeY <= 0)
                b.sizeY = 1;
            //b.sizeX = Math.Max(b.sizeX, 1);
            //b.sizeY = Math.Max(b.sizeY, 1);
            //b.posX = Math.Max(b.posX, 0);
            //b.posY = Math.Max(b.posY, 0);

            return b;
        }
    }
}
