﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters.Ruins
{
    class MapItemStack : MapTileEvent
    {
        private Vector2i location;
        public Vector2i Location { get { return location; } set { location = value; } }
        public IList<MapItem> items;

        public MapItemStack()
        {
            items = new List<MapItem>();
        }
    }
}
