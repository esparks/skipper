﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;

namespace Skipper.Encounters.Ruins
{
    class MazeTile
    {
        private bool isWall;
        private Vector2i position;

        public bool IsWall { get { return isWall; } private set { isWall = value; } }
        public Vector2i Position { get { return position; } private set { position = value; } }

        public MapTileEvent tileEvent;

        /// TODO: Replace with a class that returns an encounter or null on
        /// roll of die compared to an internal table of probabilities.
        private IList<IList<AbstractCreature>> encountersList;

        public int code;

        public MazeTile(Vector2i position, bool isWall, int code)
        {
            this.isWall = isWall;
            this.code = code;
            this.position = position;

        }


    }
}
