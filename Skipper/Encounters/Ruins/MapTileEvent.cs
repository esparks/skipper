﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters.Ruins
{
    interface MapTileEvent
    {
        Vector2i Location
        { 
            get; 
            set;
        }
    }
}
