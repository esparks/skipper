﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters.Ruins
{
    interface IMazeGenerator
    {
        Maze Generate(int sizeX, int sizeY, int seed);
    }
}
