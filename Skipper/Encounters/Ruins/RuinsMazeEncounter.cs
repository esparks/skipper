﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters.Ruins
{
    class RuinsMazeEncounter : AbstractEncounter
    {
        private Maze maze;

        public Maze Maze { get { return maze; } private set { maze = value; } }

        private int width = 60;
        private int height = 30;

        public int Width { get { return width; } private set { width = value; } }
        public int Height { get { return height; } private set { height = value; } }

        private Vector2i playerPosition;

        public RuinsMazeEncounter(AbstractEncounter parent)
            : base(parent)
        {
            //if (Rand.Instance.NextBool(5))
            {
            //    maze = new BasicMazeGenerator().Generate(width, height, 0);
            }
            //else
            {
                maze = new BSPMazeGenerator().Generate(width, height, 0, this);
            }
            this.AddOption("EXIT", (out string text) => { text = "You leave the ruins.";  return parent; });
            // player pos should be "stairwell" of current floor
            // find stairs ...
            // should make a map // or list // in a "maze.cs" class

            if (maze.Exits.Count > 0)
            {
                playerPosition = new Vector2i(maze.Exits.ElementAt(0).Position);
            }
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            //throw new NotImplementedException();
        }

        public Vector2i GetPlayerPosition()
        {
            return new Vector2i(playerPosition.x, playerPosition.y);
        }

        public AbstractEncounter MovePlayer(Vector2i d)
        {
            Vector2i next = playerPosition.Add(d);
            if (!maze.Tiles[next.x, next.y].IsWall)
            {
                playerPosition = next;
                return maze.UpdateTileEncounter(playerPosition.x, playerPosition.y, this);
            }
            return this;
        }

    }
}
