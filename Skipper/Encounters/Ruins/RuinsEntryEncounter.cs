﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;
using System.Xml.Linq;

namespace Skipper.Encounters.Ruins
{
    class RuinsEntryEncounter : AbstractEncounter 
    {

        private AbstractEncounter firstRoom;
        // generate a number of rooms and connect them together


        /// <summary>
        /// Remove this one?????
        /// </summary>
        public RuinsEntryEncounter()
            : base()
        {
            this.Name = "Ruins Entrance";
            this.Description = "You stand outside the entrance to an ancient ruin.";
            //firstRoom = new RuinsRoomEncounter(this, DirectionTools.GetRandomDirection(), Rand.Instance.NextInt(4) + 2);
            firstRoom = new RuinsMazeEncounter(this);
            GenerateOptions();
        }

        public RuinsEntryEncounter(AbstractEncounter parent)
            : base (parent)
        {

            this.Name = "Ruins Entrance";
            this.Description = "You stand outside the entrance to an ancient ruin.";
            //firstRoom = new RuinsRoomEncounter(this, DirectionTools.GetRandomDirection(), Rand.Instance.NextInt(4) + 2);
            firstRoom = new RuinsMazeEncounter(this);
            GenerateOptions();
        }

        public RuinsEntryEncounter(XElement node, AbstractEncounter parent)
            : base(node, parent)
        {
            //TODO: change as required.
            firstRoom = null;// new RuinsRoomEncounter(node.Element("room"));
            //TODO: load maze from save file if exists, else generate.
            firstRoom = new RuinsMazeEncounter(this);        
            GenerateOptions();
        }

        private void GenerateOptions()
        {
            this.AddOption("Enter", (out string text) => { text = "You proceed inside."; return firstRoom; });
            this.AddOption("Leave", (out string text) => { text = "You return to your ship"; return parent; });
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            XElement room = new XElement("room");
            firstRoom.Serialize(ref room);
            node.Add(room);
        }
    }
}
