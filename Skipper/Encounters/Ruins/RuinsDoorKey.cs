﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters.Ruins
{
    class RuinsDoorKey : AbstractCollectible
    {
        string keyId; // eg: "redplanet_floor1_door12" or just a hash "Fiwi9j29109u532"

        public RuinsDoorKey(string name)
            : base (name, 1)
        {
            // might need to procedurally generate a key id
        }

    }
}
  