﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;
using System.Xml.Linq;

namespace Skipper.Encounters.Ruins
{

    class RuinsRoomEncounter : AbstractEncounter
    {
        // should put these in text files :)
        private string[] roomMod = new string[] { "Dark", "Cold", "Wet", "Dim" };
        private string[] roomAdj = new string[] {"Yellow", "Enveloping", "Crumbling", "Desolate", "Derelict", "Cracked", "Scorched"};
        private string[] roomTitle = new string[] { "Temple", "Basement", "Corridoor", "Church", "Barracks", "Archway", "Fjord", "Passage", "Ravine" };

        private IDictionary<Directions, AbstractEncounter> exits;
        
        private class RoomAttr
        {
            public string mod;
            public string adj;
            public string title;
        }

        private RoomAttr attributes;

        public RuinsRoomEncounter(AbstractEncounter parent, Directions parentDir, int length)
            : base(parent)
        {
            exits = new Dictionary<Directions, AbstractEncounter>();
            this.attributes = GenerateRoomAttr();
            this.Name = (attributes.mod + " " + attributes.adj + " " + attributes.title).Trim();
            this.Description = "generate a description from the given parts...";
            this.exits.Add(DirectionTools.GetOppositeDirection(parentDir), parent);
            // numexits
            if (length > 0)
            {
                int maxex = 3;
                int numex = Rand.Instance.NextInt(maxex);
                int nlen = length - 1;
                for (int i = 0; i < numex; i++)
                {
                    Directions d = DirectionTools.GetRandomDirection();
                    while (exits.ContainsKey(d))
                    {
                        d = DirectionTools.GetRandomDirection();
                    }
                    //Console.WriteLine("adding exit to the " + DirectionTools.GetDirectionString(d));
                    exits.Add(d, new RuinsRoomEncounter(this, d, nlen));
                }
            }
            this.AddExitOptions();
            //this.AddOption("Leave", (out string text) => { text = "You leave the ruins."; return parent; });
        }

        private void AddExitOptions()
        {
            Directions? entry = null;
            foreach (Directions dir in exits.Keys)
            {
                // should sort this list probably.
                string exit = DirectionTools.GetDirectionString(dir);
                exit = exit.First().ToString().ToUpper() + exit.Substring(1);
                Directions arg = dir;
                AbstractEncounter ae = exits[dir];
                if (ae is RuinsEntryEncounter)
                {
                    entry = dir;
                }
                else
                {
                    this.AddOption("Go " + exit, (out string text) => { return MoveTo(out text, arg); });
                }
            }
            if (entry != null)
            {
                this.AddOption("Return to entrance", (out string text) => { return MoveTo(out text, entry.GetValueOrDefault()); });
            }
        }

        private AbstractEncounter MoveTo(out string text, Directions dir)
        {
            if (exits.ContainsKey(dir))
            {
                text = "You head in that direction";
                return exits[dir];
            }
            else
            {
                text = "You can't go that way.";
                return this;
            }
        }

        private RoomAttr GenerateRoomAttr()
        {
            RoomAttr attr = new RoomAttr();
            if (Rand.Instance.NextBool(50.0))
            {
                attr.mod = roomMod[Rand.Instance.NextInt(roomMod.Count())];
            }
            else
            {
                attr.mod = "";
            }
            attr.adj = roomAdj[Rand.Instance.NextInt(roomAdj.Count())];
            attr.title = roomTitle[Rand.Instance.NextInt(roomTitle.Count())];
            return attr;
        }

        protected override void SerializeImpl(ref System.Xml.Linq.XElement node)
        {
            // TODO: Complete, but first change how ruins work (eg. move to a map based thing rather than rooms/exits..);
        }

    }
}
