﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters.Ruins
{
    class MapLockedDoor : MapTileEvent
    {
        private Vector2i location;
        public Vector2i Location { get { return Location; } set { location = value; } }
        private RuinsDoorKey key;
        public RuinsDoorKey Key { get { return key; } private set { key = value; } }

    }
}
