﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters.Ruins
{
    // maybe a 'stack' of items should inherit from maptileevent
    class MapItem
    {
        public AbstractCollectible item;
        public Vector2i location;
    }
}
