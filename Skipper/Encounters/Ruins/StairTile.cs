﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Encounters.Ruins
{
    class StairTile : MazeTile
    {
        private AbstractEncounter exit;

        //TODO: remove "code", it's meaningless except for that stupid debug.
        // does 2015 community have better refactoring?
        public StairTile(Vector2i position, bool isWall, int code, AbstractEncounter exit)
            : base(position, isWall, code)
        {
            this.exit = exit;
        }
    }
}
