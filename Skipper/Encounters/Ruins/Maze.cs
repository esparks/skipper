﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using Skipper.Data;

namespace Skipper.Encounters.Ruins
{
    class Maze
    {
        // this should be a list/map etc. of different floors
        private MazeTile[,] tiles;
        private int width;
        private int height;
        private IList<MazeTile> exits;
        public MazeTile[,] Tiles { get { return tiles; } private set { tiles = value; } }
        public IList<MazeTile> Exits { get { return exits; } private set { exits = value; } }

        public Maze(int width, int height, MazeTile[,] tiles, IList<MazeTile> exits, IList<MapItem> items)
        {
            this.width = width;
            this.height = height;
            this.tiles = tiles;
            this.exits = exits;
        }

        public AbstractEncounter UpdateTileEncounter(int x, int y, AbstractEncounter parent)
        {
            if (Rand.Instance.NextBool(5))
            {
                IList<AbstractCreature> enemies = new List<AbstractCreature>();

                /// do this betterere!
                IList<string> pick = new List<string> { "insectoid", "flying polyp" };

                for (int i = 0; i < 4; i++)
                {
                    if (Rand.Instance.NextBool(33))
                    {
                        if (Rand.Instance.NextBool(70))
                        {
                            enemies.Add(CreatureFactory.SpawnInsectoid(Rand.Instance.NextInt(1, 2)));
                        }
                        else
                        {
                            enemies.Add(CreatureFactory.SpawnPolyp(1));
                        }
                    }
                }
                return new PersonalCombatEncounter(enemies, parent);
            }
            return parent;
        }
    }
}