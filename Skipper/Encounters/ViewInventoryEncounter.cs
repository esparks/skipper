﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cargo;
using System.Xml.Linq;

namespace Skipper.Encounters
{
    class ViewInventoryEncounter : AbstractEncounter
    {
        public ViewInventoryEncounter()
            : base()
        {
            this.Name = "View Cargo";
            this.Description = AssembleCargoView();
            this.AddOption("Exit", Exit);
        }

        public AbstractEncounter Exit(out string text)
        {
            text = "";
            return parent;
        }

        private string AssembleCargoView()
        {
            string view = "";
            if (WorldHub.Instance.PlayerInfo.Ship.Cargo.Count < 1)
            {
                return "NO CARGO";
            }

            foreach (AbstractCargo ac in WorldHub.Instance.PlayerInfo.Ship.Cargo)
            {
                string line = ac.Name;
                line = line.PadRight(30);
                line += ac.Amount + "\n";
                view += line;
            }
            return view;
        }

        protected override void SerializeImpl(ref XElement node)
        {
            // ignore
        }
    }
}
