﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures.Crew;

namespace Skipper
{
    class ShipCrewEmplacements
    {
        
        private EmplacementGroup navigation;
        private EmplacementGroup security;
        private EmplacementGroup engineering;

        public ShipCrewEmplacements()
        {

            List<Emplacement> engEmplacements = new List<Emplacement>
            {
                new Emplacement("Chief Engineer"),
                new Emplacement("2nd Engineer"),
                new Emplacement("3rd Engineer")
            };

            List<Emplacement> secEmplacements = new List<Emplacement>
            {
                new Emplacement("Head of Security"),
                new Emplacement("Security 2IC"),
                new Emplacement("Commander's Guard"),
                new Emplacement("Commander's Guard")
            };

            List<Emplacement> navEmplacements = new List<Emplacement>
            {
                new Emplacement("Ship's Pilot"),
                new Emplacement("Co-Pilot"),
                new Emplacement("Navigation Systems"),
                new Emplacement("Shuttle Pilot")
            };

            engineering = new EmplacementGroup("Engineering", engEmplacements);
            security = new EmplacementGroup("Security", secEmplacements);
            navigation = new EmplacementGroup("Navigation", navEmplacements);

        }
    }
}
