﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper
{
    class Stats
    {
        public const string MAXHP = "maxhp";
        public const string HP = "hp";
        public const string STRENGTH = "strength";
        public const string DEXTERITY = "dexterity";
        public const string AIM = "aim";
        public const string WILLPOWER = "willpower";
        public const string INTELLIGENCE = "intelligence";
        public const string CHARISMA = "charisma";
    }
}
