﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using Skipper.Data;
using System.Xml;
using System.Xml.Linq;

namespace Skipper
{
    class PlayerStats : IPersistant
    {
        private Ship ship;
        private AbstractCreature player;
        public AbstractCreature Player { get { return player; } }
        public Ship Ship { get { return ship; } }

        private IList<AbstractCreature> awayTeam;
        public AbstractCreature[] AwayTeam { get { return awayTeam.ToArray(); } }

        private int teamMax = 4;
        public int TeamMax { get { return teamMax; } }

        public void AddTeamMember(AbstractCreature member)
        {
            if (awayTeam.Count < teamMax)
            {
                awayTeam.Add(member);
            }
        }

        // shuffle, move position, etc..

        public PlayerStats(Vector2i startingLocation)
        {
            awayTeam = new List<AbstractCreature>();
            
            // FIXME: will never be used?? //
            CreatureConfiguration cc = new CreatureConfiguration();
            cc.stats[Stats.HP] = 50;
            cc.stats[Stats.MAXHP] = cc.stats[Stats.HP];
            cc.stats[Stats.STRENGTH] = 10;
            cc.stats[Stats.DEXTERITY] = 10;
            cc.stats[Stats.AIM] = 10;
            cc.stats[Stats.WILLPOWER] = 10;
            cc.stats[Stats.INTELLIGENCE] = 10;
            cc.stats[Stats.CHARISMA] = 10;
            // starting weapon!
            IList<string> verbs = new List<string> { "zap", "blast", "strike" }; // would be neat to order them so they could get picked based on damage spread
            Weapon starter = new Weapon("Laser Pistol", 10, 5, 5, 1.5f, Stats.AIM, verbs);
            cc.weapon = starter;

            player = new Player(cc);
            int startingCrewMax = 10;
            ship = new Ship(startingCrewMax, startingLocation);
        }

        public PlayerStats(XElement node)
        {
            // scan node and add team members etc.
            // can deprecate call to other constructor when more implemented.
            ship = new Ship(node.Element("ship"));
            player = new Player(node.Element("player"));
            awayTeam = new List<AbstractCreature>();
            // find awayteam members in ship's crew..

            IList<string> awayTeamNames = new List<string>();
            XElement teamNode = node.Element("awayTeam");
            foreach (XElement member in teamNode.Elements("member"))
            {
                awayTeamNames.Add(member.Value);
            }

            awayTeam = new List<AbstractCreature>();
            foreach (string s in awayTeamNames)
            {
                foreach (AbstractCreature ac in ship.Crew)
                {
                    if (ac.Name == s)
                    {
                        awayTeam.Add(ac);
                    }
                }
            }
        }
        
        public void Serialize(ref XElement node)
        {
            XElement playerNode = new XElement("player");
            player.Serialize(ref playerNode);
            node.Add(playerNode);

            XElement AwayTeamElement = new XElement("awayTeam");
            foreach (AbstractCreature ac in AwayTeam)
            {
                XElement member = new XElement("member");
                member.Value = ac.Name;
                AwayTeamElement.Add(member);
            }
            node.Add(AwayTeamElement);

            // serialize ship - actual crew members will get done here.
            XElement xship = new XElement("ship");
            ship.Serialize(ref xship);
            node.Add(xship);
        }
    }
}
