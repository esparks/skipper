﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;

namespace Skipper.Cards
{
    interface ITargetMultipleCreatures
    {
        IList<AbstractCreature> GetTargets();
        int NumToSelect { get; }
    }
}
