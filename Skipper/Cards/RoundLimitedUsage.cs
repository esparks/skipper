﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;

namespace Skipper.Cards
{
    abstract class RoundLimitedUsage : AbstractActiveUsage
    {
        protected int rounds;
        public int Rounds { get { return rounds; } }

        public RoundLimitedUsage(int startRounds, AbstractCard card, AbstractCreature user)
            : base(card, user)
        {
            this.rounds = startRounds;
        }

        public override void TimeStep()
        {
            rounds--;
            OnRoundTimeStep();
        }

        public override bool StillActive()
        {
            if (rounds > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected abstract void OnRoundTimeStep();

    }
}
