﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Cards
{
    /// <summary>
    /// These cards can be placed on the playing field to provide
    /// ongoing effects.
    /// </summary>
    /// 
    interface IActiveCard
    {
        AbstractCard Card { get; }
        bool AppliesTo(object o);
        bool StillActive();
        void TimeStep();
        string[] EndOfRoundMessages { get; }
    }
}
