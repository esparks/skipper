﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;

namespace Skipper.Cards
{
    interface IDamageModifierCard
    {
        CombatResult ModifyDamage(CombatResult preliminary);
    }
}
