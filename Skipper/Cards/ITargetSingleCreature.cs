﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;

namespace Skipper.Cards
{
    interface ITargetSingleCreature
    {
        IList<AbstractCreature> GetTargets();
    }
}
