﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Creatures;

namespace Skipper.Cards
{
    abstract class AbstractActiveUsage : AbstractUsage, IActiveCard
    {
        private IList<object> applicable;
        private IList<string> messages;
        private AbstractCombatEntity user; //tosd

        public AbstractActiveUsage(AbstractCard card, AbstractCombatEntity user)
            : base(card)
        {
            this.applicable = new List<object>();
            this.messages = new List<string>();
        }

        protected void AddMessage(string message)
        {
            messages.Add(message);
        }

        public string[] EndOfRoundMessages
        {
            get
            {
                string[] ret = messages.ToArray();
                messages.Clear();
                return ret;
            }
        }

        protected void AddApplicable(object o)
        {
            applicable.Add(o);
        }

        protected void AddApplicableList(IList<object> list)
        {
            foreach (object o in list)
            {
                AddApplicable(o);
            }
        }

        protected void AddApplicableList(IList<AbstractCreature> list)
        {
            foreach (object o in list)
            {
                AddApplicable(o);
            }
        }

        public bool AppliesTo(object o)
        {
            return applicable.Contains(o);
        }

        public abstract bool StillActive();
        public abstract void TimeStep();
        
        protected override internal string UseCard(Creatures.AbstractCreature user, AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
        {
            encounter.ActiveCards.AddActiveUsage(this, user);
            return ActiveUseCardImplementation(user, encounter, deck, arguments);
        }

        abstract protected internal string ActiveUseCardImplementation(Creatures.AbstractCreature user, AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments);
    }
}
