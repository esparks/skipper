﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;

namespace Skipper.Cards
{
    abstract class AbstractUsage
    {

        private AbstractCard card;
        public AbstractCard Card { get { return card; } }

        public AbstractUsage(AbstractCard card)
        {
            this.card = card;
        }

        abstract protected internal string UseCard(Creatures.AbstractCreature user, AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments);
      
    }
}
