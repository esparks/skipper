﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Cards
{
    class AbstractDeck
    {
        private IList<AbstractCard> deck;
        private IList<AbstractCard> discard;
        private IList<AbstractCard> hand;
        private IList<AbstractCard> destroy;

        public IList<AbstractCard> Hand { get { return hand; } }

        private int handmax;
        public int HandMax { get { return handmax; } }
        private string title;
        public string Title { get { return title; } }
        public int CountInDeck { get { return deck.Count; } }

        public AbstractDeck(string title, int handmax)
        {
            this.title = title;
            this.handmax = handmax;
            deck = new List<AbstractCard>();
            discard = new List<AbstractCard>();
            hand = new List<AbstractCard>();
            destroy = new List<AbstractCard>();
        }

        public void AddToBottomOfDeck(AbstractCard card)
        {
            deck.Insert(0, card);
        }

        public void AddToTopOfDeck(AbstractCard card)
        {
            deck.Add(card);
        }

        public void AddToDiscard(AbstractCard card)
        {
            discard.Add(card);
        }

        public void DestroyCard(AbstractCard card)
        {
            destroy.Add(card);
        }

        public void DrawCardToHand()
        {
            if (deck.Count < 1)
            {
                // Trigger something interesting?
                Console.WriteLine("DECK EMPTYY!!!!!");
                return;
            }

            hand.Add(deck.ElementAt(deck.Count - 1));
            deck.RemoveAt(deck.Count - 1);
        }

        public void Shuffle()
        {
            IList<AbstractCard> temp = new List<AbstractCard>();
            while (deck.Count > 0)
            {
                int sel = Rand.Instance.NextInt(deck.Count);
                temp.Add(deck.ElementAt(sel));
                deck.RemoveAt(sel);
            }
            deck = temp;
        }

        public void FillHand()
        {
            while (hand.Count < handmax && deck.Count > 0)
            {
                DrawCardToHand();
            }
        }
    }
}
