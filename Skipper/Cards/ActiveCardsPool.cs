﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using Skipper.Encounters;
using Skipper.Data;

namespace Skipper.Cards
{
    class ActiveCardsPool
    {
        //IList<KeyValuePair<AbstractCreature,AbstractCard> activeCards
        IList<IActiveCard> playersActiveCards;
        IList<KeyValuePair<AbstractCreature, IActiveCard>> creatureActiveCards;
        IList<IActiveCard> otherCards;

        IList<IActiveCard> allActiveCards;

        public IActiveCard[] PlayerActives { get { return playersActiveCards.ToArray(); } }

        public ActiveCardsPool()
        {
            allActiveCards = new List<IActiveCard>();
            playersActiveCards = new List<IActiveCard>();
        }

        public void AddActiveUsage(IActiveCard usage, AbstractCreature user)
        {
            if (user is Player)
            {
                playersActiveCards.Add(usage);
            }

            allActiveCards.Add(usage);
        }

        public void TimeStep()
        {
            IList<IActiveCard> remove = new List<IActiveCard>();
            foreach (IActiveCard iac in allActiveCards)
            {
                iac.TimeStep();
                if (!iac.StillActive())
                {
                    remove.Add(iac);
                }
            }

            foreach (IActiveCard iac in remove)
            {
                allActiveCards.Remove(iac);
                playersActiveCards.Remove(iac);
                //otherCards.Remove(iac);
                //creatureActiveCards.Remove(iac);
            }
        }

        public int GetStatWithEffects(AbstractCreature creature, string statName, bool playerTeam)
        {
            // run through all and apply //
            int stat = creature.GetStat(statName);
            return stat;
        }

        public IList<string> ApplyEndOfRoundEffects(AbstractEncounter encounter)
        {
            return null;
        }

        public IList<string> ApplyStartOfRoundEffects(AbstractEncounter encounter)
        {
            return null;
        }

        // Affect final damage result during combat calculations
        public object ResolveDamageModifiers(object ev)
        {
            if (ev is CombatResult)
            {
                CombatResult cr = (CombatResult)ev;
                AbstractCreature defender = cr.Defender;
                foreach (IActiveCard iac in allActiveCards)
                {
                    if (iac is IDamageModifierCard)
                    {
                        if (iac.AppliesTo(defender))
                        {
                            cr = ((IDamageModifierCard)iac).ModifyDamage(cr) as CombatResult;
                        }
                    }
                }
                return cr;
            }
            return ev;
        }

        // Triggered by events
        // EG OnCombatEvent should after you get attacked trigger to heal, do damage, etc.
        public object ResolveOnEventEffects(object ev)
        {
            ////
            if (ev is CombatResult)
            {
                CombatResult cr = (CombatResult)ev;
                AbstractCreature defender = cr.Defender;
                IList<CombatResult> effects = new List<CombatResult>();
                // apply cards to attacker

                // apply cards to defender
                foreach (IActiveCard iac in allActiveCards)
                {
                    if (iac is ICombatEventCard)
                    {
                        if (iac.AppliesTo(defender))
                        {
                            IList<CombatResult> effect = ((ICombatEventCard)iac).OnEvent(cr);
                            // apply in trigger!!!! //
                            foreach (CombatResult e in effect)
                            {
                                effects.Add(e);
                            }
                        }
                    }
                }
                // how to choose order of resolution??????
                ///// any active cards on you reflect first
                ///// then reduce damage
                ///// then convert damage
                ///// then ...
                //foreach(ActiveCard
                return effects;
            }
            return null;
        }

        public string[] GetAllMessages()
        {
            IList<string> msg = new List<string>();
            foreach (IActiveCard iac in allActiveCards)
            {
                string[] stack = iac.EndOfRoundMessages;
                if (stack != null)
                {
                    foreach (string s in stack)
                    {
                        msg.Add(s);
                    }
                }
            }
            return msg.ToArray();
        }
    }
}
