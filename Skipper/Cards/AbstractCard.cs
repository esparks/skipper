﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using Skipper.Encounters;

namespace Skipper.Cards
{
    abstract class AbstractCard
    {
        public enum Usage { REFRESH, DESTROY, DISCARD };

        private Usage[] validUsages;
        public Usage[] ValidUsages { get { return validUsages; } protected set { validUsages = value; } }

        private string name;
        public string Name { get { return name; } }

        private string description;
        public string Description { get { return description; } protected set { description = value; } }

        private Type[] useDuring;
        public Type[] UseDuring { get { return useDuring; } protected set { useDuring = value; } }

        public AbstractCard(string name)
        {
            this.name = name;
            this.description = "UNSET";
        }

        public bool CanUseHere(AbstractEncounter ae)
        {
            if (useDuring == null)
            {
                return false;
            }
            else
            {
                foreach (Type type in UseDuring)
                {
                    if (ae.GetType().IsSubclassOf(type) || ae.GetType() == type)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public string UseCard(Usage usage, AbstractCreature user, AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
        {
            string output = GetUsage(usage, deck, user, encounter).UseCard(user, encounter, deck, arguments);
            AfterUse(usage, deck);
            return output;
        }

        public AbstractUsage GetUsage(Usage usage, AbstractDeck deck, AbstractCreature user, AbstractEncounter encounter)
        {
            if (CanUseHere(encounter))
            {
                return GetCardImplementation(usage, user, encounter, deck, null);//.UseCard(user, encounter, deck, arguments);
                
                //AfterUse(usage, deck);
                //return text;
            }
            throw new Exception ("An error: should not be able to use card " + this.Name + " at this time!");
        }

        protected abstract AbstractUsage GetCardImplementation(Usage usage, AbstractCreature user, AbstractEncounter encounter, AbstractDeck deck, IDictionary <string, object> arguments);

        protected virtual void AfterUse(Usage usage, AbstractDeck deck)
        {
            switch (usage)
            {
                case Usage.REFRESH:
                    deck.AddToBottomOfDeck(this);
                    break;
                case Usage.DISCARD:
                    deck.AddToDiscard(this);
                    break;
                case Usage.DESTROY:
                    deck.DestroyCard(this);
                    break;
                default:
                    throw new Exception("Error handling card, no valid usage!");
            }
            deck.Hand.Remove(this);
        }
    }
}
