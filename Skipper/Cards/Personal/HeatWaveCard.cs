﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;

namespace Skipper.Cards.Personal
{
    /// <summary>
    /// Deals (10 * source bonus) (heat) damage
    /// to all attackers.
    /// </summary>
    class HeatWaveCard : PersonalCard
    {
        
        public HeatWaveCard()
            : base("Heat Wave")
        {
            this.Description = "Refresh: Surrounds the target in a hot wind that deals damage to anyone that approaches.";
            this.Description = "Discard: blast damage etc. (should be: refresh=%50 fire damage, discard=also do damage to attackers)";
            this.UseDuring = new Type[] { typeof(Encounters.PersonalCombatEncounter) };
        }


        protected override AbstractUsage GetCardImplementation(AbstractCard.Usage usage, Creatures.AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
        {
            if (usage == AbstractCard.Usage.REFRESH)
            {
                IList<AbstractCreature> targets;
                if (user is Player)
                {
                    targets = WorldHub.Instance.PlayerInfo.AwayTeam;
                    return new RefreshUsage(3, targets, user, this);
                }
            }
            throw new NotImplementedException();
        }

        private class RefreshUsage : RoundLimitedUsage, IActiveCard, ICombatEventCard, ITargetSingleCreature
        {
            // should just reduce fire damage to you, and have discard the mode that also deals damage.
            IList<AbstractCreature> targets; 

            public RefreshUsage(int rounds, IList<AbstractCreature> targets, AbstractCreature user, AbstractCard card)
                : base(rounds, card, user)
            {
                this.targets = targets;
            }

            protected override void OnRoundTimeStep()
            {
                this.AddMessage("The heat is scorching");
            }

            public IList<Data.CombatResult> OnEvent(Data.CombatResult midstage)
            {
                // receive an event, propogate more events..

                AbstractCreature attacker = midstage.Attacker;
                AbstractCreature defender = midstage.Defender;
                AbstractCreature me = midstage.Defender;
                Data.CombatResult damage = attacker.CalculateDamage(defender, new Data.Damage(10, Data.DamageTypes.HEAT));
                //damage.ResultText = 
                damage.SetDescription(() => { return defender.Name + "'s HeatWave deals " + attacker.Name + " " + damage.Damage + " damage."; });
                return new List<Data.CombatResult>() { damage }; // TODO: get rid of that stupid "Killed" in combat results..?
            }

            protected internal override string ActiveUseCardImplementation(AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
            {
                // target
                AbstractCreature target = arguments[CardArguments.TARGET] as AbstractCreature;
                this.AddApplicable(target);
                return "A wall of heat rises around " + target.Name;
            }

            public IList<AbstractCreature> GetTargets()
            {
                return targets;
            }
        }
    }
}
