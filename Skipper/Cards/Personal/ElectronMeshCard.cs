﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using Skipper.Data;

namespace Skipper.Cards.Personal
{
    class ElectronMeshCard : PersonalCard
    {

        private const int initialRounds = 3;

        public ElectronMeshCard()
            : base("Electron Mesh")
        {
            this.Description = "Creates a static field that dissipates energy weapons.";
            this.Description += "Refresh: Become invulnerable for three rounds.";
            this.Description += "Discard: Blow the shield to deal energy damage to all enemies.";
            this.UseDuring = new Type[] { typeof(Encounters.PersonalCombatEncounter) };
        }

        protected override AbstractUsage GetCardImplementation(AbstractCard.Usage usage, Creatures.AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
        {
            if (usage == Usage.REFRESH)
            {
                return new RefreshUsage(initialRounds, this, user);
            }
            throw new Exception();
        }

        private class RefreshUsage : RoundLimitedUsage, IDamageModifierCard
        {
            public RefreshUsage(int rounds, AbstractCard card, AbstractCreature user)
                : base(rounds, card, user)
            {
                
            }

            protected override void OnRoundTimeStep()
            {
                if (rounds > 0)
                    this.AddMessage("The electron mesh field weakens..");
                else
                    this.AddMessage("The electron mesh dissipates.");
            }

            public CombatResult ModifyDamage(Data.CombatResult midstage)
            {
                CombatResult result = new CombatResult(midstage.Attacker, midstage.Defender, 0);
                return result;
            }

            protected internal override string ActiveUseCardImplementation(AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
            {
                if (user is Player)
                {
                    this.AddApplicableList(WorldHub.Instance.PlayerInfo.AwayTeam.ToList());
                }
                return "Electron-Mesh activates!";
            }
        }
    }
}
