﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Cards.Personal
{
    class AdrenalinBooster : PersonalCard
    {
        public AdrenalinBooster()
            : base("Adrenal Boost")
        {
            this.Description = "Improves response times for a limited period.";
            this.UseDuring = new Type[] { typeof(Encounters.MonsterEncounter) };
        }

        protected override AbstractUsage GetCardImplementation(AbstractCard.Usage usage, Creatures.AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
        {
            throw new Exception("Please implement a 'cards in play' stash to check effects etc.");
        }
    }
}
