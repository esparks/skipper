﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Cards.Personal
{
    abstract class PersonalCard : AbstractCard
    {
        public PersonalCard(string name)
            : base(name)
        {

        }
    }
}
