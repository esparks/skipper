﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using Skipper.Data;

namespace Skipper.Cards.Personal
{
    class HollowpointRounds : PersonalCard
    {

        private const int initialRounds = 3;

        public HollowpointRounds()
            : base("Hollow Point Rounds")
        {
            this.Description = "Increases firearm damage";
            this.Description += "Refresh: +20% damage from firearms for 3 rounds.";
            this.Description += "Discard: +20% damage from firearms for 5 round.";
            this.UseDuring = new Type[] { typeof(Encounters.PersonalCombatEncounter) };
        }

        protected override AbstractUsage GetCardImplementation(AbstractCard.Usage usage, Creatures.AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
        {
            if (usage == Usage.REFRESH)
            {
                return new RefreshUsage(initialRounds, this, user);
            }
            throw new Exception();
        }

        private class RefreshUsage : RoundLimitedUsage, IDamageModifierCard
        {
            public RefreshUsage(int rounds, AbstractCard card, AbstractCreature user)
                : base(rounds, card, user)
            {

            }

            protected override void OnRoundTimeStep()
            {
                if (rounds > 0)
                    this.AddMessage("You are running low on hollowpoint rounds.");
                else
                    this.AddMessage("You are out of hollowpoints.");
            }

            public CombatResult ModifyDamage(CombatResult midstage)
            {
                CombatResult result = new CombatResult(midstage.Attacker, midstage.Defender, midstage.Damage + (int)(0.2 * midstage.Damage));
                return result;
            }

            protected internal override string ActiveUseCardImplementation(AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
            {
                if (user is Player)
                {
                    this.AddApplicableList(WorldHub.Instance.PlayerInfo.AwayTeam.ToList());
                }
                return user.Name + " passes out hollowpoint rounds.";
            }
        }
    }
}
