﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Cards.Personal
{
    class FlashGrenadeCard : PersonalCard
    {
        public FlashGrenadeCard()
            : base("Flash Grenade")
        {
            this.Description = "Usage: Refresh to stun all enemies for 1 round.";
        }


        protected override AbstractUsage GetCardImplementation(AbstractCard.Usage usage, Creatures.AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
        {
            throw new NotImplementedException();
        }
    }
}
