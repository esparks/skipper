﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Creatures;

namespace Skipper.Cards.Personal
{
    class MedKitCard : PersonalCard
    {
        public MedKitCard()
            : base("MedKit")
        {
            this.Description = "Usage: Refresh to restore 25hp to a crewmember\nDiscard to revive a KO'd crewmember";
            this.UseDuring = new Type[] { typeof(AbstractEncounter) };
        }

        protected override AbstractUsage GetCardImplementation(AbstractCard.Usage usage, Creatures.AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
        {
            if (usage == Usage.REFRESH)
            {
                return new RefreshUsage(this);
            }
            throw new Exception("Shouldn't be here");
        }

        private class RefreshUsage : AbstractUsage, ITargetSingleCreature
        {
            public RefreshUsage(AbstractCard card)
                : base(card)
            {

            }

            protected internal override string UseCard(Creatures.AbstractCreature user, Encounters.AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
            {
                AbstractCreature target = (AbstractCreature)arguments[CardArguments.TARGET];
                string text = target.Name + " regains 25HP!";
                target.RegainHP(25);
                return text;
            }
            public IList<AbstractCreature> GetTargets()
            {
                return WorldHub.Instance.PlayerInfo.AwayTeam.ToList();
            }
        }

    }
}
