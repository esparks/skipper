﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Creatures;

namespace Skipper.Cards.Personal
{
    class OrbitalStrikeCard : PersonalCard
    {
        // this card destroys one enemy
        // shuffle back into the deck on use
        public OrbitalStrikeCard()
            : base("Orbital Strike")
        {
            this.Description = "Refresh during combat to call a precision laser strike down from your ship and destroy a single enemy.";
            this.Description += "\nDiscard to destroy all enemies.";
            this.Description += "\nCannot be used indoors.";
            this.ValidUsages = new Usage[] { Usage.REFRESH, Usage.DISCARD };
            this.UseDuring = new Type[] { typeof(PersonalCombatEncounter) };
        }

        // need something like: OrbitalStrikeCardUsageGui ...
        protected override AbstractUsage GetCardImplementation(AbstractCard.Usage usage, Creatures.AbstractCreature user, AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
        {
            if (usage == Usage.REFRESH)
            {
                return new RefreshUsage(this, ((PersonalCombatEncounter)encounter).Enemies);
            }

            if (usage == Usage.DISCARD)
            {
                //text = "Flashes of light skewer down from the sky, destroying the group of enemies. (but not implemented yet..)";
                throw new NotImplementedException();
            }
            throw new Exception("Shouldn't enter here..");
        }
        
        private class RefreshUsage : AbstractUsage, ITargetSingleCreature
        {
            private IList<AbstractCreature> targets;
            public RefreshUsage(AbstractCard card, IList<AbstractCreature> targets)
                : base(card)
            {
                this.targets = targets;
            }

            protected internal override string UseCard(Creatures.AbstractCreature user, AbstractEncounter encounter, AbstractDeck deck, IDictionary<string, object> arguments)
            {
                PersonalCombatEncounter me = (PersonalCombatEncounter)encounter;
                AbstractCreature monster = (AbstractCreature)arguments[CardArguments.TARGET];
                monster.Kill();
                me.UpdateOpponents();
                return "A sudden blast of light vapourizes your opponents.";
            }

            public IList<AbstractCreature> GetTargets()
            {
                return targets;
            }
        }


    }
}
