﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;

namespace Skipper.Cards
{
    interface ICombatEventCard
    {
        IList<CombatResult> OnEvent(CombatResult midstage);
    }
}
