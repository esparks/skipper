﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace Skipper
{
    class Program
    {
        public const int USING_CURSES = 0;
        
        static void PrintTest()
        {
            Console.Write("   DESIGN TEST.... \n" +
                           "╔════GALACTIC════╗    ╔═════SECTOR═════╗\n" +
                           "║    ░ ▒▒▒▒░     ║    ║                ║    HERE: Nothing\n" +
                           "║   ░ ▒▓▓▓▓▒░    ║    ║   ▒   §        ║    CREW:\n" +
                           "║  ░ ▒▓▓▓▓▓▓▒ ░  ║    ║  ▒▒     ×      ║        NAVCON  Heshel Romero\n" +
                           "║  ░▒║▓▓▓▓▓▓║▒░  ║    ║   ▒▒           ║        COMBAT  Geert Gniewek\n" +
                           "║   ░▒║▓▓▓▓║▒░   ║    ║           ϴ    ║        SCIENCE Iovianus Lee\n" +
                           "║   ░░▒║▓▓║▒░    ║    ║ ►     .        ║        MEDICAL Melik Morra\n" +
                           "║    ░░▒║║▒░     ║    ║   .     ʘ      ║        RADAR   Byrne Stankić\n" +
                           "║     ░░▒▒░      ║    ║     .          ║        \n" +
                           "╚════════════════╝    ╚═══════JX═══════╝        \n");
        }

        static void Main(string[] args)
        {

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.WriteLine("Skipper 1.0");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            PrintTest();
            Console.Clear();
            Console.WriteLine("calcheck:");
            double ttime = Map.TravelTimes.GetConstantThrustTravelTimeKM(384000, 1, 0) / 60.0 / 60.0;
            Console.WriteLine("time to moon = " + ttime);
            int quit = 0;
            ConsoleGame game = new ConsoleGame();
            //if CursesGame game = new CursesGame();
            do
            {
                quit = game.DoAStep();
                //Console.Clear();
            } while (quit != 1);
            Console.WriteLine("Goodbye.");
        }
    }
}
