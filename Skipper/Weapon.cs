﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;
using System.Xml.Linq;

namespace Skipper
{
    class Weapon : IPersistant
    {
        private string name;
        private int damage;
        private int variance;
        private float critchance;
        private float critmult;
        private string primaryStat;

        public int Damage { get { return damage; } }
        public int Variance { get { return variance; } }
        public float CriticalChance { get { return critchance; } }
        public float CriticalMultiplier { get { return critmult; } }
        public string PrimaryStat { get { return primaryStat; } }

        public string Name { get { return name; } }

        public IList<string> attackVerbs;

        public Weapon(string name, int damage, int variance, float critchance, float multiplier, string primaryStat, IList<string> attackVerbs)
        {
            this.name = name;
            this.damage = damage;
            this.primaryStat = primaryStat;
            this.critchance = critchance;
            this.critmult = multiplier;
            this.attackVerbs = attackVerbs;
            this.variance = variance;
        }

        public Weapon(XElement node)
        {
            this.name = node.Attribute("name").Value;
            this.damage = int.Parse(node.Attribute("damage").Value);
            this.primaryStat = node.Attribute("primaryStat").Value;
            this.critchance = float.Parse(node.Attribute("critchance").Value);
            this.critmult = float.Parse(node.Attribute("critmult").Value);
            this.variance = int.Parse(node.Attribute("variance").Value);
            this.attackVerbs = new List<string>();
            XElement verbs = node.Element("verbs");
            foreach (XElement v in verbs.Elements("verb"))
            {
                attackVerbs.Add(v.Value);
            }
        }

        public string GetVerb(int damage)
        {
            // damage should factor in to selecting the verb, should be ordered by least damaging to most
            return attackVerbs.ElementAt(Rand.Instance.NextInt(attackVerbs.Count));
        }



        public void Serialize(ref System.Xml.Linq.XElement node)
        {
            node.Add(new XAttribute("name", name));
            node.Add(new XAttribute("damage", damage));
            node.Add(new XAttribute("variance", variance));
            node.Add(new XAttribute("critchance", critchance));
            node.Add(new XAttribute("critmult", critmult));
            node.Add(new XAttribute("primaryStat", primaryStat));
            XElement verbs = new XElement("verbs");
            foreach (string s in attackVerbs)
            {
                verbs.Add(new XElement("verb", s));
            }
            node.Add(verbs);
        }
    }
}
