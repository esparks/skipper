﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;
using Skipper.Encounters;
using Skipper.Creatures;
using Skipper.Cards;
using Skipper.Display;

namespace Skipper
{
    class ConsoleGame
    {
        private AsciiViewMap views;

        ICustomView currentView;
        AbstractEncounter lastEncounter;

        private Stack<AbstractEncounter> masterEncounterStack;
        private WorldData data;
        private Stack<AbstractEncounter> encounterStack;
        private bool quit = false;

        private int statLineHeight;
        private int cardY;
        private int cardHeight = 5;
        private int viewOptionsHeight = 0;

        private int selectedCard = 0;

        private IRenderer renderer;
        private IKeyInput input;

        int width = 80;
        int height = 40;

        // deprecate all this, replace with a "Default" custom view :)
        struct ColorPair
        {
            public ConsoleColor Background;
            public ConsoleColor Foreground;
        }

        private Stack<ColorPair> colorStack;

        public ConsoleGame()
        {
            lastEncounter = null;
            currentView = null;
            Setup();
        }

        private void Setup()
        {
            Console.WriteLine("Init callbacks");
            
            MasterTactics tactics = new MasterTactics();
            WorldHub.Instance.InjectTactics(tactics);
            
            Console.WriteLine("Init renderer");
            renderer = new LibtcodRenderer(width, height);//new ConsoleRenderer();
            
            Console.WriteLine("Init input");
            input = new TCODInput();//new ConsoleInput();
            
            Console.WriteLine("Init asciisectordisplay");
            AsciiSectorDisplay.Initialise();
            Console.WriteLine("Init asciiviewmap");
            views = new AsciiViewMap(renderer, input);
            Console.WriteLine("Init creature weapons");
            WorldHub.Instance.InjectCreatureWeapons(new Creatures.CreatureWeaponsList());
            Console.WriteLine("Inject master weapons");
            WorldHub.Instance.InjectMasterWeaponsList(new Data.MasterWeaponsList());
            Console.WriteLine("Init player");
            WorldHub.Instance.InjectPlayerStats(new PlayerStats(new Vector2i(2,2))); //FIXME: start location gets re-set later
            Console.WriteLine("Init name generator");
            WorldHub.Instance.InjectNameGenerator(new NameGenerator());

            Console.WriteLine("Init Careers");
            WorldHub.Instance.InjectCareers(new Careers());

            Console.WriteLine("Init Origins");
            WorldHub.Instance.InjectOrigins(new Origins());

            Console.WriteLine("Init crew factory");
            WorldHub.Instance.InjectCrewFactory(new CrewFactory());
            
            Console.WriteLine("Init stack");
            encounterStack = new Stack<AbstractEncounter>();
            colorStack = new Stack<ColorPair>();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            PushColors();
            Console.WriteLine("Init world data");
            data = new WorldData();

            Console.WriteLine("init sector map");
            WorldHub.Instance.InjectSectorMap(data.CreateSectors());
            Console.WriteLine("sector map initialized");

            // move player ship to the correct location - space station
            Vector2i pStart = data.GetPlayerStartLocation();
            WorldHub.Instance.PlayerInfo.Ship.SetPosition(WorldHub.Instance.SectorMap, pStart.x, pStart.y);

            //IList<AbstractEncounter> encTabTemp = data.PopulateEncountersTable();
            //var reversed = encTabTemp.Reverse();
            masterEncounterStack = new Stack<AbstractEncounter>();
            //foreach (AbstractEncounter ae in reversed)
            //{
            //    masterEncounterStack.Push(ae);
            //}
            statLineHeight = Console.WindowHeight - 1;
            cardY = statLineHeight - cardHeight + 1;
            //NewEncounter();
            Console.WriteLine("Encounter initialized, game ready.");
            encounterStack.Push(new NavigationEncounter());
        }

        private void NewEncounter()
        {
            int max = masterEncounterStack.Count;
            if (max < 1)
            {
                DoWin();
                return;
            }
            AbstractEncounter encounter = masterEncounterStack.Pop();
            encounterStack.Push(encounter);
        }

        public int DoAStep()
        {
            AbstractEncounter encounter = encounterStack.Pop();

            // moving into custom views: take the below code and push it into a AsciiStandardView

            //DrawPlayerHand(WorldHub.Instance.PlayerInfo.Player.Deck);
            
            //DrawViewOptions();
            //DrawPlayerStats();
            ICustomView view = null;

            if (encounter == lastEncounter)
            {
                view = currentView;
            }
            else
            {
                view = views.GetView(encounter.GetType());
            }

            lastEncounter = encounter;
            currentView = view;

            if (view != null)
            {
                view.Render(encounter);
                AbstractEncounter enc = view.ProcessInput(encounter);
                //if (enc == null) NewEncounter();
                if (enc != null)
                    encounterStack.Push(enc);
                else
                {
                    encounterStack.Push(new NavigationEncounter());
                }
                return 0;
            }

            //string[] options = encounter.Options;
            //Console.SetCursorPosition(0, 0);
            //Console.Clear();
            //Console.ForegroundColor = ConsoleColor.Green;
            //Console.BackgroundColor = ConsoleColor.Black;
            //PushColors();
            //Console.ForegroundColor = ConsoleColor.Cyan;
            //Console.WriteLine(encounter.Name + '\n');
            //PopColors();
            //Console.WriteLine(encounter.Description + "\n");

            //PushColors();
            //int optx = Console.CursorLeft;
            //int opty = Console.CursorTop;
            //Console.BackgroundColor = ConsoleColor.Gray;
            //Console.ForegroundColor = ConsoleColor.Blue;
            //foreach (string option in options)
            //    Console.WriteLine(option);
            //PopColors();

            //int curx = Console.CursorLeft;
            //int cury = Console.CursorTop;
            //DrawPlayerHand(WorldHub.Instance.PlayerInfo.Player.Deck);
            
            //Console.SetCursorPosition(curx, cury);
            //ConsoleKeyInfo key = Console.ReadKey(true);
            //// check if "special" keys (select / display / etc)
            //if (key.Key == ConsoleKey.RightArrow)
            //{
            //    ChangeCardSelection(1);
            //}
            //else if (key.Key == ConsoleKey.LeftArrow)
            //{
            //    ChangeCardSelection(-1);
            //}
            //else if (key.Key == ConsoleKey.Enter)
            //{
            //    // use card
            //    //
            //    AbstractCard card = WorldHub.Instance.PlayerInfo.Player.Deck.Hand.ElementAt(selectedCard);
            //    string output = "";
            //    if (card.CanUseHere(encounter))
            //    {
            //        // go to use card encounter?
            //        output += "Playing " + card.Name + " >>> \n";
            //        output += card.UseCard(AbstractCard.Usage.REFRESH, WorldHub.Instance.PlayerInfo.Player.Deck, WorldHub.Instance.PlayerInfo.Player, encounter);
            //    }
            //    else
            //    {
            //        output = "You can't use that card here.";
            //    }

            //    Console.WriteLine("\n"+output);
            //    PressAnyKey();
            //    encounterStack.Push(encounter);
            //    return 0;
            //}

            //AbstractEncounter meta = CheckMetaOptions(key);
            //if (meta != null)
            //{
            //    /* only push if not another meta */
            //    if (!(encounter is ViewInventoryEncounter))
            //        encounterStack.Push(encounter);
            //    encounterStack.Push(meta);
            //}
            //else
            //{
            //    string text;
            //    bool valid = encounter.ValidOption(key.KeyChar);
            //    if (valid)
            //    {
            //        int sel = encounter.OptionPosition(key.KeyChar);//int.Parse("" + key.KeyChar); // won't work with 'l', 'x' etc. need fix, ok?
            //        if (sel != -1)
            //        {
            //            Console.SetCursorPosition(optx, opty + sel);
            //            PushColors();
            //            Console.BackgroundColor = ConsoleColor.DarkYellow;
            //            Console.ForegroundColor = ConsoleColor.DarkRed;
            //            Console.Write(options[sel]);
            //            PopColors();
            //        }
            //        Console.SetCursorPosition(curx, cury);
            //        AbstractEncounter retVal = encounter.Update(key.KeyChar, out text);

            //        if (!string.IsNullOrEmpty(text))
            //        {
            //            Console.WriteLine("\n" + text);
            //            PressAnyKey();
            //        }

            //        if (retVal != null)
            //        {
            //            encounterStack.Push(retVal);
            //        }
            //    }
            //    else
            //    {
            //        // return this encounter as no options were selected.
            //        encounterStack.Push(encounter);
            //    }

                if (encounterStack.Count < 1)
                {
                    // moving on, regen some HP
                   // WorldHub.Instance.PlayerInfo.Player.RegainHP(10);
                    NewEncounter();
                }

            //}
//            if (key.Key == ConsoleKey.F10 || quit == true) 
//                return 1;
            return 0;
        }

        private void SetAnyKeyColors()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.DarkYellow;
        }

        private void PushColors()
        {
            ColorPair pair = new ColorPair();
            pair.Background = Console.BackgroundColor;
            pair.Foreground = Console.ForegroundColor;
            colorStack.Push(pair);
        }

        private void PopColors()
        {
            ColorPair pair;
            if (colorStack.Count > 1)
            {
                pair = colorStack.Pop();
            }
            else
            {
                pair = colorStack.Peek();
            }
            Console.BackgroundColor = pair.Background;
            Console.ForegroundColor = pair.Foreground;
        }

        private void DoDead()
        {
            Console.WriteLine("You died! You stupid person. So stupid, argh.");
            Console.WriteLine("Press a key to start again.");
            Console.ReadKey(true);
            Setup();
        }

        private void DoWin()
        {
            Console.WriteLine("You Won The Game! Press any key to restart.");
            Console.ReadKey(true);
            Setup();
        }

        private void DrawViewOptions()
        {
            PushColors();
            Console.SetCursorPosition(0, viewOptionsHeight);
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.Yellow;
            string opts = "";
            opts += "F1: CARGO F2: ITEMS F3: STATS";
            // pad
            opts = opts.PadRight(Console.BufferWidth);
            Console.WriteLine(opts);
            PopColors();
        }

        private void DrawPlayerStats()
        {
            PushColors();
            int rMax = Console.BufferWidth;
            Console.SetCursorPosition(0, statLineHeight);
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            string stats = "";
            AbstractCreature p = WorldHub.Instance.PlayerInfo.Player;
            Ship s = WorldHub.Instance.PlayerInfo.Ship;
            stats += "HULL: " + s.HullPoints;
            stats += " SHIELDS: " + s.ShieldPoints;
            stats += " HP: " + p.GetStat(Stats.HP);
            stats += " DECK: " + p.Deck.CountInDeck;
            stats += "     " + Items.Money.DollarSign + p.Money.Amount;
            stats = stats.PadRight(rMax - 1, ' ');
            Console.Write(stats);
            Console.MoveBufferArea(rMax - 2, statLineHeight, 1, 1, rMax - 1, statLineHeight); 
            PopColors();
        }

        private void DrawPlayerHand(AbstractDeck deck)
        {
            throw new Exception("fuckoff");
            PushColors();
            int cardX = 7;
            int cardWidth = 11;
            int y = cardY;
            string hand = "";
            string[] cards = new string[deck.Hand.Count];
            string[] line = new string[cardHeight];
            string[] selectLine = new string[cardHeight + 1];

            for(int j = 0; j < deck.Hand.Count; j++)
            {
                AbstractCard card = deck.Hand.ElementAt(j);
                string[] split = card.Name.Split(' ');
                // copy into a new array
                string[] hold = new string[3] { "", "", "" };
                for (int i = 0; i < split.Length; i++)
                {
                    hold[i] = split[i];
                }
                for (int i = 0; i < hold.Length; i++)
                {
                    hold[i] = hold[i].PadRight(cardWidth - 3);
                }

                if (selectedCard == j)
                {
                    selectLine[0] += "  _______  ";
                    selectLine[1] += " /       \\ ";
                    selectLine[2] += "| " + hold[0] + "|";
                    selectLine[3] += "| " + hold[1] + "|";
                    selectLine[4] += "| " + hold[2] + "|";
                    selectLine[5] += "|         |";
                    line[0] += "           ";
                    line[1] += "           ";
                    line[2] += "           ";
                    line[3] += "           ";
                    line[4] += "           ";
                    continue;
                }

                line[0] += "  _______  ";
                line[1] += " /       \\ ";
                line[2] += "| " + hold[0] + "|";
                line[3] += "| " + hold[1] + "|";
                line[4] += "| " + hold[2] + "|";
            }

            for (int i = 0; i < cardHeight; i++)
            {
                Console.SetCursorPosition(cardX, y + i);
                Console.Write(line[i]);
            }

            Console.ForegroundColor = ConsoleColor.Yellow;

            for (int i = 0; i < cardHeight + 1; i++)
            {
                Console.SetCursorPosition(cardX + (selectedCard * (cardWidth)), y - 1 + i);
                Console.Write(selectLine[i]);
            }

            // draw deck
            /*
            Console.ForegroundColor = ConsoleColor.Blue;
            string blank = " ";
            string ncards = deck.CountInDeck + " cards";
            string indeck = "in deck";
            ncards = ncards.PadRight(cardWidth - 3);
            indeck = indeck.PadRight(cardWidth - 3);
            blank = blank.PadRight(cardWidth - 3);

            line[0] = "  _______  ";
            line[1] = " /       \\ ";
            line[2] = "| " + ncards + "|";
            line[3] = "| " + indeck + "|";
            line[4] = "| " + blank + "|";

            for (int i = 0; i < cardHeight; i++)
            {
                Console.SetCursorPosition(cardX + (cardWidth * 5), y + i);
                Console.Write(line[i]);
            }*/

            PopColors();
        }

        private void ChangeCardSelection(int dir)
        {
            selectedCard += dir;

            if (selectedCard > WorldHub.Instance.PlayerInfo.Player.Deck.Hand.Count - 1)
            {
                selectedCard = 0;
            }

            if (selectedCard < 0)
            {
                selectedCard = WorldHub.Instance.PlayerInfo.Player.Deck.Hand.Count - 1;
            }
        }

        private AbstractEncounter CheckMetaOptions(ConsoleKeyInfo key)
        {
            AbstractEncounter ret = null;
            switch(key.Key) {
                case ConsoleKey.F1:
                    ret = new ViewInventoryEncounter();
                    break;
                case ConsoleKey.F2:
                    ret = null;
                    break;
                case ConsoleKey.F3:
                    ret = null;
                    break;
                default:
                    break;
            }
            return ret;
        }

        private void PressAnyKey()
        {
            PushColors();
            SetAnyKeyColors();
            Console.WriteLine("\tAny key to continue");
            PopColors();
            Console.ReadKey(true);
        }
    }
}
