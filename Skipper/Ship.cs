﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cargo;
using Skipper.Map;
using Skipper.Data;
using Skipper.Creatures.Crew;
using System.Xml.Linq;

namespace Skipper
{
    class Ship : IPersistant
    {
        private int hull;
        private int shield;
        public int HullPoints { get { return hull; } }
        public int ShieldPoints { get { return shield; } }

        public int ArmourValue { get { return CalcTotalArmour(); } }
        private IDictionary<string, AbstractCargo> cargo;
        public IList<AbstractCargo> Cargo { get { return cargo.Values.ToList(); } }

        private int crewMax;
        private IList<CrewMember> totalCrew;
        public CrewMember[] Crew { get { return totalCrew.ToArray(); } }

        private SectorMap currentSector; // should just be a ref?? or is this where it's stored :O should be able to reference by name i think.
        private int sectorX; //TODO: Replace with v2i

        private ShipCrewEmplacements emplacements;
        public ShipCrewEmplacements Emplacements { get { return emplacements; } private set { emplacements = value; } }

        private int sectorY;
        public int SectorX { get { return sectorX; } }
        public int SectorY { get { return sectorY; } }

        public Ship(int crewMax, Vector2i startLocation)
        {
            hull = 100;
            shield = 100;
            cargo = new Dictionary<string, AbstractCargo>();
            sectorX = startLocation.x;
            sectorY = startLocation.y;
            this.crewMax = crewMax;
            totalCrew = new List<CrewMember>();
            emplacements = new ShipCrewEmplacements();
        }

        public Ship(XElement node)
        {
            hull = int.Parse(node.Attribute("hull").Value);
            shield = int.Parse(node.Attribute("shield").Value);
            sectorX = int.Parse(node.Attribute("sectorX").Value);
            sectorY = int.Parse(node.Attribute("sectorY").Value);
            crewMax = int.Parse(node.Attribute("crewMax").Value);
            cargo = new Dictionary<string, AbstractCargo>();

            /// TODO: SERIALIZE!!! ///
            emplacements = new ShipCrewEmplacements();

            //TODO: deprecate..
            if (node.Element("cargo") != null)
            {
                foreach (XElement item in node.Element("cargo").Elements("item"))
                {
                    this.AddCargo(Items.ItemFactory.Instantiate(item) as AbstractCargo);
                }
            }
            // read crew
            totalCrew = new List<CrewMember>();
            foreach (XElement crewNode in node.Elements("member"))
            {
                Console.WriteLine("Loading crew...");
                //get type, instantiate...
                string typename = crewNode.Attribute("type").Value;
                Type t = Type.GetType(typename);
                Console.WriteLine("LOADED TYPE: " + t.Name);
                //AbstractCrewMember acm = t.GetConstructor(new Type[] { typeof(XElement) }).Invoke(new object[] {crewNode}) as AbstractCrewMember;
                System.Reflection.ConstructorInfo ctor = t.GetConstructor(new Type[] { typeof(XElement) });
                object o = ctor.Invoke(new object[] { crewNode});
                CrewMember acm = (CrewMember)o;
                totalCrew.Add(acm);
            }
        }

        private int CalcTotalArmour()
        {
            return 1;
        }

        public void SetPosition(SectorMap sector, int x, int y)
        {
            this.currentSector = sector;
            this.sectorX = x;
            this.sectorY = y;
            // if move outside, get the appropriate new sector..
        }

        public void AddCargo(AbstractCargo ac)
        {

            if (cargo.ContainsKey(ac.Name))
            {
                cargo[ac.Name].AddAmount(ac);
            }
            else
            {
                cargo.Add(ac.Name, ac);
            }
        }

        public bool AddCrew(CrewMember crew)
        {
            if (totalCrew.Count < crewMax)
            {
                totalCrew.Add(crew);
                if (WorldHub.Instance.PlayerInfo.AwayTeam.Count() < WorldHub.Instance.PlayerInfo.TeamMax)
                {
                    WorldHub.Instance.PlayerInfo.AddTeamMember(crew);
                }
                return true;
            }
            return false;
        }

        public bool RemoveCrew(CrewMember crew)
        {
            return totalCrew.Remove(crew);
        }

        public void Serialize(ref System.Xml.Linq.XElement node)
        {
            //TODO add cargo, hp, etc. etc.
            node.Add(new XAttribute("hull", hull));
            node.Add(new XAttribute("shield", shield));
            node.Add(new XAttribute("crewMax", crewMax));
            node.Add(new XAttribute("sectorX", sectorX));
            node.Add(new XAttribute("sectorY", sectorY));

            XElement cargoElement = new XElement("cargo");
            foreach(AbstractCargo ac in cargo.Values)
            {
                XElement item = new XElement("item");
                ac.Serialize(ref item);
                cargoElement.Add(item);
            }

            node.Add(cargoElement);

            foreach (CrewMember acm in totalCrew)
            {
                XElement member = new XElement("member");
                acm.Serialize(ref member);
                node.Add(member);
            }
        }
    }
}
