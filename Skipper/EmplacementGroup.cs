﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper
{
    class EmplacementGroup
    {
        public IList<Emplacement> emplacements;

        public EmplacementGroup(string groupName, IList<Emplacement> emplacements)
        {
            this.emplacements = emplacements;
            Console.WriteLine("Creating EmplacementGroup {0}:", groupName);
            foreach (Emplacement e in emplacements)
            {
                Console.WriteLine("\t" + e.Name);
            }
        }

    }
}
