﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper
{
    class Vector2i : IEquatable<Vector2i>
    {
        private int xvalue;
        private int yvalue;
        public int x { get { return xvalue; } set { xvalue = value; } }
        public int y { get { return yvalue; } set { yvalue = value; } }

        public Vector2i(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector2i(Vector2i v)
        {
            this.x = v.x;
            this.y = v.y;
        }

        public Vector2i Add(Vector2i v)
        {
            Vector2i t = new Vector2i(this.x + v.x, this.y + v.y);
            return t;
        }

        public Vector2i Sub(Vector2i v)
        {
            Vector2i t = new Vector2i(this.x - v.x, this.y - v.y);
            return t;
        }

        public string ToString()
        {
            return this.x + " | " + this.y;
        }


        public override int GetHashCode()
        {
            //return base.GetHashCode();
            // TODO:: Make this better!
            int BIGNUM = 10000000;
            int SMALLERNUM = 1;
            return x * BIGNUM + y * SMALLERNUM;
        }

        public bool Equals(Vector2i v)
        {
            return (this.x == v.x && this.y == v.y);
        }
    }
}
