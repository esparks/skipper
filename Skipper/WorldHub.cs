﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;
using Skipper.Data;
using System.Xml;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.IO;

namespace Skipper
{
    class WorldHub : IPersistant
    {
        private static WorldHub instance = new WorldHub();
        public static WorldHub Instance { get { return instance; } private set { } }
        
        // world elements
        private PlayerStats player;
        public PlayerStats PlayerInfo { get { return player; } }
        private SectorMap sectorMap;
        public SectorMap SectorMap { get { return sectorMap; } }

        // factories and generators
        private NameGenerator nameGenerator;
        public NameGenerator NameGenerator { get { return nameGenerator; } }

        private CrewFactory crewFactory;
        public CrewFactory CrewFactory { get { return crewFactory; } }

        private Careers careers;
        public Careers Careers { get { return careers; } }

        private Origins origins;
        public Origins Origins { get { return origins; } }

        // master lists
        private MasterTactics tactics;
        public MasterTactics Tactics { get { return tactics; } }

        // todo: move to a sub-set of master weapons list
        private Creatures.CreatureWeaponsList creatureWeaponsList;
        public Creatures.CreatureWeaponsList CreatureWeapons { get { return creatureWeaponsList; } }

        private Data.MasterWeaponsList masterWeaponsList;
        public Data.MasterWeaponsList MasterWeapons { get { return masterWeaponsList; } }

        private WorldHub()
        {
            //
        }

        public void InjectTactics(MasterTactics tactics)
        {
            this.tactics = tactics;
        }

        public void InjectCrewFactory(CrewFactory factory)
        {
            this.crewFactory = factory;
        }

        public void InjectNameGenerator(NameGenerator generator)
        {
            this.nameGenerator = generator;
        }

        public void InjectSectorMap(SectorMap map)
        {
            this.sectorMap = map;
        }

        public void InjectPlayerStats(PlayerStats player)
        {
            this.player = player;
        }

        public void InjectCareers(Careers career)
        {
            this.careers = career;
        }

        public void InjectOrigins(Origins origins)
        {
            this.origins = origins;
        }

        public void InjectCreatureWeapons(Creatures.CreatureWeaponsList list)
        {
            this.creatureWeaponsList = list;
        }

        public void InjectMasterWeaponsList(Data.MasterWeaponsList list)
        {
            this.masterWeaponsList = list;
        }

        public void Save(string fileName)
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("SaveData");
            // start with ... PlayerInfo/AwayTeam
            XElement pinfo = new XElement("PlayerStats");
            PlayerInfo.Serialize(ref pinfo);

            XElement secInfo = new XElement("Sectors");
            sectorMap.Serialize(ref secInfo);
            root.Add(pinfo);
            root.Add(secInfo);
            doc.Add(root);
            doc.Save(fileName);

            // test datacontract serialization....
            var types = new List<Type>();
            types.Add(typeof(Creatures.Crew.CrewMember));
            types.Add(typeof(Creatures.Crew.CrewConfigElement));
            types.Add(typeof(Creatures.AbstractCreature));
            DataContractSerializer dcs = new DataContractSerializer(typeof(Creatures.AbstractCreature), types);
            FileStream write = new FileStream("TEST_DATACON.xml", FileMode.Create);
            foreach (Creatures.Crew.CrewMember mem in PlayerInfo.AwayTeam) {
                dcs.WriteObject(write, mem);
            }
            write.Close();
        }

        public void Load(string fileName)
        {
            XDocument doc = XDocument.Load(fileName);
            XElement root = doc.Element("SaveData");
            //reload playinfo
            InjectPlayerStats(new PlayerStats(root.Element("PlayerStats")));
            InjectSectorMap(new SectorMap(root.Element("Sectors")));
        }

        void IPersistant.Serialize(ref XElement node)
        {
            // iterate through all my items 
        }
    }
}
