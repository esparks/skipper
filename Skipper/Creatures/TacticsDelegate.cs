﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Data;

namespace Skipper.Creatures
{
    delegate CombatResult Tactic(AbstractCreature source, AbstractCreature[] enemies, AbstractCreature[] friends, PersonalCombatEncounter encounter);
}
