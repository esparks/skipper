﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cards;

namespace Skipper.Creatures
{
    class Player : AbstractCreature
    {
        public Player(CreatureConfiguration config)
            : base (config)
        {
            this.Money.AddAmount(1000);
            GenerateTestDeck();
        }

        public Player(System.Xml.Linq.XElement node)
            : base(node)
        {
            GenerateTestDeck();
        }

        public void GenerateTestDeck()
        {
            this.Deck.AddToTopOfDeck(new Cards.Personal.OrbitalStrikeCard());
            this.Deck.AddToTopOfDeck(new Cards.Personal.OrbitalStrikeCard());
            this.Deck.AddToTopOfDeck(new Cards.Personal.MedKitCard());
            this.Deck.AddToTopOfDeck(new Cards.Personal.MedKitCard());
            this.Deck.AddToTopOfDeck(new Cards.Personal.FlashGrenadeCard());
            this.Deck.AddToTopOfDeck(new Cards.Personal.FlashGrenadeCard());
            this.Deck.AddToTopOfDeck(new Cards.Personal.AdrenalinBooster());
            this.Deck.AddToTopOfDeck(new Cards.Personal.ElectronMeshCard());
            this.Deck.AddToTopOfDeck(new Cards.Personal.HollowpointRounds());
            this.Deck.AddToTopOfDeck(new Cards.Personal.HollowpointRounds());
            this.Deck.Shuffle();
            ////TODO: REMOVE THIs testing
            //TEST CARDS
            this.Deck.AddToTopOfDeck(new Cards.Personal.ElectronMeshCard());
            this.Deck.AddToTopOfDeck(new Cards.Personal.HeatWaveCard());
            this.Deck.AddToTopOfDeck(new Cards.Personal.HollowpointRounds());


            this.Deck.FillHand();
        }

        //TODO: Make target selectable OR remove commander from combat?
        public override Data.CombatResult PerformCombatAction(Encounters.PersonalCombatEncounter encounter)
        {
            //throw new NotImplementedException();
            return GenericTactics.AttackWeakest(this, encounter.Enemies, WorldHub.Instance.PlayerInfo.AwayTeam, encounter);
        }

        protected override void SerializeImplementation(ref System.Xml.Linq.XElement node)
        {
            // nothing else to add to parent
            //throw new NotImplementedException();
        }

    }
}
