﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace Skipper.Creatures
{
    //TODO: make combat a component!

    [DataContract(Name = "AbstractCombatEntity", Namespace = "http://www.skippergame.com")]
    abstract class AbstractCombatEntity : IPersistant
    {
        public abstract CombatResult CalculateDamage(AbstractCombatEntity attacker, Damage damage);
        public abstract CombatResult ApplyCombatResult(CombatResult cr);
        public abstract void Serialize(ref XElement node);
    }
}
