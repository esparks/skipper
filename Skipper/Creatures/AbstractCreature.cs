﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Items;
using Skipper.Cards;
using Skipper.Data;
using Skipper.Encounters;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace Skipper.Creatures
{
    [DataContract(Name = "AbstractCreature", Namespace = "http://www.skippergame.com")]
    abstract class AbstractCreature : AbstractCombatEntity
    {
        [DataMember()]
        protected int maxhp = 1;
        [DataMember()]
        protected int hp = 1;
        [DataMember()]
        protected int strength = 1;
        [DataMember()]
        protected int dexterity = 1;
        [DataMember()]
        protected int aim = 1;
        [DataMember()]
        protected int willpower = 1;
        [DataMember()]
        protected int intelligence = 1;
        [DataMember()]
        protected int charisma = 1;

        private Weapon weapon = null;
        [DataMember()]
        private string name = "";

        private Money money;
        [DataMember()]
        private int level;
        public int Level { get { return level; } }

        private AbstractDeck deck;
        public AbstractDeck Deck { get { return deck; } }

        public Money Money { get { return money; } }
        public string Name { get { return name; } }
        public Weapon Weapon { get { return weapon; } protected set { weapon = value; } }

        public int ArmourValue { get { return CalcTotalArmour(); } }

        //todo refactor to store: "my name", "name of method in map"
        protected IDictionary<string, string> tactics;
        protected Tactic currentTactic;
        [DataMember()]
        protected string currentTacticName;

        public AbstractCreature(CreatureConfiguration config)
        {
            this.level = config.level;
            this.name = config.Name;
            this.money = config.money ?? new Money(0);
            //TODO add deck to config.
            this.deck = new CreatureDeck("UNSET DECK", 5);//was about say crews won't ghave decks but they will, that get added to yours :)

            this.hp = config.stats[Stats.HP];
            this.maxhp = config.stats[Stats.MAXHP];
            this.aim = config.stats[Stats.AIM];
            this.charisma = config.stats[Stats.CHARISMA];
            this.dexterity = config.stats[Stats.DEXTERITY];
            this.intelligence = config.stats[Stats.INTELLIGENCE];
            this.strength = config.stats[Stats.STRENGTH];
            this.willpower = config.stats[Stats.WILLPOWER];
            this.weapon = config.weapon;
            this.tactics = config.tactics;
        }

        public AbstractCreature(XElement node)
        {
            this.deck = new CreatureDeck("UNSET DECK", 5);
            this.name = node.Attribute("name").Value;
            this.hp = int.Parse(node.Attribute("hp").Value);
            this.maxhp = int.Parse(node.Attribute(Stats.MAXHP).Value);
            this.aim = int.Parse(node.Attribute(Stats.AIM).Value);
            this.charisma = int.Parse(node.Attribute(Stats.CHARISMA).Value);
            this.dexterity = int.Parse(node.Attribute(Stats.DEXTERITY).Value);
            this.strength = int.Parse(node.Attribute(Stats.STRENGTH).Value);
            this.willpower = int.Parse(node.Attribute(Stats.WILLPOWER).Value);
            this.intelligence = int.Parse(node.Attribute(Stats.INTELLIGENCE).Value);
            this.level = int.Parse(node.Attribute("level").Value);

            if (node.Element("Weapon") != null)
            {
                this.weapon = new Weapon(node.Element("Weapon"));
            }
            //this.weapon = new Weapon(node.
            // TODO:
            // deserialize money
            this.money = new Money(0);
            //XElement moneyNode = new XElement("money");
            //money.Serialize(ref moneyNode);
            //node.Add(moneyNode);
            //node.Add(new XAttribute("Weapon", "TODO"));
            if (node.Element("weapon") != null)
            {
                this.weapon = new Weapon(node.Element("weapon"));
            }
            tactics = new Dictionary<string, string>();
            XElement tacticsElement = node.Element("tactics");
            foreach (XElement tacticElement in tacticsElement.Elements("tactic"))
            {
                string name = tacticElement.Attribute("name").Value;
                string reference = tacticElement.Attribute("reference").Value;
                this.AddTactic(name, reference);
            }
            
        }

        public int GetStat(string stat)
        {
            switch(stat)
            {
                case Stats.MAXHP:
                    return maxhp;
                case Stats.HP:
                    return hp;
                case Stats.STRENGTH:
                    return strength;
                case Stats.DEXTERITY:
                    return dexterity;
                case Stats.AIM:
                    return aim;
                case Stats.WILLPOWER:
                    return willpower;
                case Stats.INTELLIGENCE:
                    return intelligence;
                case Stats.CHARISMA:
                    return charisma;
                default:
                    return 0;
            }
        }

        private int CalcTotalArmour()
        {
            return 1;
        }

        public override CombatResult CalculateDamage(AbstractCombatEntity attacker, Damage damage)
        {
            // TODO
            // types not accounted for yet

            int amount = damage.Amount;
            amount = amount - CalcTotalArmour();
            return new CombatResult(attacker, this, amount);
            
        }

        public CombatResult CalculateWeaponAttack(AbstractCreature enemy, AbstractEncounter encounter)
        {
            Weapon weap = enemy.Weapon;
            int damage = 0;
            if (weap == null)
            {
                damage = enemy.strength;
            }
            else
            {
                damage = enemy.Weapon.Damage;
                int variance = Rand.Instance.NextInt(enemy.Weapon.Variance);
                damage += variance;
                int pStat = enemy.GetStat(enemy.Weapon.PrimaryStat);
                int statBoost = (int)(((float)pStat / 100f) * (float)damage);
                damage += statBoost;
                float crit = (float)Rand.Instance.NextInt(100);
                if (crit < enemy.Weapon.CriticalChance)
                {
                    damage = (int)((float)damage * enemy.Weapon.CriticalMultiplier);
                }
            }
            // todo: get types from weapon!
            Damage d = new Damage(damage, DamageTypes.PIERCE);
            CombatResult prelim = this.CalculateDamage(enemy, d);
            // process final..
            CombatResult final = encounter.ActiveCards.ResolveDamageModifiers(prelim) as CombatResult;
            return final;
        }

        public override CombatResult ApplyCombatResult(CombatResult cr)
        {
            this.hp -= cr.Damage;
            bool killed = false;

            if (this.hp < 0)
            {
                this.hp = 0;
                killed = true;
            }
            cr.Killed = killed;
            return cr;
        }

        // instant kill!
        public void Kill()
        {
            this.hp = 0;
        }

        public void RegainHP(int p)
        {
            hp = hp + p;
            if (hp > maxhp)
                hp = maxhp;
        }

        #region persistence

        public override void Serialize(ref XElement node)
        {
            node.Add(new XAttribute("name", name));
            node.Add(new XAttribute(Stats.HP, hp));
            node.Add(new XAttribute(Stats.MAXHP, maxhp));
            node.Add(new XAttribute(Stats.AIM, aim));
            node.Add(new XAttribute(Stats.CHARISMA, charisma));
            node.Add(new XAttribute(Stats.DEXTERITY, dexterity));
            node.Add(new XAttribute(Stats.INTELLIGENCE, intelligence));
            node.Add(new XAttribute(Stats.STRENGTH, strength));
            node.Add(new XAttribute(Stats.WILLPOWER, willpower));
            node.Add(new XAttribute("level", level));
            XElement moneyNode = new XElement("money");
            money.Serialize(ref moneyNode);
            node.Add(moneyNode);
            if (weapon != null)
            {
                XElement weaponElement = new XElement("Weapon");
                weapon.Serialize(ref weaponElement);
                node.Add(weaponElement);
            }

            XElement tacticsElement = new XElement("tactics");
            foreach (string s in tactics.Keys)
            {
                XElement tacticElement = new XElement("tactic");
                tacticElement.Add(new XAttribute("name", s));
                tacticElement.Add(new XAttribute("reference", tactics[s]));
                tacticsElement.Add(tacticElement);
            }
            node.Add(tacticsElement);
            SerializeImplementation(ref node);
        }
        
        protected abstract void SerializeImplementation(ref XElement node);
        #endregion

        public abstract CombatResult PerformCombatAction(PersonalCombatEncounter encounter);


        public void AddTactic(string ourName, string reference)
        {
            tactics.Add(ourName, reference);
            if (currentTactic == null)
            {
                SetTactic(ourName);
            }
        }

        public bool SetTactic(string name)
        {
            string reference;
            if (tactics.TryGetValue(name, out reference))
            {
                currentTactic = WorldHub.Instance.Tactics.GetTactic(reference);
                currentTacticName = name;
                return true;
            }
            return false;
        }

    }
}
