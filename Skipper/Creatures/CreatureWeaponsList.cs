﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Creatures
{
    class CreatureWeaponsList
    {
        private IList<Weapon> insectWeapons;
        private IList<Weapon> beastWeapons;
        private IList<Weapon> rockWeapons;
        private IList<Weapon> waterWeapons;

        public IList<Weapon> InsectWeapons { get { return insectWeapons; } }
        public IList<Weapon> BeastWeapons { get { return beastWeapons; } }

        private static IList<string> biteverbs = new List<string>() { "bites", "shreds", "pierces", "pinches" };
        private static IList<string> clawverbs = new List<string>() { "grabs", "cuts", "slices", "crushes" };
        private static IList<string> spitverbs = new List<string>() { "shoots", "squirts", "blasts", "sprays" };
        private static IList<string> cutverbs = new List<string>() { "cuts", "slices", "slashes" }; // maybe need to keep pairs - "stab / stabs, slash/slashes"
        private static IList<string> stabverbs = new List<string>() { "stabs", "pierces", "skewers" };

        public CreatureWeaponsList()
        {
            GenerateInsectWeapons();
        }

        private void GenerateInsectWeapons()
        {
            // please do in roughly order of strength.. ?
            insectWeapons = new List<Weapon>();
            insectWeapons.Add(new Weapon("Mandible", 5, 5, 5, 1.5f, Stats.DEXTERITY, biteverbs));
            insectWeapons.Add(new Weapon("Claw", 8, 4, 5, 1.5f, Stats.DEXTERITY, clawverbs));
            insectWeapons.Add(new Weapon("Poison Gland", 10, 0, 0, 0f, Stats.AIM, spitverbs));
            insectWeapons.Add(new Weapon("Acid Gland", 15, 5, 0, 0, Stats.AIM, spitverbs));
            insectWeapons.Add(new Weapon("Razor Wing", 15, 3, 5, 1.5f, Stats.DEXTERITY, cutverbs));
            insectWeapons.Add(new Weapon("Proboscis", 18, 2, 5, 1.5f, Stats.DEXTERITY, stabverbs));
        }
    }
}
