﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Data;

namespace Skipper.Creatures
{
    class GenericTactics
    {
        public const string GENERIC_ATTACK = "GenericAttack";
        public const string RANDOM_ATTACK = "RandomAttack";
        public const string ATTACK_WEAKEST = "AttackWeakest";
        public const string GENERIC_DEFEND = "GenericDefend";

        [TacticsMethod(GENERIC_DEFEND)]
        public static CombatResult GenericDefend(AbstractCreature source, AbstractCreature[] enemies, AbstractCreature[] friends, PersonalCombatEncounter encounter)
        {
            return new DefenceResult(source, 50);
        }


        [TacticsMethod(GENERIC_ATTACK)]
        public static CombatResult GenericAttack(AbstractCreature source, AbstractCreature[] enemies, AbstractCreature[] friends, PersonalCombatEncounter encounter)
        {
            IList<AbstractCreature> targets = enemies.Where(o => o.GetStat(Stats.HP) > 0).ToList();
            if (targets.Count() > 0)
            {
                // get first enemy with non-0 hp;

                AbstractCreature target = targets.First(o => o.GetStat(Stats.HP) > 0);
                if (target != null && target.GetStat(Stats.HP) > 0)
                {
                    return target.CalculateWeaponAttack(source, encounter);
                    //return new CombatResult(BasicResultText(source, target, damage));
                }
                else
                {
                    return HasNoTarget(source);
                }
            }
            else
            {
                return HasNoTarget(source);
            }
        }

        [TacticsMethod(RANDOM_ATTACK)]
        public static CombatResult RandomAttack(AbstractCreature source, AbstractCreature[] enemies, AbstractCreature[] friends, PersonalCombatEncounter encounter)
        {
            if (enemies.Count() > 0)
            {

                IList<AbstractCreature> pool = enemies.Where(o => o.GetStat(Stats.HP) > 0).ToList();

                if (pool.Count <= 0) return HasNoTarget(source);

                int i = Rand.Instance.NextInt(pool.Count());
                return enemies.ElementAt(i).CalculateWeaponAttack(source, encounter);
            }
            else
            {
                return HasNoTarget(source);
            }
        }

        [TacticsMethod(ATTACK_WEAKEST)]
        public static CombatResult AttackWeakest(AbstractCreature source, AbstractCreature[] enemies, AbstractCreature[] friends, PersonalCombatEncounter encounter)
        {
            if (enemies.Count() > 0)
            {
                // sort by HP then attack first on list with HP > 0
                IList<AbstractCreature> sorted = enemies.OrderBy(o => o.GetStat(Stats.HP)).ToList();
                sorted = sorted.Where(o => o.GetStat(Stats.HP) > 0).ToList();
                if (sorted.Count() <= 0)
                {
                    return HasNoTarget(source);
                }
                return sorted.First().CalculateWeaponAttack(source, encounter);
            }
            else
            {
                return HasNoTarget(source);
            }
        }

        private static string BasicResultText(AbstractCreature a, AbstractCreature b, int damage)
        {
            string resultText = a.Name + " attacks " + b.Name + " for " + damage + " damage.";
            if (b.GetStat(Stats.HP) <= 0)
            {
                resultText += " " + b.Name + " is killed.";
            }
            return resultText;
        }

        public static CombatResult HasNoTarget(AbstractCreature source)
        {
            return new CombatResult(source, null, 0);
        }
    }
}
