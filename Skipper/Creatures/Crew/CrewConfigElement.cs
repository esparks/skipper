﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Skipper.Creatures.Crew
{
    [DataContract(Name = "CrewConfig", Namespace = "http://www.skippergame.com")]
    class CrewConfigElement : CreatureConfiguration
    {
        [DataMember()]
        public string Class;

        [DataMember()]
        public int Age;

        [DataMember()]
        public int Level;

        [DataMember()]
        public string Homeworld;

        [DataMember()]
        public string Origin;

        [DataMember()]
        public string Description;

        [DataMember()]
        public string History;

        [DataMember()]
        public string defaultTactic;
        // skills
        // other stuff..
        public CrewConfigElement()
            : base()
        {
            
        }
    }
}
