﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Data;
using System.Xml.Linq;
using System.Runtime.Serialization;


namespace Skipper.Creatures.Crew
{
    [DataContract(Name = "CrewMember", Namespace = "http://www.skippergame.com")] 
    class CrewMember : AbstractCreature
    {

        [DataMember()]
        private string classname;

        public string Class { get { return this.classname; } }

        public string History { get { return config.History; } }

        public string CurrentTactic { get { return currentTacticName; } }

        [DataMember()]
        public string[] AvailableTactics { get { return tactics.Keys.ToArray(); } }

        // if moving towards more component based, keep this?
        [DataMember()]
        private CrewConfigElement config;

        public int Age { get { return config.Age; } }
        public string Homeworld { get { return config.Homeworld; } }
        public string Origin { get { return config.Origin; } }
        
        private int tacticIndex;
        private List<string> sortedTactics;

        public CrewMember(CrewConfigElement config)
            : base(config)
        {
            this.config = config; // should discard and not keep this in here...
            this.classname = config.Class;
            tactics = config.tactics;
            sortedTactics = tactics.Keys.ToList();
            sortedTactics.Sort();
            tacticIndex = sortedTactics.IndexOf(config.defaultTactic);
            SetTactic(config.defaultTactic);
        }

        public override CombatResult PerformCombatAction(PersonalCombatEncounter encounter)
        {
            AbstractCreature[] enemies = encounter.Enemies;
            AbstractCreature[] friends = WorldHub.Instance.PlayerInfo.AwayTeam;
            return currentTactic(this, enemies, friends, encounter);
        }

        public void NextTactic()
        {
            tacticIndex++;
            if (tacticIndex >= sortedTactics.Count)
            {
                tacticIndex = 0;
            }
            SetTactic(sortedTactics.ElementAt(tacticIndex));
        }

        public void PreviousTactic()
        {
            tacticIndex--;
            if (tacticIndex < 0)
            {
                tacticIndex = sortedTactics.Count - 1;
            }
            SetTactic(sortedTactics.ElementAt(tacticIndex));
        }

        public void Hire() { }
        public void Fire() { }
        public string GetClassDescription() { throw new NotImplementedException();  }
        protected string personality; // perky, aggressive, sullen..
        
        #region persistance

        public CrewMember(XElement node)
        : base (node)
        {
            CrewConfigElement cce = new CrewConfigElement();
            this.classname = node.Attribute("classname").Value;
            cce.History = node.Element("history").Value;
            cce.Age = int.Parse(node.Attribute("age").Value);
            cce.Homeworld = node.Attribute("homeworld").Value;
            this.personality = node.Attribute("personality").Value;
            // rest of CCE == origin etc..
            this.config = cce;
        }

        protected override void SerializeImplementation(ref System.Xml.Linq.XElement node)
        {
            node.Add(new XAttribute("classname", classname));
            XElement xhistory = new XElement("history");
            xhistory.Value = History ?? "unknown";
            node.Add(xhistory);
            node.Add(new XAttribute("age", Age));
            node.Add(new XAttribute("homeworld", Homeworld ?? "unknown"));
            node.Add(new XAttribute("personality", personality ?? "unknown"));
            node.Add(new XAttribute("type", this.GetType().Namespace + "." + this.GetType().Name));
        }
        
        #endregion
    }
}
