﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Items;
using System.Runtime.Serialization;

namespace Skipper.Creatures
{
    /// <summary>
    /// Passed in to AbstractCreature constructor
    /// </summary>
    /// 
    /// TODO: This is actually a step towards component architecture.. keep going!
    /// 
    [DataContract(Name = "CreatureConfig", Namespace = "http://www.skippergame.com")]
    class CreatureConfiguration
    {
        [DataMember()]
        public IDictionary<string, int> stats;
        [DataMember()]
        public IDictionary<string, string> tactics;
        [DataMember()]
        public string Name;
        public Weapon weapon;
        public Money money;
        public IList<AbstractCollectible> items;
        [DataMember()]
        public int level;

        public CreatureConfiguration()
        {
            level = 1;
            stats = new Dictionary<string, int>();
            stats[Stats.HP] = 1;
            stats[Stats.MAXHP] = 1;
            stats[Stats.AIM] = 1;
            stats[Stats.CHARISMA] = 1;
            stats[Stats.DEXTERITY] = 1;
            stats[Stats.INTELLIGENCE] = 1;
            stats[Stats.STRENGTH] = 1;
            stats[Stats.WILLPOWER] = 1;
            tactics = new Dictionary<string, string>();
            Name = "unnamed";
            items = new List<AbstractCollectible>();
        }

    }
}
