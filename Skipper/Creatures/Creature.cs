﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Data;
using Skipper.Encounters;

namespace Skipper.Creatures
{
    class Creature : AbstractCreature
    {
        public Creature(CreatureConfiguration config)
            : base(config)
        {

        }

        public override CombatResult PerformCombatAction(PersonalCombatEncounter encounter)
        {
            //TODO: basic, performs the first attack in the list.
            //more advanced ai's could change their tactics..
            Tactic t = WorldHub.Instance.Tactics.GetTactic(tactics.Values.First());
            if (t == null)
                throw new Exception(this.Name + " did not find tactic " + tactics.Values.First() + " available.");
            return t(this, WorldHub.Instance.PlayerInfo.AwayTeam, encounter.Enemies, encounter);
        }

        protected override void SerializeImplementation(ref System.Xml.Linq.XElement node)
        {
            //
        }
    }
}
