﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper
{
    class Emplacement
    {

        private string name;
        public string Name { get { return name; } private set { name = value; } }

        // todo: this will define what effect a position has ..
        // public StatBonus bonus;//

        public Emplacement(string name)
        {
            this.name = name;
        }
    }
}
