﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    class AsciiViewWindow
    {
        private AsciiDisplayElement[,] grid;
        private int width;
        private int height;

        public int Width { get { return width; } }
        public int Height { get { return height; } }

        public AsciiViewWindow(int w, int h)
        {
            this.width = w;
            this.height = h;
            grid = new AsciiDisplayElement[w, h];

            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    this[x, y] = new AsciiDisplayElement();
                    this[x, y].Icon = ' ';
                    this[x, y].Background = ColorPreset.BLACK;
                    this[x, y].Foreground = ColorPreset.WHITE;
                }
            }
        }

        public void Set(int x, int y, char c, ColorPreset fore, ColorPreset back)
        {
            this[x, y].Icon = c;
            this[x, y].Foreground = fore;
            this[x, y].Background = back;
        }

        public AsciiDisplayElement this[int x, int y]
        {
            get
            {
                // hmm.. better to throw an exception or create a temporary and just discard the write/get?
                //if (x > 0 && x <
                try
                {
                    return grid[x, y];
                }
                catch(IndexOutOfRangeException ioor)
                {
                    Console.WriteLine("index out of range: " + x + "," + y);
                    throw new Exception("index out of range: " + x + "," + y + " not inside " + this.width + "," + this.height);
                }
            }

            set
            {
                grid[x, y] = value;
            }
        }


        public void Clear()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    this[x, y] = new AsciiDisplayElement();
                    this[x, y].Icon = ' ';
                    this[x, y].Background = ColorPreset.BLACK;
                    this[x, y].Foreground = ColorPreset.WHITE;
                }
            }
        }
    }
}
