﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cards;
using Skipper.Encounters;

namespace Skipper.Display.Cards
{
    abstract class AbstractCardOptionsDisplay : AbstractAsciiOptionWindow
    {
        private AbstractCard card;
        private AbstractEncounter encounter;
        private AbstractUsage usage;

        protected AbstractCard Card { get { return card; } }
        protected AbstractEncounter Encounter { get { return encounter; } }
        protected AbstractUsage Usage { get { return usage; } }
        // should be simple options windows that select from a bunch of things..
        public AbstractCardOptionsDisplay(int l, int t, int w, int h, AbstractUsage usage, AbstractEncounter encounter, AsciiViewWindow parent, IRenderer render, IKeyInput input)
            : base(l, t, w, h, usage.Card.Name, parent, render, input)
        {
            this.card = usage.Card;
            this.encounter = encounter;
            //this.window = window;
        }

        //
        //  go into a reading loop, update the display
        //

        public IDictionary<string, object> GetSelection()
        {
            var args = SelectionImplementation();
            this.Restore();
            return args;
        }

        protected abstract IDictionary<string, object> SelectionImplementation();

    }
}
