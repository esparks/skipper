﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cards;
using Skipper.Encounters;
using Skipper.Creatures;

namespace Skipper.Display.Cards
{
    class SelectSingleCreature : AbstractCardOptionsDisplay
    {
        //private PersonalCombatEncounter pce;
        private IList<AbstractCreature> targets;
        int selected = 0;
        int selMax = 0;
        //PersonalCombatEncounter pce;

        public SelectSingleCreature(int l, int t, int w, int h, AbstractUsage usage, AbstractEncounter encounter, AsciiViewWindow parent, IRenderer render, IKeyInput input)
            : base(l, t, w, h, usage, encounter, parent, render, input)
        {
            ITargetSingleCreature isel = (ITargetSingleCreature)usage;
            this.targets = isel.GetTargets();
            int newH = 3 + targets.Count() + 2;
            Resize(l, t, w, newH);
            selMax = targets.Count();

            if (targets.Count() > 0)
            {
                AsciiTools.Write(this.X + 1, this.Y + 1, "Select target:", ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
                DrawList();
            }
            else
            {
                AsciiTools.Write(this.X + 1, this.Y + 1, "NO TARGETS", ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            }
        }

        private void DrawList()
        {

            int j = 0;
            int i = 2;

            foreach (AbstractCreature c in targets)
            {
                ColorPreset fore = ColorPreset.WHITE;
                ColorPreset back = ColorPreset.BLACK;
                if (j == selected)
                {
                    fore = ColorPreset.SELECTED_OPTION_FG;
                    back = ColorPreset.SELECTED_OPTION_BG;
                }

                AsciiTools.Write(this.X + 1, this.Y + i++, c.Name + ": " + c.GetStat(Stats.HP) + "/" + c.GetStat(Stats.MAXHP), fore, back, Window);
                j++;
            }

        }

        protected override IDictionary<string, object> SelectionImplementation()
        {
            bool selection = false;

            if (targets.Count() < 1)
            {
                this.Input.ReadKeyBlocking();
                return null;
            }

            while (selection == false)
            {
                IKeyInfo key = this.Input.ReadKeyBlocking();
                if (key.IsSpecial(SpecialKeys.UP))
                {
                    selected--;
                }
                else if (key.IsSpecial(SpecialKeys.DOWN))
                {
                    selected++;
                }
                else if (key.IsSpecial(SpecialKeys.ENTER))
                {
                    IDictionary<string, object> args = new Dictionary<string, object>();
                    args.Add(CardArguments.TARGET, targets.ElementAt(selected));
                    selection = true;
                    return args;
                }

                if (selected < 0)
                {
                    selected = selMax - 1; 
                }
                
                if (selected >= selMax)
                {
                    selected = 0;
                }
                DrawList();
                Render.Render(0, 0, Window);
            }
            return null;
        }
    }
}
