﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Creatures;
using Skipper.Creatures.Crew;
using Skipper.Data;
using Skipper.Cards;

namespace Skipper.Display.EncounterViews
{
    class AsciiPersonalCombatDisplay : AbstractCustomView
    {
        //PersonalCombatEncounter encounter;

        int selection = 0;
        bool showingResult;

        public AsciiPersonalCombatDisplay(int l, int t, int w, int h, IRenderer render, IKeyInput input)
            : base(l, t, w, h, render, input)
        {
            showingResult = false;
        }

        protected override Encounters.AbstractEncounter Process(IKeyInfo key, Encounters.AbstractEncounter encounter)
        {
            PersonalCombatEncounter pce = (PersonalCombatEncounter)encounter;


            if (showingResult)
            {
                showingResult = false;
                return pce.AdvanceEncounter();
            }

            if (pce.Enemies.Count() > 0)
            {
                //TODO: access the current AwayTeam through the encounter
                AbstractCreature[] team = WorldHub.Instance.PlayerInfo.AwayTeam;
                //throw new NotImplementedException();
                if (key.IsSpecial(SpecialKeys.LEFT) || key.Key() == 'a')
                {
                    // select--
                    selection--;
                }

                if (key.IsSpecial(SpecialKeys.RIGHT) || key.Key() == 'd')
                {
                    selection++;
                }

                if(key.IsSpecial(SpecialKeys.ENTER) || key.Key() == 'e')
                {
                    // execute all tactics!!
                    // update after each...
                    pce.ExecuteCombat();
                    return pce.AdvanceEncounter();
                }

                if(key.IsSpecial(SpecialKeys.UP) || key.Key() == 'w')
                {
                    ((CrewMember)team[selection]).PreviousTactic();
                }

                if (key.IsSpecial(SpecialKeys.DOWN) || key.Key() == 's')
                {
                    ((CrewMember)team[selection]).NextTactic();
                }

                if (key.Key() == 'q')
                {
                    return pce.Cheat();
                }

                if (selection < 0)
                    selection = team.Count() - 1;
                if (selection >= team.Count())
                    selection = 0;
            }
            else
            {
                ////////// NO ENEMIES ///////////
            }
            return pce.AdvanceEncounter();
        }

        protected override void Display(Encounters.AbstractEncounter encounter)
        {
            // list crew
            PersonalCombatEncounter combat = (PersonalCombatEncounter)encounter;
            int crewNameLen = 12;
            int baseX = 7;
            int x = baseX;
            AbstractCreature player = WorldHub.Instance.PlayerInfo.Player;
            AsciiTools.Write(x, 1, encounter.Name, ColorPreset.ENCOUNTER_NAME, ColorPreset.DEFAULT_BG, Window);
            //AsciiTools.Write(x, 3, "COMMANDER", ColorPreset.WHITE, ColorPreset.DEFAULT_BG, Window);
            //AsciiTools.Write(x, 4, player.GetStat(Stats.HP) + "/" + player.GetStat(Stats.MAXHP), ColorPreset.WHITE, ColorPreset.DEFAULT_BG, Window);

            //get active effect titles!
            IActiveCard[] playerCards = combat.ActiveCards.PlayerActives;
            int cardY = 3;
            int cardX = 60;
            foreach (IActiveCard iac in playerCards)
            {
                // name, rounds..
                //AbstractActiveUsage aau = (AbstractActiveUsage)iac;
                string name = iac.Card.Name;//aau.Card.Name;
                AsciiTools.Write(cardX, cardY, name, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
                if (iac is RoundLimitedUsage)
                {
                    AsciiTools.Write(cardX + 15, cardY, " " + ((RoundLimitedUsage)iac).Rounds, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
                
                }
                cardY++;
            }

            // draw select arrow
            AsciiTools.Write(baseX + (selection * crewNameLen), 6, "^", ColorPreset.CHARACTER_SELECT_ARROW, ColorPreset.DEFAULT_BG, Window);

            AbstractCreature[] team = WorldHub.Instance.PlayerInfo.AwayTeam; 
            //x += crewNameLen;
            foreach(AbstractCreature c in team)
            {
                CrewMember acm = (CrewMember)c;

                string name = "";
                string fname = c.Name.Split(' ').First();
                if (fname.Length < crewNameLen)
                {
                    name = fname.PadRight(crewNameLen);
                } else if (fname.Length > crewNameLen) {
                    name = fname.Substring(0, crewNameLen);
                }
                AsciiTools.Write(x, 3, name, ColorPreset.WHITE, ColorPreset.BLACK, Window);
                AsciiTools.Write(x, 4, c.GetStat(Stats.HP) + "/" + c.GetStat(Stats.MAXHP), ColorPreset.WHITE, ColorPreset.BLACK, Window);
                AsciiTools.Write(x, 5, acm.CurrentTactic, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
                x += crewNameLen;
            }

            // now monsters:)
            int y = 8;
            x = baseX;

            if (combat.Enemies.Count() > 0)
            {
                foreach (AbstractCreature c in combat.Enemies)
                {
                    string name = "";
                    string fname = c.Name.Split(' ').First();
                    if (fname.Length < crewNameLen)
                    {
                        name = fname.PadRight(crewNameLen);
                    }
                    else if (fname.Length > crewNameLen)
                    {
                        name = fname.Substring(0, crewNameLen);
                    }
                    AsciiTools.Write(x, y, name, ColorPreset.RED, ColorPreset.BLACK, Window);
                    AsciiTools.Write(x, y + 1, c.GetStat(Stats.HP) + "/" + c.GetStat(Stats.MAXHP), ColorPreset.RED, ColorPreset.BLACK, Window);
                    x += crewNameLen;
                }
            }
            else
            {
                //wait for all messages to be cleared before display this.
                AsciiTools.Write(x, y, "Your enemies are slain. Press any key to continue.", ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            }

            TurnEvent te = combat.GetNextEvent();
            if (te != null)
            {
                // write the result!
                AbstractAsciiOptionWindow message = new SimpleMessageWindow(Width / 2 - 20, Height / 2 - 1, 30, 5, "result", te.ResultText, Window, Renderer, Input);

                // set flag 
                showingResult = true;
                // 
            }
        }
    }
}
