﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters.Ruins;
using Skipper.Creatures.Crew;

namespace Skipper.Display.EncounterViews
{
    class AsciiMazeDisplay : AbstractCustomView
    {
        public AsciiMazeDisplay(int x, int y, int w, int h, IRenderer renderer, IKeyInput input)
            : base(x, y, w, h, renderer, input)
        {
            //secctor
        }


        protected override Encounters.AbstractEncounter Process(IKeyInfo key, Encounters.AbstractEncounter encounter)
        {
            RuinsMazeEncounter rme = encounter as RuinsMazeEncounter;
            //throw new NotImplementedException();
            if (key.Key() == 'd')
            {
                Console.WriteLine("MOVE PLAYER!!");
            }

            if (key.Key() == 'q')
            {
                // boot to nav view!
                return null;
            }

            if (key.IsSpecial(SpecialKeys.UP))
            {
                return rme.MovePlayer(new Vector2i(0, -1));
            }

            if (key.IsSpecial(SpecialKeys.DOWN))
            {
                return rme.MovePlayer(new Vector2i(0, 1));
            }

            if (key.IsSpecial(SpecialKeys.LEFT))
            {
                return rme.MovePlayer(new Vector2i(-1, 0));
            }

            if (key.IsSpecial(SpecialKeys.RIGHT))
            {
                return rme.MovePlayer(new Vector2i(1, 0));
            }



            // return rme.MovePlayer()...
            return encounter;
        }

        protected override void Display(Encounters.AbstractEncounter encounter)
        {

            //TODO: put player in centre and scroll around to allow arbitrary map sizes..
            RuinsMazeEncounter rme = (RuinsMazeEncounter)encounter;
            int pX = 2;
            int pY = 3;

            Maze maze = rme.Maze;
            MazeTile[,] tiles = maze.Tiles;

            int sizeX = rme.Width;
            int sizeY = rme.Height;

            
            Vector2i playerPos = rme.GetPlayerPosition();

            // offset px/py

            /// moving screen now not player!
            int playX = playerPos.x + pX;
            int playY = playerPos.y + pY;            
            int width = 40;
            int height = 24;
            int offX = pX + playerPos.x - (width / 2);
            int offY = pY + playerPos.y - (height / 2);
            AsciiTools.DrawDoubleLineBox(pX - 1, pY - 1, width + 2, height + 2, "RUINS", ColorPreset.WINDOW_BORDER, ColorPreset.BLACK, Window);

            int col2 = pX + width + 5;
            int col3 = col2 + 25;
            int statsY = pY;

            foreach (CrewMember cm in WorldHub.Instance.PlayerInfo.AwayTeam)
            {
                AsciiTools.Write(col2, statsY, cm.Name , ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
                AsciiTools.Write(col3, statsY, cm.GetStat(Stats.HP) + "/" + cm.GetStat(Stats.MAXHP), ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
                statsY++;
            }


            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int tx = x + offX;
                    int ty = y + offY;

                    char t = ' ';

                    ColorPreset bg;
                    ColorPreset fg;

                    if (tx < 0 || ty < 0 || tx >= sizeX || ty >= sizeY)
                    {
                        t = ' ';
                        bg = ColorPreset.BLACK;
                        fg = ColorPreset.BLACK;
                    }
                    else
                    {
                        if (tiles[tx, ty].IsWall)
                            t = ' ';

                        if (tiles[tx, ty].code == 99) //pathgen
                            t = ' ';

                        if (tiles[tx, ty] is StairTile)
                        {
                            t = '=';
                        }
                        //t = (char)(tiles[x, y].code + 97);

                        if (tiles[tx, ty].IsWall)
                        {
                            bg = ColorPreset.MAZE_WALL_BG;
                            fg = ColorPreset.MAZE_WALL_FG;
                        }
                        else
                        {
                            bg = ColorPreset.MAZE_FLOOR_BG;
                            fg = ColorPreset.MAZE_FLOOR_FG;
                        }
                        //Console.WriteLine("drawing item at : " + tx + " : " + ty);
                        //Window.Set(pX + x, pY + y, 'i', ColorPreset.ASTEROID, bg);
                        // new method!
                        if (tiles[tx, ty].tileEvent != null)
                        {
                            MapTileEvent mte = tiles[tx, ty].tileEvent;
                            if (mte is MapItemStack)
                            {
                                t = 'i';
                                fg = ColorPreset.ASTEROID;
                                if (tx == playerPos.x && ty == playerPos.y)
                                {
                                    AddPickupOption((MapItemStack)mte, col2, statsY);
                                }
                            }
                            else if (mte is MapLockedDoor)
                            {
                                t = '#';
                                fg = ColorPreset.ASTEROID;
                            }
                        }
                    }
                    Window.Set(pX + x, pY + y, t, fg, bg);
                }
            }

            // stick to centre...
            Window.Set(width / 2, height / 2, '@', ColorPreset.WHITE, Window[width / 2, height / 2].Background);
            AsciiTools.Write(10, height + pY + 2, "px: " + playerPos.x + " py: " + playerPos.y, ColorPreset.WHITE, ColorPreset.BLACK, Window);
        }

        private void AddPickupOption(MapItemStack stack, int column, int row)
        {   
            int itemid = 1;
            foreach (var item in stack.items)
            {
                AsciiTools.Write(column, row, itemid + ". " + item.item.Name, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            }
        }
    }
}
