﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Creatures.Crew;

namespace Skipper.Display.EncounterViews
{
    class AsciiCrewMemberDisplay : AsciiOptionsView
    {
        public AsciiCrewMemberDisplay(int left, int top, int width, int height, IRenderer renderer, IKeyInput input)
            : base(left, top, width, height, renderer, input)
        {

        }

        protected override void Display(AbstractEncounter encounter)
        {
            ViewCrewStatsEncounter view = (ViewCrewStatsEncounter)encounter;
            CrewMember cm = view.GetMember();

            int y = 2;
            int x_1 = 2;
            int x_2 = 40;
            AsciiTools.Write(x_1, y++, "Name: " + cm.Name, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "Age: " + cm.Age, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "Origin: " + cm.Origin, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "Career: " + cm.Class, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "HP: " + cm.GetStat(Stats.HP) + "/" + cm.GetStat(Stats.MAXHP), ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "AIM: " + cm.GetStat(Stats.AIM), ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "CHA: " + cm.GetStat(Stats.CHARISMA), ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "DEX: " + cm.GetStat(Stats.DEXTERITY), ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "INT: " + cm.GetStat(Stats.INTELLIGENCE), ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "STR: " + cm.GetStat(Stats.STRENGTH), ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "WIL: " + cm.GetStat(Stats.WILLPOWER), ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x_1, y++, "Equipped: " + cm.Weapon.Name, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            // options also
            DisplayOptions(x_1, y, encounter);
        }
    }
}
