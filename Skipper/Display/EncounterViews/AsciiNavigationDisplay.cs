﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Map;
using Skipper.Data;

namespace Skipper.Display.EncounterViews
{
    class AsciiNavigationDisplay : AbstractCustomView
    {

        private const int sectorWinX = 40;
        private const int sectorWinY = 2;
        // fix these to use from the WorldHub Sector Instance!!!
        private const string sectorTitle = "SECTOR";

        private const char shipChar = (char)16;

        public AsciiNavigationDisplay(int x, int y, int w, int h, IRenderer renderer, IKeyInput input)
            : base(x, y, w, h, renderer, input)
        {
            //secctor
        }

        protected override void Display(AbstractEncounter encounter)
        {
            Ship ship = WorldHub.Instance.PlayerInfo.Ship;
            SectorMap map = WorldHub.Instance.SectorMap;
            NavigationEncounter nav = (NavigationEncounter)encounter;
            int sectorWid = WorldHub.Instance.SectorMap.SizeX;
            int sectorHeight = WorldHub.Instance.SectorMap.SizeY;
            int viewBaseX = sectorWinX + 1;
            int viewBaseY = sectorWinY + 1;
            int viewMaxX = sectorWinX + sectorWid - 1;
            int viewMaxY = sectorWinY + sectorHeight - 1;
        
            AsciiTools.Write(5, 5, "WASD MOVE", ColorPreset.WHITE, ColorPreset.BLACK, Window);
            AbstractEncounter at = map[ship.SectorX, ship.SectorY].BaseEncounter;
            if (at != null)
            {
                AsciiTools.Write(5, 7, "AT: " + map[ship.SectorX, ship.SectorY].BaseEncounter.Name, ColorPreset.WHITE, ColorPreset.BLACK, Window);
                AsciiTools.Write(5, 8, "    (V)isit", ColorPreset.WHITE, ColorPreset.BLACK, Window);
                AsciiTools.Write(5, 9, "    (R)esearch", ColorPreset.UNAVAILABLE, ColorPreset.BLACK, Window);
                AsciiTools.Write(5, 10, "    (M)ark", ColorPreset.UNAVAILABLE, ColorPreset.BLACK, Window);
            }
            else
            {
                AsciiTools.Write(5, 7, "AT: Deep Space", ColorPreset.WHITE, ColorPreset.BLACK, Window);
                AsciiTools.Write(5, 8, "    (V)isit", ColorPreset.UNAVAILABLE, ColorPreset.BLACK, Window);
                AsciiTools.Write(5, 9, "    (R)esearch", ColorPreset.UNAVAILABLE, ColorPreset.BLACK, Window);
                AsciiTools.Write(5, 10, "    (M)ark", ColorPreset.UNAVAILABLE, ColorPreset.BLACK, Window);
            }
            AsciiTools.Write(5, 11, "    (C)argo", ColorPreset.WHITE, ColorPreset.BLACK, Window);

            AsciiTools.DrawDoubleLineBox(sectorWinX, sectorWinY, sectorWid, sectorHeight, sectorTitle, ColorPreset.WINDOW_BORDER, ColorPreset.BLACK, Window);
            AsciiTools.Fill(' ', sectorWinX + 1, sectorWinY + 1, sectorWid - 2, sectorHeight - 2, ColorPreset.WHITE, ColorPreset.STAR_BG, Window);
            //AsciiTools.Write(sectorWinX, sectorWinY + 1, "123456789ABCDEFGHIJK", ConsoleColor.White, ConsoleColor.DarkBlue, Window);

            for (int x = 0; x < sectorWid - 2; x++)
            {
                for (int y = 0; y < sectorHeight - 2; y++)
                {
                    AbstractEncounter ae = WorldHub.Instance.SectorMap[x, y].BaseEncounter;
                    AsciiDisplayElement elem = AsciiSectorDisplay.GetDisplayElement(ae);
                    Window[x + viewBaseX, y + viewBaseY].Icon = elem.Icon;
                    Window[x + viewBaseX, y + viewBaseY].Foreground = elem.Foreground;
                }
            }

            DrawShip(viewBaseX, viewBaseY);
        }

        private void DrawShip(int viewBaseX, int viewBaseY)
        {
            Ship ship = WorldHub.Instance.PlayerInfo.Ship;

            int x = viewBaseX + ship.SectorX;
            int y = viewBaseY + ship.SectorY;
            Window.Set(x, y, shipChar, ColorPreset.PLAYER_SHIP, ColorPreset.STAR_BG);
        }

        protected override AbstractEncounter Process(IKeyInfo key, AbstractEncounter encounter)
        {
            Ship ship = WorldHub.Instance.PlayerInfo.Ship;
            SectorMap sector = WorldHub.Instance.SectorMap;
            //input.ReadKeyBlocking();

            if (key.IsSpecial(SpecialKeys.UP))
            {
                ship.SetPosition(sector, ship.SectorX, ship.SectorY - 1);
            }

            if (key.IsSpecial(SpecialKeys.DOWN))
            {
                ship.SetPosition(sector, ship.SectorX, ship.SectorY + 1);
            }

            if (key.IsSpecial(SpecialKeys.LEFT))
            {
                ship.SetPosition(sector, ship.SectorX - 1, ship.SectorY);
            }

            if (key.IsSpecial(SpecialKeys.RIGHT))
            {
                ship.SetPosition(sector, ship.SectorX + 1, ship.SectorY);
            }

            switch (key.Key())
            {
                case 'w':
                    ship.SetPosition(sector, ship.SectorX, ship.SectorY - 1);
                    break;
                case 's':
                    ship.SetPosition(sector, ship.SectorX, ship.SectorY + 1);
                    break;
                case 'a':
                    ship.SetPosition(sector, ship.SectorX - 1, ship.SectorY);
                    break;
                case 'd':
                    ship.SetPosition(sector, ship.SectorX + 1, ship.SectorY);
                    break;
                case 'v':
                    AbstractEncounter ae = sector[ship.SectorX, ship.SectorY].BaseEncounter;
                    if (ae != null)
                        return ae;
                    break;
                case 'c':
                    AbstractEncounter vi = new ViewInventoryEncounter();
                    return vi;
                    break;
                default:
                    break;
            }
            //TODO: Remove, replace with ships on map etc.
            bool combat = Rand.Instance.NextBool(5);
            if (combat)
            {
                return new SpaceCombatEncounter(encounter);
            }
            return encounter;
        }
    }
}
