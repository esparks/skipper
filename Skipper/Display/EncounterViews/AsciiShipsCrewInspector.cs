﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Creatures.Crew;

namespace Skipper.Display.EncounterViews
{
    class AsciiShipsCrewInspector : AbstractCustomView
    {
        //TODO: Not finished or tied to view mapping yet!!!

        private ViewShipsCrewEncounter viewEncounter;
        //private IList<CrewMember> crew;

        public AsciiShipsCrewInspector(int x, int y, int w, int h, IRenderer renderer, IKeyInput input)
            : base(x, y, w, h, renderer, input)
        {

        }

        protected override Encounters.AbstractEncounter Process(IKeyInfo key, Encounters.AbstractEncounter encounter)
        {
            // displa
            if (key.Key() == 'q')
                return viewEncounter.Quit();

            return encounter;
        }

        protected override void Display(Encounters.AbstractEncounter encounter)
        {
            int y = 2;
            int x = 5;
            foreach (CrewMember cm in WorldHub.Instance.PlayerInfo.Ship.Crew)
            {
                AsciiTools.Write(x, y++, cm.Name, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
            }
        }
    }
}
