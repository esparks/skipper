﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;

namespace Skipper.Display.EncounterViews
{
    class AsciiSpaceCombatView : AbstractCustomView
    {

        private int viewWidth;
        private int viewHeight;
        private int borderWidth;
        private int borderHeight;

        private int viewX = 1;
        private int viewY = 1;
        private char[,] stars;

        private AsciiSprite playerShip;
        private AsciiSprite enemShip;

        public AsciiSpaceCombatView(int left, int top, int width, int height, IRenderer renderer, IKeyInput input)
            : base (left, top, width, height, renderer, input)
        {
            playerShip = new SimpleShipSprite(SimpleShipSprite.Orientation.RIGHT);
            enemShip = new SimpleShipSprite(SimpleShipSprite.Orientation.LEFT);
            borderWidth = Window.Width - 20;
            borderHeight = Window.Height - 11; //TODO: Reference cardheight..
            viewWidth = borderWidth - 2;
            viewHeight = borderHeight - 2;
            viewX = 2;
            viewY = 3;

            stars = new char[viewWidth, viewHeight];
            for (int x = 0; x < viewWidth; x++)
            {
                for (int y = 0; y < viewHeight; y++)
                {
                    bool star = Rand.Instance.NextBool(8);
                    if (star)
                    {
                        stars[x, y] = '.';
                    }
                    else
                    {
                        stars[x, y] = ' ';
                    }
                }
            }
        }

        protected override Encounters.AbstractEncounter Process(IKeyInfo key, Encounters.AbstractEncounter encounter)
        {
            // wait //
            //char c = input

            Console.WriteLine("key.Key() == " + key.Key());

            if (key.Key() == 'q')
            {
                return encounter.End();
            }
            return encounter;
        }

        protected override void Display(Encounters.AbstractEncounter encounter)
        {
            AsciiTools.DrawDoubleLineBox(viewX, viewY, borderWidth, borderHeight, null, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);

            DrawStars();
            DrawDrones(encounter);
            //TODO: make these relative
            // and dynamic!
            playerShip.Draw(5, 10, Window);
            enemShip.Draw(viewWidth - 12, 15, Window);
        }

        private void DrawStars()
        {
            for (int x = 0; x < viewWidth; x++)
            {
                for (int y = 0; y < viewHeight; y++)
                {
                    Window.Set(x + viewX + 1, y + viewY + 1, stars[x, y], ColorPreset.WHITE, ColorPreset.STAR_BG);
                }
            }
        }

        private void DrawDrones(AbstractEncounter encounter)
        {
            IList<SpaceCombatEncounter.Drone> drones = ((SpaceCombatEncounter)encounter).GetDrones();
            foreach (SpaceCombatEncounter.Drone drone in drones)
            {
                int x = viewX + drone.x + 1;
                int y = viewY + drone.y + 1;
                ColorPreset fg = ColorPreset.ASTEROID;
                if (drone.icon == '>')
                {
                    fg = ColorPreset.UNSELECTED_CARD_FG;
                }
                else if (drone.icon == '<')
                {
                    fg = ColorPreset.RED;
                }

                Window.Set(x, y, drone.icon, fg, /*Window[x, y].Background*/ ColorPreset.BLACK);
            }
        }
    }
}
