﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display.EncounterViews
{
    class AsciiOptionsView : AbstractCustomView
    {
        //private AbstractEncounter encounter;
        private int posX;
        private int posY;
        private Stack<ColorPair> colorStack;
        private Stack<v2d> cursorStack;

        private int optx;
        private int opty;

        private struct ColorPair 
        {
            public ColorPreset fore;
            public ColorPreset back;
        }

        public AsciiOptionsView(int x, int y, int w, int h, IRenderer renderer, IKeyInput input)
            : base(x, y, w, h, renderer, input)
        {
            cursorStack = new Stack<v2d>();
            colorStack = new Stack<ColorPair>();
        }

        protected override void Display(Encounters.AbstractEncounter encounter)
        {

            string[] options = encounter.Options;
            v2d cursor = new v2d();
            cursor.x = 0;
            cursor.y = 1;
            cursorStack.Push(cursor);

            ColorPair pen = new ColorPair();
            pen.fore = ColorPreset.DEFAULT_TEXT_FG;
            pen.back = ColorPreset.DEFAULT_BG;
            colorStack.Push(pen);

            //DrawViewOptions();
            //PushColors();
            //Console.ForegroundColor = ConsoleColor.Cyan;
            
            pen.fore = ColorPreset.ENCOUNTER_NAME;
            AsciiTools.Write(cursor.x, cursor.y, encounter.Name, pen.fore, pen.back, Window);

            pen = colorStack.Peek();
            cursor.y += 2;
            // split into lines
            IList<string> desclist = AsciiTools.SplitString(encounter.Description, Window.Width);

            //Consol
            foreach(string s in desclist)
            {
                //Console.WriteLine(s);
                //Console.ReadKey();
                AsciiTools.Write(cursor.x, cursor.y++, s, pen.fore, pen.back, Window);

            }

            cursor.y += 2;
            cursorStack.Push(cursor);

            // may not need..
            optx = cursor.x;
            opty = cursor.y;
            
            colorStack.Push(pen);
            DisplayOptions(cursor.x, cursor.y, encounter);   
            pen = colorStack.Pop();
        }

        protected void DisplayOptions(int x, int y, Encounters.AbstractEncounter encounter)
        {
            ColorPair pen = new ColorPair();
            pen.fore = ColorPreset.UNSELECTED_OPTION_FG;
            pen.back = ColorPreset.UNSELECTED_OPTION_BG;

            foreach (string option in encounter.Options)
                AsciiTools.Write(x, y++, " " + option, pen.fore, pen.back, Window);
            v2d pos = new v2d();
            pos.x = x;
            pos.y = y;
            cursorStack.Push(pos);
        }

        protected override Encounters.AbstractEncounter Process(IKeyInfo key, Encounters.AbstractEncounter encounter)
        {
            // if key in options, do, mark, return this. on next keypress return result.
            if (encounter.ValidOption(key.Key()))
            {
                string text = "";
                Encounters.AbstractEncounter newEnc = encounter.Update(key.Key(), out text);
                if (!string.IsNullOrEmpty(text))
                {
                    IList<string> tlist = AsciiTools.SplitString(text, Width);
                    v2d cursor = cursorStack.Pop();
                    cursor.y++;

                    foreach (string s in tlist)
                    {
                        AsciiTools.Write(cursor.x, cursor.y++, s, ColorPreset.DEFAULT_TEXT_FG, ColorPreset.DEFAULT_BG, Window);
                    }
                    AsciiTools.Write(cursor.x + 5, cursor.y++, "Press Any Key", ColorPreset.ANY_KEY_DEFAULT, ColorPreset.DEFAULT_BG, Window);
                    // get rid of the ints? // probably not an issue since we have left and top;
                    Renderer.Render(Left, Top, Window);
                    Input.ReadKeyBlocking();
                }
                return newEnc;
            }
            return encounter;
        }
    }
}
