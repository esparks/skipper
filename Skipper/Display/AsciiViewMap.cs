﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Display.EncounterViews;

namespace Skipper.Display
{
    class AsciiViewMap
    {
        private IDictionary<Type, Type> map;
        private IRenderer renderer;
        private IKeyInput input;
        private Type[] args;

        public AsciiViewMap(IRenderer renderer, IKeyInput input)
        {
            map = new Dictionary<Type, Type>();
            map.Add(typeof(Encounters.NavigationEncounter), typeof(AsciiNavigationDisplay));
            map.Add(typeof(Encounters.PersonalCombatEncounter), typeof(AsciiPersonalCombatDisplay));
            map.Add(typeof(Encounters.ViewCrewStatsEncounter), typeof(AsciiCrewMemberDisplay));
            map.Add(typeof(Encounters.SpaceCombatEncounter), typeof(AsciiSpaceCombatView));
            map.Add(typeof(Encounters.Ruins.RuinsMazeEncounter), typeof(AsciiMazeDisplay));
            //map.Add(typeof(Encounters.
            args = new Type[] {typeof(int),typeof(int),typeof(int),typeof(int),typeof(IRenderer),typeof(IKeyInput)};

            this.renderer = renderer;
            this.input = input;
        }

        public ICustomView GetView(Type t)
        {
            if (map.ContainsKey(t))
            {
                object[] objects = new object[] { 0, 0, renderer.GetWidth(), renderer.GetHeight(), renderer, input };
                return (ICustomView)map[t].GetConstructor(args).Invoke(objects);
            }
            return new AsciiOptionsView(0, 0, renderer.GetWidth(), renderer.GetHeight(), renderer, input); // replace with calls to renderer
        }

    }
}
