﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Cards;
using Skipper.Creatures;
using Skipper.Display.Cards;
using Skipper.Encounters;

namespace Skipper.Display
{
    abstract class AbstractCustomView : ICustomView
    {
        protected struct v2d
        {
            public int x;
            public int y;
        }

        private AsciiViewWindow window;
        protected AsciiViewWindow Window { get { return window; } }

        private int left;
        private int top;
        private int width;
        private int height;

        public int Left { get { return left; } }
        public int Top { get { return top; } }
        public int Width { get { return width; } }
        public int Height { get { return height; } }

        /// <summary>
        ///  oops, should have cursor stack in here. move from 
        /// </summary>
        private v2d cursor;


        protected bool drawHand;
        protected bool selectHand;

        protected bool drawStats;
        protected bool readBlocking;

        private int cardWidth;
        private int statLineHeight;
        private readonly int cardY;
        private int cardHeight;
        private int cardLeft;

        private IRenderer renderer;
        private IKeyInput input;
        protected IRenderer Renderer { get { return renderer; } }
        protected IKeyInput Input { get { return input; } }

        // move this into WorldHub//Player
        private static int selectedCard = 0;

        public AbstractCustomView(int left, int top, int width, int height, IRenderer renderer, IKeyInput input)
        {
            this.renderer = renderer;
            this.input = input;
            this.left = left;
            this.top = top;
            this.width = width;
            this.height = height;
            window = new AsciiViewWindow(width, height);
            drawHand = true;
            selectHand = true;
            drawStats = true;
            readBlocking = true;
            statLineHeight = height - 1;
            cardHeight = 6;
            cardY = statLineHeight - cardHeight;
            cardLeft = 7;
            cardWidth = 11;

            //Window.Clear();
        }

        public void Render(Encounters.AbstractEncounter encounter)
        {
            Window.Clear();
            this.Display(encounter);
            if (drawHand)
            {
                DrawHand();
            }
            if (drawStats)
            {
                DrawPlayerStats();
            }
            DrawViewOptions();
            renderer.Render(left, top, window);

            // pop any messages from the encounter //
            string[] cardMessages = encounter.ActiveCards.GetAllMessages();
            if (cardMessages.Count() > 0)
            {
                foreach (string s in cardMessages)
                {
                    AbstractAsciiOptionWindow w = new SimpleMessageWindow(width / 2 - 20, height / 2 - 5, 40, 10, "CARDMSG", s, Window, renderer, input);
                    Renderer.Render(Left, Top, Window);
                    input.ReadKeyBlocking();
                    w.Restore();
                }
                Renderer.Render(Left, Top, Window);
            }
            
        }

        protected abstract Encounters.AbstractEncounter Process(IKeyInfo key, Encounters.AbstractEncounter encounter);
        protected abstract void Display(Encounters.AbstractEncounter encounter);

        public Encounters.AbstractEncounter ProcessInput(Encounters.AbstractEncounter encounter)
        {
            if (readBlocking) /*in unity readBlock would be something like { while(!anyInput) { yield; } } */
            {
                IKeyInfo key = input.ReadKeyBlocking();

                if (key.IsSpecial(SpecialKeys.F3))
                {
                    //Console.WriteLine("making crew assignments.");
                    return new ViewShipsCrewEncounter(encounter);
                }

                if (key.IsSpecial(SpecialKeys.F12))
                {
                    WorldHub.Instance.Save("TestSaveFile.xml");
                    return encounter;
                }

                if (key.IsSpecial(SpecialKeys.F11))
                {
                    WorldHub.Instance.Load("TestSaveFile.xml");
                    //TODO: Return the actual encounter!
                    return encounter;
                }

                if (selectHand)
                {
                    int handMax = WorldHub.Instance.PlayerInfo.Player.Deck.Hand.Count - 1;
                    bool sel = false;
                    if (key.Key() == ',')
                    {
                        selectedCard--;
                        sel = true;
                    }

                    if (key.Key() == '.')
                    {
                        selectedCard++;
                        sel = true;
                    }
                    
                    if (selectedCard < 0) selectedCard = handMax;
                    if (selectedCard > handMax) selectedCard = 0;
                    if (sel == true)
                    {
                        return encounter;
                    }
                    else if (key.Key() == 'p')
                    {
                        //return encounter;
                        AbstractCreature player = WorldHub.Instance.PlayerInfo.Player;
                        AbstractDeck deck = WorldHub.Instance.PlayerInfo.Player.Deck;
                        AbstractCard card = deck.Hand[selectedCard];
                        if (card.CanUseHere(encounter))
                        {
                            IDictionary<string, object> cardArguments = null; // = CardGui.GatherArguments(cardtype);//  
                            
                            //test hack..
                            AbstractAsciiOptionWindow w = null;
                            AbstractUsage usage = card.GetUsage(AbstractCard.Usage.REFRESH, deck, player, encounter);
                            //usage.UseCard(
                            if (usage is Skipper.Cards.ITargetSingleCreature)
                            {
                                w = new Cards.SelectSingleCreature(width / 2 - 20, height / 2 - 5, 30, 5, usage, encounter, Window, renderer, input);
                                Renderer.Render(Left, Top, Window);
                                cardArguments = ((AbstractCardOptionsDisplay)w).GetSelection();
                                // result will get moved..?
                                // check we didn't cancel or have no targets to select etc...
                                if (cardArguments != null)
                                {
                                    string result = deck.Hand[selectedCard].UseCard(AbstractCard.Usage.REFRESH, player, encounter, deck, cardArguments);
                                    w = new SimpleMessageWindow(width / 2 - 20, height / 2 - 5, 30, 5, "result", result, Window, renderer, input);
                                }
                            }
                            else
                            {
                                string result = deck.Hand[selectedCard].UseCard(AbstractCard.Usage.REFRESH, player, encounter, deck, cardArguments);
                                w = new SimpleMessageWindow(width / 2 - 20, height / 2 - 5, 30, 5, "result", result, Window, renderer, input);
                            }
                            Renderer.Render(Left, Top, Window);
                            input.ReadKeyBlocking();
                            w.Restore();
                            return encounter;
                        }
                        else
                        {
                            AbstractAsciiOptionWindow w = new SimpleMessageWindow(width / 2 - 20, height / 2 - 5, 40, 10, "Error", "You can't use that card here.", Window, renderer, input);
                            Renderer.Render(Left, Top, Window);
                            input.ReadKeyBlocking();
                            w.Restore();
                            return encounter;
                        }
                    }
                    else
                    {
                        return this.Process(key, encounter);
                    }

                }

                return this.Process(key, encounter);
            }
            else
            {
                // careful turning blocking off, could get null!
                return this.Process(null, encounter);
            }
        }


        private void DrawHand()
        {
            int cardX = cardLeft;
            AbstractDeck deck = WorldHub.Instance.PlayerInfo.Player.Deck;
            for (int i = 0; i < deck.Hand.Count; i++)
            {
                int finalY = cardY;
                while (selectedCard >= deck.Hand.Count && selectedCard > 0)
                {
                    selectedCard--;
                }
                bool sel = false;
                AbstractCard card = deck.Hand[i];
                if (selectedCard == i)
                {
                    sel = true;
                    finalY--;
                }
                DrawCard(cardX, finalY, sel, card);
                cardX += cardWidth;
            }
        }

        private void DrawCard(int x, int y, bool selected, AbstractCard card)
        {
            ColorPreset color = ColorPreset.UNSELECTED_CARD_FG;
            if (selected) color = ColorPreset.SELECTED_CARD_FG;

            int lineY = y;
            AsciiTools.Write(x, lineY++, @"  _______  ", color, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x, lineY++, @" /       \  ", color, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x, lineY++, @"|         |", color, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x, lineY++, @"|         |", color, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x, lineY++, @"|         |", color, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x, lineY++, @"|         |", color, ColorPreset.DEFAULT_BG, Window);
            AsciiTools.Write(x, lineY++, @"|         |", color, ColorPreset.DEFAULT_BG, Window);
            lineY = y + 2;
            int namex = x + 2;
            IList<string> cname = card.Name.Split(' ');
            foreach (string s in cname)
            {
                AsciiTools.Write(namex, lineY++, s, color, ColorPreset.DEFAULT_BG, Window);
            }
        }

        private void DrawPlayerStats()
        {
            int rMax = Window.Width;
            int x = 0;
            int y = statLineHeight;
            ColorPreset fore = ColorPreset.STATUS_BAR_FG;
            ColorPreset back = ColorPreset.STATUS_BAR_BG;
            string stats = "";
            AbstractCreature p = WorldHub.Instance.PlayerInfo.Player;
            Ship s = WorldHub.Instance.PlayerInfo.Ship;
            stats += "HULL: " + s.HullPoints;
            stats += " SHIELDS: " + s.ShieldPoints;
            stats += " HP: " + p.GetStat(Stats.HP);
            stats += " DECK: " + p.Deck.CountInDeck;
            stats += "     " + Items.Money.DollarSign + p.Money.Amount;
            stats = stats.PadRight(rMax - 1, ' ');
            AsciiTools.Write(0, statLineHeight, stats, fore, back, Window);
            //Console.MoveBufferArea(rMax - 2, statLineHeight, 1, 1, rMax - 1, statLineHeight);
            //PopColors();
        }

        private void DrawViewOptions()
        {
            int x = 0;
            int y = 0;
            ColorPreset fore = ColorPreset.VIEW_OPTIONS_FG;
            ColorPreset back = ColorPreset.VIEW_OPTIONS_BG;
            string opts = "";
            opts += "F1: CARGO F2: ITEMS F3: CREW";
            // pad
            opts = opts.PadRight(Window.Width);
            AsciiTools.Write(x, y, opts, fore, back, Window);
        }

    }
}
