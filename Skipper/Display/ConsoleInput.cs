﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    class ConsoleInput : IKeyInput
    {
        public ConsoleInput()
        {
            Console.CursorVisible = false;
        }

        IKeyInfo IKeyInput.ReadKeyBlocking()
        {
            ConsoleKeyInfo cKey;// = Console.ReadKey(true);
            //flush out the rest of the buffer
            while (Console.KeyAvailable)
            {
                cKey = Console.ReadKey(true);
            }

            return (new ConsoleKeyWrapper(Console.ReadKey(true)));
        }

        IKeyInfo IKeyInput.ReadCharNoBlock()
        {
            throw new NotImplementedException();
        }

        Boolean IsKeyPressed(char key)
        {
            throw new NotImplementedException();
        }

        Boolean IsKeyPressed(SpecialKeys key)
        {
            throw new NotImplementedException();
        }

        public Boolean IsKeyDown(char c)
        {
            throw new NotImplementedException();
        }

        public Boolean IsKeyDown(SpecialKeys skey)
        {
            throw new NotImplementedException();
        }

    }
}
