﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    public enum SpecialKeys
    {
        F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, ESC, TAB, LEFT, RIGHT, UP, DOWN, ENTER
    };

    interface IKeyInfo
    {
        char Key();
        bool IsSpecial(SpecialKeys key);
        SpecialKeys Special();
    }
}
