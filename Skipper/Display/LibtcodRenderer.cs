﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using libtcod;

namespace Skipper.Display
{
    class LibtcodRenderer : IRenderer
    {
        private IDictionary<ColorPreset, TCODColor> colorMap;

        private int width;
        private int height;
        public int GetWidth() { return width; }
        public int GetHeight() { return height; }

        public LibtcodRenderer(int width, int height)
        {
            this.width = width;
            this.height = height;
            //TCODConsole.setCustomFont("terminal_16x16_noaa.png", (int)TCODFontFlags.LayoutAsciiInColumn);
            TCODConsole.setCustomFont("terminal_16x16_noaa.png", (int)TCODFontFlags.LayoutAsciiInColumn);
            
            
            //TCODConsole.mapAsciiCodesToFont((int)' ', 60, 2, 0);
            int i = 0;
            for (int x = 0; x < 16; x++)
            {
                for (int y = 0; y < 16; y++)
                {

                    //Console.WriteLine("mapping '" + (char)i + " to " + x + " : " + y);
                    TCODConsole.mapAsciiCodeToFont(i, x, y);
                    i++;
                }
            }
            //TCODConsole.mapAsciiCodeToFont((int)'a', 6, 1);

            SetupColorMap();    
            TCODConsole.initRoot(width, height, "Skipper");
            // TCODConsole.fo

        }

        public void Clear()
        {
            TCODConsole.root.setBackgroundColor(TCODColor.black);
            TCODConsole.root.clear();
        }

        public void Render(int x, int y, AsciiViewWindow window)
        {
            Clear();
            //throw new NotImplementedException();
            for (int i = 0; i < window.Width; i++)
            {
                for (int j = 0; j < window.Height; j++)
                {
                    //TCODConsole.root.setBackgroundColor(TCODColor.black);
                   // TCODConsole.root.setForegroundColor(TCODColor.white);
                   // TCODConsole.root.setChar(i, j, window[i, j].Icon);
                    TCODColor fg = TCODColor.white;
                    TCODColor bg = TCODColor.black;
                    TCODColor test = null;
                    if (colorMap.TryGetValue(window[i, j].Foreground, out test))
                    {
                        fg = test;
                    }

                    if (colorMap.TryGetValue(window[i, j].Background, out test))
                    {
                        bg = test;
                    }

                    TCODConsole.root.putCharEx(i, j, window[i, j].Icon, fg, bg);
                }
            }
            TCODConsole.flush();
        }

        private void SetupColorMap()
        {
            // set up the color mapping.
            colorMap = new Dictionary<ColorPreset, TCODColor>();

            colorMap.Add(ColorPreset.ASTEROID, TCODColor.amber);
            colorMap.Add(ColorPreset.BLACK, TCODColor.black);
            colorMap.Add(ColorPreset.DEFAULT_BG, TCODColor.black);
            colorMap.Add(ColorPreset.DEFAULT_TEXT_FG, TCODColor.green);
            colorMap.Add(ColorPreset.PLANET, TCODColor.lightYellow);
            colorMap.Add(ColorPreset.PLAYER_SHIP, TCODColor.cyan);
            colorMap.Add(ColorPreset.SPACE_STATION, TCODColor.white);
            colorMap.Add(ColorPreset.STAR_BG, TCODColor.darkBlue);
            colorMap.Add(ColorPreset.WHITE, TCODColor.white);
            colorMap.Add(ColorPreset.WINDOW_BORDER, TCODColor.grey);
            colorMap.Add(ColorPreset.SELECTED_CARD_FG, TCODColor.yellow);
            colorMap.Add(ColorPreset.UNSELECTED_CARD_FG, TCODColor.green);
            colorMap.Add(ColorPreset.UNSELECTED_OPTION_BG, TCODColor.black);
            colorMap.Add(ColorPreset.UNSELECTED_OPTION_FG, TCODColor.lightBlue);
            colorMap.Add(ColorPreset.SELECTED_OPTION_BG, TCODColor.yellow);
            colorMap.Add(ColorPreset.SELECTED_OPTION_FG, TCODColor.darkRed);
            colorMap.Add(ColorPreset.STATUS_BAR_FG, TCODColor.white);
            colorMap.Add(ColorPreset.STATUS_BAR_BG, TCODColor.blue);
            colorMap.Add(ColorPreset.VIEW_OPTIONS_FG, TCODColor.yellow);
            colorMap.Add(ColorPreset.VIEW_OPTIONS_BG, TCODColor.blue);
            colorMap.Add(ColorPreset.ENCOUNTER_NAME, TCODColor.cyan);
            colorMap.Add(ColorPreset.ANY_KEY_DEFAULT, TCODColor.yellow);
            colorMap.Add(ColorPreset.UNAVAILABLE, TCODColor.darkGrey);
            colorMap.Add(ColorPreset.RED, TCODColor.red);
            colorMap.Add(ColorPreset.CHARACTER_SELECT_ARROW, TCODColor.yellow);
            colorMap.Add(ColorPreset.ALPHA100, TCODColor.blue);
            colorMap.Add(ColorPreset.COMBAT_SHIP_BG, TCODColor.darkGrey);
            colorMap.Add(ColorPreset.COMBAT_SHIP_FG, TCODColor.white);
            colorMap.Add(ColorPreset.MAZE_FLOOR_BG, TCODColor.darkGrey);
            colorMap.Add(ColorPreset.MAZE_FLOOR_FG, TCODColor.yellow);
            colorMap.Add(ColorPreset.MAZE_WALL_BG, TCODColor.grey);
            colorMap.Add(ColorPreset.MAZE_WALL_FG, TCODColor.white);

        }
    }
}
