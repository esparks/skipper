﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;

namespace Skipper.Display
{
    interface ICustomView
    {
        void Render(AbstractEncounter encounter);
        AbstractEncounter ProcessInput(AbstractEncounter encounter);
    }
}
