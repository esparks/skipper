﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    class AsciiTools
    {
        public static void DrawDoubleLineBox(int x, int y, int wid, int height, string title, ColorPreset fore, ColorPreset back, AsciiViewWindow window)
        {
            DrawBox((char)201, (char)187, (char)200, (char)188, (char)186, (char)205, x, y, wid, height, title, fore, back, window);
        }

        public static void DrawBox(char ul, char ur, char ll, char lr, char v, char h, int x, int y, int wid, int height, string title, ColorPreset fore, ColorPreset back, AsciiViewWindow window)
        {
            wid = wid - 1;
            height = height - 1;
            
            for (int i = x + 1; i < x + wid; i++)
            {
                int j = y;
                window[i, j].Icon = h;
                window[i, j].Foreground = fore;
                window[i, j].Background = back;
                j = y + height;
                window[i, j].Icon = h;
                window[i, j].Foreground = fore;
                window[i, j].Background = back;
            }

            for (int j = y + 1; j < y + height; j++)
            {
                int i = x;
                window[i, j].Icon = v;
                window[i, j].Foreground = fore;
                window[i, j].Background = back;
                
                i = x + wid;
                window[i, j].Icon = v;
                window[i, j].Foreground = fore;
                window[i, j].Background = back;
                
            }

            window[x, y].Icon = ul;
            window[x, y].Foreground = fore;
            window[x, y].Background = back;

            window[x, y + height].Icon = ll;
            window[x, y + height].Foreground = fore;
            window[x, y + height].Background = back;
            
            window[x + wid, y].Icon = ur;
            window[x + wid, y].Foreground = fore;
            window[x + wid, y].Background = back;
            
            window[x + wid, y + height].Icon = lr;
            window[x + wid, y + height].Foreground = fore;
            window[x + wid, y + height].Background = back;
            
            // write title
            if (!string.IsNullOrEmpty(title))
            {
                int i = x + (wid / 2) - (title.Length / 2) + 1;
                int j = y;
                Write(i, j, title, fore, back, window);
            }
        }

        public static void Write(int startX, int startY, string text, ColorPreset fore, ColorPreset back, AsciiViewWindow window)
        {
            for (int x = 0; x < text.Length; x++)
            {
                window[startX + x, startY].Icon = text[x];
                window[startX + x, startY].Foreground = fore;
                window[startX + x, startY].Background = back;
            }
        }

        public static void Fill(char f, int x, int y, int wid, int height, ColorPreset fore, ColorPreset back, AsciiViewWindow window)
        {
            for (int i = x; i < x + wid; i++)
            {
                for (int j = y; j < y + height; j++)
                {
                    window[i, j].Icon = f;
                    window[i, j].Background = back;
                    window[i, j].Foreground = fore;
                }
            }
        }

        public static IList<string> SplitString(string text, int maxw)
        {
            IList<string> lines = new List<string>();
            int c = 0;
            string t = "";
            for (int i = 0; i < text.Length; i++)
            {
                if (c >= maxw - 1)
                {
                    // check if char or space....
                    int j = i;
                    while (text[j] != ' ' && j > 0)
                    {
                        j--;
                    }
                    if (j == 0)
                    {
                        // no space, abandon...
                    } else {
                        // remove from t for each j
                        t = t.Substring(0, t.Length - (i - j));
                        if (text[i] != '\n')
                            i = j;
                    }
                    t = t.Trim();
                    lines.Add(t);
                    t = "";
                    c = 0;
                }
                else if (text[i] == '\n')
                {
                    //t = t.Substring(t.Trim();
                    lines.Add(t);
                    t = "";
                    c = 0;
                }

                if (text[i] != '\n')
                    t = t + text[i];
                c++;
            }
            if (t != "")
                lines.Add(t.Trim());
            return lines;
        }

        //public static 

    }
}
