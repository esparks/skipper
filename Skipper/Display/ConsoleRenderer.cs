﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    class ConsoleRenderer : IRenderer
    {
        private IDictionary<ColorPreset, ConsoleColor> map;
        private const int width = 80;
        private const int height = 24;
        public int GetWidth() { return width; }
        public int GetHeight() { return height; } 

        public void Clear()
        {
            Console.Clear();
        }

        public ConsoleRenderer()
        {
            Console.SetWindowSize(width, height);
            Console.SetBufferSize(width, height);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.CursorVisible = false;
            Console.Title = "Skipper";
            Console.Clear();

            // set up the color mapping.
            map = new Dictionary<ColorPreset, ConsoleColor>();

            map.Add(ColorPreset.ASTEROID, ConsoleColor.Yellow);
            map.Add(ColorPreset.BLACK, ConsoleColor.Black);
            map.Add(ColorPreset.DEFAULT_BG, ConsoleColor.Black);
            map.Add(ColorPreset.DEFAULT_TEXT_FG, ConsoleColor.Green);
            map.Add(ColorPreset.PLANET, ConsoleColor.Red);
            map.Add(ColorPreset.PLAYER_SHIP, ConsoleColor.Cyan);
            map.Add(ColorPreset.SPACE_STATION, ConsoleColor.White);
            map.Add(ColorPreset.STAR_BG, ConsoleColor.DarkBlue);
            map.Add(ColorPreset.WHITE, ConsoleColor.White);
            map.Add(ColorPreset.WINDOW_BORDER, ConsoleColor.Gray);
            map.Add(ColorPreset.SELECTED_CARD_FG, ConsoleColor.Yellow);
            map.Add(ColorPreset.UNSELECTED_CARD_FG, ConsoleColor.Green);
            map.Add(ColorPreset.UNSELECTED_OPTION_BG, ConsoleColor.Gray);
            map.Add(ColorPreset.UNSELECTED_OPTION_FG, ConsoleColor.Blue);
            map.Add(ColorPreset.SELECTED_OPTION_BG, ConsoleColor.DarkYellow);
            map.Add(ColorPreset.SELECTED_OPTION_FG, ConsoleColor.DarkRed);
            map.Add(ColorPreset.STATUS_BAR_FG, ConsoleColor.White);
            map.Add(ColorPreset.STATUS_BAR_BG, ConsoleColor.Blue);
            map.Add(ColorPreset.VIEW_OPTIONS_FG, ConsoleColor.Yellow);
            map.Add(ColorPreset.VIEW_OPTIONS_BG, ConsoleColor.Blue);
            map.Add(ColorPreset.ENCOUNTER_NAME, ConsoleColor.Cyan);
            map.Add(ColorPreset.ANY_KEY_DEFAULT, ConsoleColor.DarkYellow);
            map.Add(ColorPreset.UNAVAILABLE, ConsoleColor.DarkGray);
            map.Add(ColorPreset.RED, ConsoleColor.Red);
            map.Add(ColorPreset.CHARACTER_SELECT_ARROW, ConsoleColor.Yellow);
            map.Add(ColorPreset.ALPHA100, ConsoleColor.Black);
            map.Add(ColorPreset.COMBAT_SHIP_BG, ConsoleColor.DarkGray);
            map.Add(ColorPreset.COMBAT_SHIP_FG, ConsoleColor.White);
            map.Add(ColorPreset.MAZE_FLOOR_BG, ConsoleColor.Gray);
            map.Add(ColorPreset.MAZE_FLOOR_FG, ConsoleColor.Yellow);
            map.Add(ColorPreset.MAZE_WALL_BG, ConsoleColor.DarkGray);
            map.Add(ColorPreset.MAZE_WALL_FG, ConsoleColor.White);
        }

        class ConsoleElement
        {
            public ConsoleColor fore;
            public ConsoleColor back;
            public char icon;
        }

        void IRenderer.Render(int left, int top, AsciiViewWindow window)
        {
            int width = window.Width;
            int height = window.Height;
            ConsoleElement[,] grid = new ConsoleElement[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    AsciiDisplayElement elem = window[x, y];
                    if (!elem.dirty)
                    {
                        grid[x, y] = null;
                    }
                    else
                    {
                        ConsoleColor fore;
                        ConsoleColor back;
                        if (!map.TryGetValue(elem.Foreground, out fore))
                        {
                            fore = ConsoleColor.Magenta;
                        }

                        if (!map.TryGetValue(elem.Background, out back))
                        {
                            back = ConsoleColor.White;
                        }
                        //Console.ForegroundColor = fore;
                        //Console.BackgroundColor = back;
                        //Console.Write(elem.icon);
                        //Console.Write(elem.icon);
                        grid[x, y] = new ConsoleElement();
                        grid[x, y].fore = fore;
                        grid[x, y].back = back;
                        grid[x, y].icon = elem.Icon;
                        elem.dirty = false;
                    }
                }
            }

            for (int y = 0; y < height; y++)
            {
                Console.SetCursorPosition(left, y + top);
                
                for (int x = 0; x < width; x++)
                {
                    // AHHH, we don't want this check, we want    if(y=height-1 and x = width-1) don't draw..
                    if ((y == height - 1) && (x == width - 1))// can't draw in bottom right corner without triggering a new line :|
                    {
                        Console.MoveBufferArea(x - 1, y, 1, 1, x, y);
                    }
                    else
                    {
                        if (grid[x, y] != null)
                        {
                            Console.SetCursorPosition(x + left, y + top);
                            Console.ForegroundColor = grid[x, y].fore;
                            Console.BackgroundColor = grid[x, y].back;
                            Console.Write(grid[x, y].icon);
                        }
                    }
                }
            }
        }
    }
}
