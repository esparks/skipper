﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    interface AsciiSprite
    {
        void Draw(int x, int y, AsciiViewWindow window);
    }
}
