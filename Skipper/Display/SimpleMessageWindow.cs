﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    class SimpleMessageWindow : AbstractAsciiOptionWindow
    {

        public SimpleMessageWindow(int l, int t, int w, int h, string title, string message, AsciiViewWindow parent, IRenderer render, IKeyInput input)
            : base(l, t, w, h, title, parent, render, input)
        {
            IList<string> tlines = AsciiTools.SplitString(message, w - 1);
            if (tlines.Count >= h)
            {
                h = tlines.Count + 1;
            }

            Resize(l, t, w, h);

            int tx = this.X + 1;
            int ty = this.Y + 1;
            foreach (string s in tlines)
            {
                AsciiTools.Write(tx, ty++, s, ColorPreset.WHITE, ColorPreset.BLACK, parent);
            }
        }
    }
}
