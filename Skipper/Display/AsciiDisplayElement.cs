﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    class AsciiDisplayElement
    {
        private char icon;
        private ColorPreset foreground;
        private ColorPreset background;
        public char Icon { get { return icon; } set { icon = value; dirty = true; } }
        public ColorPreset Foreground { get { return foreground; } set { foreground = value; dirty = true; } }
        public ColorPreset Background { get { return background; } set { background = value; dirty = true; } }
        public bool dirty;
        public AsciiDisplayElement()
        {
            this.dirty = true;
        }
    }
}
