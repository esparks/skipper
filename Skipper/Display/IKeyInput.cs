﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    interface IKeyInput
    {
        IKeyInfo ReadKeyBlocking();
        IKeyInfo ReadCharNoBlock();
        Boolean IsKeyDown(char key);
        Boolean IsKeyDown(SpecialKeys key);
        //string ReadString(); //?// may make this a tool.
    }
}
