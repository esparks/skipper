﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    abstract class AbstractShipSprite : AsciiSprite
    {
        public abstract void Draw(int x, int y, AsciiViewWindow window);
    }
}
