﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    class SimpleShipSprite : AbstractShipSprite
    {
        public enum Orientation
        {
            LEFT,
            RIGHT
        }

        private Orientation orientation;
        private AsciiViewWindow data;

        public SimpleShipSprite(Orientation orientation)
        {
            this.orientation = orientation;
            data = new AsciiViewWindow(10, 6);
            AsciiTools.Fill(' ', 0, 0, data.Width, data.Height, ColorPreset.BLACK, ColorPreset.ALPHA100, data);
            string[] dat = new string[4];
            //dat[0] = " ______  ";
            dat[0] = "/######\\";
            dat[1] = "|########\\";
            dat[2] = "|########/";
            dat[3]= "\\######/";
            int y = 0;
            foreach (string s in dat)
            {
                for (int i = 0; i < s.Length; i++)
                {
                    data.Set(i, y, s.ElementAt(i), ColorPreset.COMBAT_SHIP_FG, ColorPreset.COMBAT_SHIP_BG);
                }
                y++;
            }
        }

        public override void Draw(int x, int y, AsciiViewWindow window)
        {
            switch (orientation)
            {
                case Orientation.RIGHT:
                    for (int sx = 0; sx < data.Width; sx++)
                    {
                        for (int sy = 0; sy < data.Height; sy++)
                        {
                            CopyData(data[sx, sy], window[x + sx, y + sy]);
                        }
                    }
                    break;
                case Orientation.LEFT:
                    for (int sx = 0; sx < data.Width; sx++)
                    {
                        for (int sy = 0; sy < data.Height; sy++)
                        {
                            CopyData(data[sx, sy], window[x + data.Width - sx, y + sy]);
                        }
                    }
                    break;
            }
        }

        private void CopyData(AsciiDisplayElement source, AsciiDisplayElement dest)
        {
            if (source.Icon == ' ')
            {
                return;
            }

            if (source.Background != ColorPreset.ALPHA100)
            {
                dest.Background = source.Background;
            }

            dest.Foreground = source.Foreground;
            dest.Icon = source.Icon;
        }

    }
}
