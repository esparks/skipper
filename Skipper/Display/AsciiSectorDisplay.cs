﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Encounters.Ruins;

namespace Skipper.Display
{

    delegate AsciiDisplayElement DisplayMethod(Object o);


    class AsciiSectorDisplay
    {
        // map sector encounter to an ASCII display method //

        private static IDictionary<Type, DisplayMethod> map;

        public static void Initialise()
        {
            // set up the map here i guess
            // then i get injected into some display instance somewhere..
            map = new Dictionary<Type, DisplayMethod>();
            map.Add(typeof(PlanetEncounter), DisplayPlanet);
            map.Add(typeof(SystemEncounter), DisplayPlanet);
            map.Add(typeof(SpaceStationEncounter), DisplaySpaceStation);
            map.Add(typeof(AsteroidEncounter), DisplayAsteroid);
            map.Add(typeof(RuinsEntryEncounter),DisplayRuins);//testing only, should be on planet etc.
        }

        public static AsciiDisplayElement GetDisplayElement(AbstractEncounter ae)
        {   
            DisplayMethod method = null;
            if (ae != null && map.TryGetValue(ae.GetType(), out method))
            {
                return method(ae);
            }
            // else default
            return DefaultDisplay();
        }

        private static AsciiDisplayElement DefaultDisplay()
        {
            AsciiDisplayElement icon = new AsciiDisplayElement();
            icon.Icon = ' ';
            icon.Background = ColorPreset.BLACK;
            icon.Foreground = ColorPreset.WHITE;
            return icon;
        }

        private static AsciiDisplayElement DisplaySpaceStation(Object o)
        {
            AsciiDisplayElement icon = new AsciiDisplayElement();
            icon.Icon = (char)10;//(char)245;
            icon.Background = ColorPreset.BLACK;
            icon.Foreground = ColorPreset.SPACE_STATION;
            return icon;
        }

        private static AsciiDisplayElement DisplayAsteroid(Object o)
        {
            AsciiDisplayElement icon = new AsciiDisplayElement();
            icon.Icon = (char)7;
            icon.Background = ColorPreset.BLACK;
            icon.Foreground = ColorPreset.ASTEROID;
            return icon;
        }

        private static AsciiDisplayElement DisplayPlanet(Object o)
        {
            AsciiDisplayElement icon = new AsciiDisplayElement();
            icon.Icon = (char)15;
            icon.Background = ColorPreset.BLACK;
            icon.Foreground = ColorPreset.PLANET;
            return icon;
        }

        private static AsciiDisplayElement DisplayRuins(Object o)
        {
            AsciiDisplayElement icon = new AsciiDisplayElement();
            icon.Icon = 'R';
            icon.Background = ColorPreset.BLACK;
            icon.Foreground = ColorPreset.WHITE;
            return icon;
        }
    }
}
