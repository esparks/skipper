﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    class AbstractAsciiOptionWindow
    {
        private AsciiViewWindow buffer;
        private AsciiViewWindow parent;
        private IRenderer render;
        private IKeyInput input;

        protected IKeyInput Input { get { return input; } }
        protected IRenderer Render { get { return render; } }

        private string title;

        private int x;
        private int y;
        private int w;
        private int h;

        protected int X { get { return x; } }
        protected int Y { get { return y; } }
        protected int W { get { return w; } }
        protected int H { get { return h; } }

        protected AsciiViewWindow Window { get { return parent; } }

        public AbstractAsciiOptionWindow(int x, int y, int w, int h, string title, AsciiViewWindow parent, IRenderer render, IKeyInput input)
        {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.parent = parent;
            // copy parent into buffer
            this.title = title;
            this.input = input;
            this.render = render;
            Store();
            RedrawWindow();
        }

        private void Store()
        {
            buffer = new AsciiViewWindow(w, h);
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    buffer[i, j] = new AsciiDisplayElement();
                    buffer[i, j].Icon = parent[i + x, j + y].Icon;
                    buffer[i, j].Foreground = parent[i + x, j + y].Foreground;
                    buffer[i, j].Background = parent[i + x, j + y].Background;
                }
            }
        }

        private void RedrawWindow()
        {
            AsciiTools.DrawDoubleLineBox(x, y, w, h, title, ColorPreset.BLACK, ColorPreset.WHITE, parent);
            AsciiTools.Fill(' ', x + 1, y + 1, w - 2, h - 2, ColorPreset.WHITE, ColorPreset.BLACK, parent);
        }

        protected void Resize(int l, int t, int w, int h)
        {
            Restore();
            this.x = l;
            this.y = t;
            this.w = w;
            this.h = h;
            Store();
            RedrawWindow();
        }

        public void Restore()
        {
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    parent[i + x, j + y].Icon = buffer[i, j].Icon;
                    parent[i + x, j + y].Background = buffer[i, j].Background;
                    parent[i + x, j + y].Foreground = buffer[i, j].Foreground;

                }
            }
        }

        protected void Redraw()
        {
            //re
        }
    }
}
