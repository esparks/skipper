﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using libtcod;

namespace Skipper.Display
{
    class TCODInput : IKeyInput
    {
        private class TCODKeyWrapper : IKeyInfo
        {
            private TCODKey tckey;

            public TCODKeyWrapper(TCODKey key)
            {
                this.tckey = key;
            }

            public char Key()
            {
                return tckey.Character;
            }

            public bool IsSpecial(SpecialKeys skey)
            {
                switch (skey)
                {
                    case SpecialKeys.LEFT:
                        return tckey.KeyCode == TCODKeyCode.Left;
                    case SpecialKeys.RIGHT:
                        return tckey.KeyCode == TCODKeyCode.Right;
                    case SpecialKeys.UP:
                        return tckey.KeyCode == TCODKeyCode.Up;
                    case SpecialKeys.DOWN:
                        return tckey.KeyCode == TCODKeyCode.Down;
                    case SpecialKeys.ENTER:
                        return tckey.KeyCode == TCODKeyCode.Enter;
                    case SpecialKeys.F1:
                        return tckey.KeyCode == TCODKeyCode.F1;
                    case SpecialKeys.F2:
                        return tckey.KeyCode == TCODKeyCode.F2;
                    case SpecialKeys.F3:
                        return tckey.KeyCode == TCODKeyCode.F3;
                    case SpecialKeys.F4:
                        return tckey.KeyCode == TCODKeyCode.F4;
                    case SpecialKeys.F5:
                        return tckey.KeyCode == TCODKeyCode.F5;
                    case SpecialKeys.F6:
                        return tckey.KeyCode == TCODKeyCode.F6;
                    case SpecialKeys.F7:
                        return tckey.KeyCode == TCODKeyCode.F7;
                    case SpecialKeys.F8:
                        return tckey.KeyCode == TCODKeyCode.F8;
                    case SpecialKeys.F9:
                        return tckey.KeyCode == TCODKeyCode.F9;
                    case SpecialKeys.F10:
                        return tckey.KeyCode == TCODKeyCode.F10;
                    case SpecialKeys.F11:
                        return tckey.KeyCode == TCODKeyCode.F11;
                    case SpecialKeys.F12:
                        return tckey.KeyCode == TCODKeyCode.F12;
                    default:
                        return false;
                }
            }

            public SpecialKeys Special()
            {
                throw new NotImplementedException();
            }
        }

        public IKeyInfo ReadKeyBlocking()
        {
            TCODKey tcodkey = TCODConsole.waitForKeypress(true);
            //TCODKey key = TCOD.
            
            return new TCODKeyWrapper(tcodkey);
        }

        public IKeyInfo ReadCharNoBlock()
        {
            throw new NotImplementedException();
        }

        public Boolean IsKeyDown(char key)
        {
            throw new NotImplementedException();
        }

        public Boolean IsKeyPressed(SpecialKeys key)
        {
            throw new NotImplementedException();
        }

        public Boolean IsKeyDown(SpecialKeys key)
        {
            throw new NotImplementedException();
        }
    }
}
