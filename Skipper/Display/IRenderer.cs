﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    interface IRenderer
    {
        void Clear();
        int GetWidth();
        int GetHeight();
        void Render(int x, int y, AsciiViewWindow window);
    }
}
