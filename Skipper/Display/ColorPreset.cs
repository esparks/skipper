﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    public enum ColorPreset
    {
        SELECTED_CARD_FG,
        UNSELECTED_CARD_FG,
        ENCOUNTER_NAME,
        UNSELECTED_OPTION_FG,
        UNSELECTED_OPTION_BG,
        SELECTED_OPTION_FG,
        SELECTED_OPTION_BG,
        BLACK,
        WHITE,
        PLAYER_SHIP,
        PLANET,
        STAR_BG,
        DEFAULT_BG,
        DEFAULT_TEXT_FG,
        WINDOW_BORDER,
        SPACE_STATION,
        ASTEROID,
        STATUS_BAR_FG,
        STATUS_BAR_BG,
        VIEW_OPTIONS_FG,
        VIEW_OPTIONS_BG,
        ANY_KEY_DEFAULT,
        UNAVAILABLE,
        RED,
        CHARACTER_SELECT_ARROW,
        ALPHA100,
        COMBAT_SHIP_FG,
        COMBAT_SHIP_BG,
        MAZE_FLOOR_FG,
        MAZE_FLOOR_BG,
        MAZE_WALL_FG,
        MAZE_WALL_BG
    };
}
