﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Display
{
    class ConsoleKeyWrapper : IKeyInfo
    {
        private ConsoleKeyInfo key;
        public ConsoleKeyWrapper(ConsoleKeyInfo cki)
        {
            key = cki;
        }

        public char Key()
        {
            return key.KeyChar;
        }

        public bool IsSpecial(SpecialKeys skey)
        {
            //throw new NotImplementedException();

            switch (skey)
            {
                case SpecialKeys.LEFT:
                    return key.Key == ConsoleKey.LeftArrow;
                case SpecialKeys.RIGHT:
                    return key.Key == ConsoleKey.RightArrow;
                case SpecialKeys.UP:
                    return key.Key == ConsoleKey.UpArrow;
                case SpecialKeys.DOWN:
                    return key.Key == ConsoleKey.DownArrow;
                case SpecialKeys.ENTER:
                    return key.Key == ConsoleKey.Enter;
                case SpecialKeys.F11:
                    return key.Key == ConsoleKey.F11;
                case SpecialKeys.F12:
                    return key.Key == ConsoleKey.F12;
                default:
                    return false;
            }
            //return false;
        }

        public SpecialKeys Special()
        {
            throw new NotImplementedException();
        }
    }
}
