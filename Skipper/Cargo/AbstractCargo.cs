﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Skipper.Cargo
{
    class AbstractCargo : AbstractCollectible
    {
        public AbstractCargo(XElement node)
            : base(node)
        {

        }

        public AbstractCargo(string name, int amount)
            : base(name, amount)
        { }
    }
}
