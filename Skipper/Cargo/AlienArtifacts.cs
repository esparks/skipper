﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Skipper.Cargo
{
    //TODO: Replace these classes with data definitions
    class AlienArtifacts : AbstractCargo
    {
        public AlienArtifacts(XElement node)
            : base(node)
        {

        }

        public AlienArtifacts(int amount)
            : base("Alien Artifacts", amount)
        {
            this.Value = 1000;
        }
    }
}
