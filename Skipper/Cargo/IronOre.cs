﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Skipper.Cargo
{
    class IronOre : AbstractCargo
    {
        public IronOre(XElement node)
            : base(node)
        {

        }

        public IronOre(int amount)
            : base("Iron Ore", amount)
        {
            this.Value = 100;
        }
    }
}
