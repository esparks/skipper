﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Skipper.Cargo
{
    class GoldOre : AbstractCargo
    {
        public GoldOre(XElement node)
            : base(node)
        {

        }


        public GoldOre(int amount)
            : base("Gold", amount)
        {
            this.Value = 500;
        }
    }
}
