﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Data
{
    class TurnMessageEvent : TurnEvent
    {
        private string message;
        public TurnMessageEvent(string title, string message)
        {
            this.SetDescription(() => { return message; });
        }

        public override void ApplyAnyEffects()
        {
            // basic message no effects //
        }
    }
}
