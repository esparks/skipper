﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Data
{
    public enum DamageTypes
    {
        BLUNT,
        BLADE,
        PIERCE,
        HEAT,
        RADIATION,
        COLD
    }

    class Damage
    {
        private int amount;
        private DamageTypes type;
        
        public int Amount { get { return amount; } }
        public DamageTypes Type { get { return type; } }

        public Damage(int amount, DamageTypes type)
        {
            this.amount = amount;
            this.type = type;
        }
    }
}
