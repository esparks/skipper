﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Data
{
    public enum Directions
    {
        NORTH = 0, 
        EAST = 1, 
        SOUTH = 2, 
        WEST = 3, 
        UP = 4, 
        DOWN = 5, 
        NORTHEAST = 6, 
        SOUTHEAST = 7, 
        NORTHWEST = 8, 
        SOUTHWEST = 9
    }

    
    public class DirectionTools
    {
        
        private static IDictionary<Directions, string> names = new Dictionary<Directions, string> {
            {Directions.NORTH, "north"},
            {Directions.SOUTH, "south"},
            {Directions.EAST, "east"},
            {Directions.WEST, "west"},
            {Directions.UP, "up"},
            {Directions.DOWN, "down"},
            {Directions.SOUTHEAST, "south-east"},
            {Directions.SOUTHWEST, "south-west"},
            {Directions.NORTHEAST, "north-east"},
            {Directions.NORTHWEST, "north-west"},            
        };

        public static Directions GetRandomDirection()
        {
            int dlen = Enum.GetNames(typeof(Directions)).Count();
            return (Directions)Rand.Instance.NextInt(dlen);
        }

        public static Directions GetOppositeDirection(Directions d)
        {
            switch (d)
            {
                case Directions.NORTH: return Directions.SOUTH; 
                case Directions.EAST: return Directions.WEST;
                case Directions.WEST: return Directions.EAST;
                case Directions.SOUTH: return Directions.NORTH;

                case Directions.UP: return Directions.DOWN; 
                case Directions.DOWN: return Directions.UP; 
                case Directions.NORTHEAST: return Directions.SOUTHWEST; 
                case Directions.NORTHWEST: return Directions.SOUTHEAST; 

                case Directions.SOUTHEAST: return Directions.NORTHWEST; 
                case Directions.SOUTHWEST: return Directions.NORTHEAST; 
                default:
                    throw new Exception("Direction not paired with opposite");

            }
        }

        public static string GetDirectionString(Directions dir)
        {

            if (names.ContainsKey(dir))
            {
                return names[dir];
            }
            else
            {
                return "Direction Name Not Defined";
            }

        }
    

    }
}
