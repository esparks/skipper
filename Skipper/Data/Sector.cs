﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using System.Xml.Linq;

namespace Skipper.Data
{
    class Sector : IPersistant
    {
        private AbstractEncounter baseEncounter;
        public AbstractEncounter BaseEncounter { get { return baseEncounter; } }

        public Sector(AbstractEncounter baseEncounter)
        {
            this.baseEncounter = baseEncounter;
        }

        public void Serialize(ref System.Xml.Linq.XElement node)
        {
            //throw new NotImplementedException();
            if (baseEncounter != null)
            {
                //node.Add(new XAttribute("type", baseEncounter.GetType().Namespace + "." + baseEncounter.GetType().Name));
                baseEncounter.Serialize(ref node);
            }
            else
            {
                node.Add(new XAttribute("type", "none"));
            }
                
        }
    }
}
