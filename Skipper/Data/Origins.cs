﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace Skipper.Data
{
    class Origins
    {
        private const string FOLDER_PATH = @"Data\content\origins\";
        public const string ELEMENT_ORIGIN = "origin";
        public const string ELEMENT_TYPE = "type";
        public const string ELEMENT_DESCRIPTION = "description";
        public const string ELEMENT_BOOSTS = "boosts";
        public const string ELEMENT_SKILLS = "skills";
        public const string ELEMENT_SPECIALITIES = "specialities";
        public const string ELEMENT_SKILL = "skill";

        private IDictionary<string, XElement> origins;

        public Origins()
        {
            origins = new Dictionary<string, XElement>();

            foreach (string file in Directory.EnumerateFiles(FOLDER_PATH, "*.xml"))
            {
                XDocument doc = XDocument.Load(file);
                XElement root = doc.Element(ELEMENT_ORIGIN);
                string type = root.Element(ELEMENT_TYPE).Value;
                origins.Add(type, root);
            }
        }

        public string GetRandomOrigin()
        {
            string[] list = origins.Keys.ToArray();
            int r = Rand.Instance.NextInt(list.Count());
            return list[r];
        }

        public IDictionary<string, int> GetStatBoosts(string origin)
        {
            IDictionary<string, int> boosts = new Dictionary<string, int>();
            XElement originElement;
            if (origins.TryGetValue(origin, out originElement))
            {
                XElement boostsElement = originElement.Element(ELEMENT_BOOSTS);
                foreach (XElement boost in boostsElement.Descendants())
                {
                    boosts.Add(boost.Name.LocalName, int.Parse(boost.Value));
                }
                return boosts;
            }
            throw new Exception("Origin " + origin + " does not exist");
        }
    }
}
