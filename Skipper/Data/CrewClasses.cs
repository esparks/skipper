﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Data
{
    class CrewClasses
    {

        public const string CLASS_ENGINEER = "Engineer";
        public const string CLASS_SIGINT = "SigInt";
        public const string CLASS_SECURITY = "Security";
        public const string CLASS_NAVIGATOR = "Navigator";
        public const string CLASS_SCIENTIST = "Scientist";

    }
}
