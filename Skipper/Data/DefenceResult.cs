﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;

namespace Skipper.Data
{
    class DefenceResult : CombatResult
    {
        public DefenceResult(AbstractCombatEntity source, int damageReduction)
            : base(source, null, 0)
        {

        }

        protected override string TextDescription()
        {
            return Attacker.Name + " is defending.";
        }
    }
}
