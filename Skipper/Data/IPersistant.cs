﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Skipper.Data
{
    interface IPersistant
    {
        void Serialize(ref XElement node);
        //DeSerialize(...) // can't enfore a constructor this(XmlElement config) but that would be nice...
    }
}
