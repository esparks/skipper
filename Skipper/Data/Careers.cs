﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using Skipper.Creatures;
using Skipper.Creatures.Crew;

namespace Skipper.Data
{
    class Careers
    {
        public const string ELEMENT_CAREER = "career";
        public const string ELEMENT_TITLE = "title";
        public const string ELEMENT_DESCRIPTION = "description";
        public const string ELEMENT_PREREQUISITES = "prereqs";
        public const string ELEMENT_SKILLS = "skills";
        public const string ELEMENT_TACTICS = "tactics";
        public const string ELEMENT_TACTIC = "tactic";
        public const string ELEMENT_NAME = "name";
        public const string ELEMENT_TYPE = "type";

        public const string CAREER_NO_PREREQS = "Traveller";

        private const string FILEPATH = @"Data\content\careers\";
        private IDictionary<string, XElement> careers;

        private IDictionary<string, XElement> gatedCareers;
        private XElement defaultCareer;

        public Careers()
        {
            // store and provide access to the career elements
            careers = new Dictionary<string, XElement>();
            gatedCareers = new Dictionary<string, XElement>();
            // scan all files in careers folder.
            foreach(string file in Directory.EnumerateFiles(FILEPATH, "*.xml"))
            {
                XDocument doc = XDocument.Load(file);
                XElement root = doc.Element(ELEMENT_CAREER);
                string title = root.Element(ELEMENT_TITLE).Value;
                if (title == CAREER_NO_PREREQS)
                    defaultCareer = root;
                else gatedCareers.Add(title, root);

                careers.Add(title, root);
            }

            foreach (XElement career in careers.Values)
            {
                Console.WriteLine("{0} : {1}", career.Element(ELEMENT_TITLE).Value, career.Element(ELEMENT_DESCRIPTION).Value);
            }
            //Console.ReadKey();
        }

        //TODO: abstract stats blocks out?
        public string FindCareerByPrerequisites(CrewConfigElement crew)
        {
            IList<string> list = gatedCareers.Keys.ToArray().ToList();
            // randomize list..
            IList<string> randomized = new List<string>();
            while (list.Count > 0)
            {
                int p = Rand.Instance.NextInt(list.Count - 1);
                randomized.Add(list.ElementAt(p));
                list.RemoveAt(p);
            }
            foreach (string s in randomized)
            {
                bool pass = true;
                //Console.WriteLine("checking career " + s);
                XElement prereqs = careers[s].Element(ELEMENT_PREREQUISITES);
                foreach (XElement pr in prereqs.Descendants())
                {
                    string stat = pr.Name.LocalName;
                    int min = int.Parse(pr.Value);
                    //Console.WriteLine("checking stat " + stat + " : " + crew.stats[stat] + " > " + min + " ?");
                    if (crew.stats[stat] < min)
                        pass = false;
                }
                //Console.ReadKey();
                if (pass)
                {
                    //Console.WriteLine("returning career " + s);
                    return s;
                }
            }
            return defaultCareer.Element(ELEMENT_TITLE).Value;
        }

        public string[] GetCareerAttackNames(string career)
        {
            IList<string> names = new List<string>();
            foreach(XElement tactic in careers[career].Element(ELEMENT_TACTICS).Elements(ELEMENT_TACTIC))
            {
                names.Add(tactic.Attribute(ELEMENT_NAME).Value);
            }
            return names.ToArray();
        }

        public string GetCareerAttackReference(string career, string attack)
        {
            foreach(XElement tactic in careers[career].Element(ELEMENT_TACTICS).Elements(ELEMENT_TACTIC))
            {
                if (tactic.Attribute(ELEMENT_NAME).Value == attack)
                {
                    return tactic.Attribute(ELEMENT_TYPE).Value;
                }
            }
            throw new Exception("Tactic " + attack + " does not exist");
        }
    }
}
