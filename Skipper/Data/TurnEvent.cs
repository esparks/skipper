﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Data
{
    delegate string TurnEventDescription();

    abstract class TurnEvent
    {
        private TurnEventDescription description;
        public string ResultText { get { return description(); } }
        abstract public void ApplyAnyEffects();

        public void SetDescription(TurnEventDescription desc)
        {
            this.description = desc;
        }
    }
}
