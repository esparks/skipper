﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;

namespace Skipper.Data
{
    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class TacticsMethod : System.Attribute
    {
        private string name;
        public string Name { get { return name; } }
        public TacticsMethod(string name)
        {
            this.name = name;
        }
    }

    class MasterTactics
    {
        private IDictionary<string, Tactic> tactics;

        public MasterTactics()
        {
            Initialise();
        }

        private void Initialise()
        {
            //load all into a dictionary
            tactics = new Dictionary<string, Tactic>();
            // use metadata...
            var methods = this.GetType().Assembly.GetTypes()
                      .SelectMany(t => t.GetMethods())
                      .Where(m => m.GetCustomAttributes(typeof(TacticsMethod), false).Length > 0)
                      .ToArray();

            // list all!
            foreach (var m in methods)
            {
                object attr = m.GetCustomAttributes(typeof(TacticsMethod), false).First();
                TacticsMethod tm = (TacticsMethod)attr;
                tactics.Add(tm.Name, (Tactic)Delegate.CreateDelegate(typeof(Tactic), m));
                Console.WriteLine("Read method: " + tm.Name);
            }
        }
        public Tactic GetTactic(string name)
        {
            Tactic t;
            if (tactics.TryGetValue(name, out t))
            {
                return t;
            }
            return null;
        }
    }
}
