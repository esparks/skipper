﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Skipper.Data
{
    delegate string NameComponentGenerator();

    class PlanetData : IPersistant
    {
        private static string[] types = new string[] { "rock", "red", "gas", "earth-like", "acid", "metal", "forest", "lava", "ice" };
        private static string[] adjectives = new string[] { "large", "small", "giant", "shattered" };
        private static string[] colors = new string[] { "blue", "red", "green", "mottled", "brown", "silver", "golden", "white", "dark" };

        private string type;
        private string adjective;
        private string color;
        private string name;

        private bool useType;
        private bool useAdjective;
        private bool useColor;

        private static NameComponentGenerator[] nameGenerators = new NameComponentGenerator[] {
            NameLettersGenerator,
            NameNumbersGenerator
        };

#region Generators

        private static string NameLettersGenerator()
        {
            int min = 2;
            int max = 3;

            string component = "";
            int[] casemins = new int[] {65, 97};
            int[] casemaxs = new int[] {90, 122};
            for (int i = min; i < max; i++)
            {
                int aCase = Rand.Instance.NextInt(2);
                int letter = Rand.Instance.NextInt(casemins[aCase], casemaxs[aCase]);
                component += (char)letter;
            }
            return component;
        }

        private static string NameNumbersGenerator()
        {
            int min = 1;
            int max = 4;
            string component = "";
            for (int i = min; i < max; i++)
            {
                int num = Rand.Instance.NextInt(0, 9);
                string letter = "" + num;
                component += letter;
            }
            return component;
        }

        private static string NewNameConnective()
        {
            string contypes = " ../\\---";
            int idx = Rand.Instance.NextInt(contypes.Length);
            return "" + contypes[idx];
        }

#endregion

        public PlanetData(XElement node)
        {

            type = node.Attribute("type").Value;
            if (node.Attribute("adjective") != null)
            {
                this.useAdjective = true;
                adjective = node.Attribute("adjective").Value;
            }

            if (node.Attribute("color") != null)
            {
                this.useColor = true;
                color = node.Attribute("color").Value;
            }
            name = node.Attribute("name").Value;
        }

        private PlanetData()
        {
            //generate out a random name;
            int maxComponents = 4;
            int minComponents = 2;
            int numTypes = 3;
            //types= LETTERS, NUMBERS,
            int numComponents = Rand.Instance.NextInt(minComponents, maxComponents);
            string[] components = new string[numComponents];
            name = "";
            for (int i = 0; i < numComponents; i++)
            {
                components[i] = nameGenerators[Rand.Instance.NextInt(nameGenerators.Count())]();
                name += components[i];
                if(i < numComponents - 1)
                    name += NewNameConnective();
            }
        }

        public static PlanetData NewPlanet()
        {
            PlanetData pd = new PlanetData();
            double adjectiveChance = 25.0;
            double colorChance = 50.0;

            pd.type = types[Rand.Instance.NextInt(types.Count())];

            pd.useAdjective = Rand.Instance.NextBool(adjectiveChance);
            pd.useColor = Rand.Instance.NextBool(colorChance);
            if (pd.useAdjective)
            {
                pd.adjective = adjectives[Rand.Instance.NextInt(adjectives.Count())];
            }

            if (pd.useColor)
            {
                pd.color = colors[Rand.Instance.NextInt(colors.Count())];
            }
            return pd;
        }

        public string GetDescription()
        {
            string d = "";
            if (useAdjective) d += this.adjective + " ";
            if (useColor) d += this.color + " ";
            d += this.type;
            return d;
        }

        public string GetName()
        {
            return this.name;
        }

        public void Serialize(ref System.Xml.Linq.XElement node)
        {
            node.SetAttributeValue("type", type);
            node.SetAttributeValue("adjective", adjective);
            node.SetAttributeValue("color", color);
            node.SetAttributeValue("name", name);
        }
    }
}
