﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;

namespace Skipper.Data
{
    
    class CombatResult : TurnEvent
    {
        private string result;
        
        private AbstractCombatEntity attacker;
        private AbstractCombatEntity defender;
        private int damage;
        private string verb;
        private Weapon weapon;
        private bool killed;

        public AbstractCreature Attacker { get { return (AbstractCreature)attacker; } } //break this and fix it to work with ships too~!
        public AbstractCreature Defender { get { return (AbstractCreature)defender; } }
        public int Damage { get { return damage; } }

        public bool Killed { get { return killed; } set { killed = value; } }

        public CombatResult(AbstractCombatEntity attacker, AbstractCombatEntity defender, int damage)
        {
            this.damage = damage;
            //this.weapon = attacker.Weapon;
            this.killed = false;
            
            this.attacker = attacker;
            this.defender = defender;
            this.SetDescription(TextDescription);
        }

        public void SetWeapon(Weapon w)
        {
            this.weapon = w;
        }

        public override void ApplyAnyEffects()
        {
            ApplyCombatResult();
        }

        private void ApplyCombatResult()
        {
            if (defender != null)
            {
                Defender.ApplyCombatResult(this);
            }

        }

        virtual protected string TextDescription()
        {
            // hack until you break it then implement names for ACE's
            AbstractCreature atk = (AbstractCreature)attacker;
            AbstractCreature def = (AbstractCreature)defender;
            // verb
            if (weapon == null && atk != null)
                weapon = atk.Weapon;

            if (weapon != null)
            {
                this.verb = weapon.GetVerb(this.damage);
            }
            else
            {
                this.verb = "strikes";//fist should be a weapon too
            }

            if (defender == null)
            {
                return atk.Name + " has no target.";
            }

            string result = "";
            result += atk.Name + " " + verb + " " + def.Name + " for " + damage + " damage.";
            return result;
        }
    }
}
