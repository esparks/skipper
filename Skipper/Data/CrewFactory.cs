﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures.Crew;
using Skipper.Creatures;

namespace Skipper.Data
{
    class CrewFactory
    {
        // TODO::
        // would be cool to roll random stats then pick a career that is suitable
        // randomize list of careers and then  pick first one stats are eligable for
        // exception, have a fallback "drifter/vagrant/etc." if none exist

        // nothing to stop weird hybrid generations also
        public int STARTING_AGE = 20;

        public CrewMember GenerateCrewMember(int level)
        {
            //CrewConfigElement config = new CrewConfigElement();
            //config.Level = level; // deprecate, instead base on age/career etc..
            // generate some basic attributes

            // skills: focused vs balanced

            int age = Rand.Instance.NextInt(30);
            age = age + STARTING_AGE;
            
            // draw one basic psychology..

            // for 1nce at 20 and each five years afterwards determine some stuff...

            CrewConfigElement cce;
            cce = NewCrewElement(1);
            cce.Name = WorldHub.Instance.NameGenerator.GetRandomName();
            cce.Origin = WorldHub.Instance.Origins.GetRandomOrigin();
            cce.History = "Please create histories";
            var statBoosts = WorldHub.Instance.Origins.GetStatBoosts(cce.Origin);
            
            foreach (string stat in statBoosts.Keys)
            {
                cce.stats[stat] += statBoosts[stat];
            }

            cce.Class = WorldHub.Instance.Careers.FindCareerByPrerequisites(cce);

            // add this stuff to career xml
            cce.weapon = WorldHub.Instance.MasterWeapons.GetWeapon("Hunting Rifle");
            /* tactics */
            foreach (string attackName in WorldHub.Instance.Careers.GetCareerAttackNames(cce.Class))
            {
                cce.tactics.Add(attackName, WorldHub.Instance.Careers.GetCareerAttackReference(cce.Class, attackName));
                cce.tactics.Add("Defend", GenericTactics.GENERIC_DEFEND);
                cce.defaultTactic = attackName;
            }
            
            return new CrewMember(cce);

          /*  switch(Rand.Instance.NextInt(5))
            {
                case 0:
                    cce = GenerateScientist(age, level);
                    return new CrewMember(cce);
                case 1:
                    cce = GenerateEngineer(age, level);
                    return new CrewMember(cce);
                case 2:
                    cce = GenerateSecurity(age, level);
                    return new CrewMember(cce);
                case 3:
                    cce = GenerateNavigator(age, level);
                    return new CrewMember(cce);
                case 4:
                    cce = GenerateSigInt(age, level);
                    return new CrewMember(cce);
                default:
                    CrewConfigElement ccd = GenerateEngineer(age, level);
                    return new CrewMember(ccd);
                    throw new Exception("bad crewe gen");
            }*/
        }

        private CrewConfigElement NewCrewElement(int level)
        {
            CrewConfigElement cce = new CrewConfigElement();
            cce.Age = STARTING_AGE;
            cce.level = level;
            cce.stats[Stats.HP] = 40 + level * 10;
            cce.stats[Stats.MAXHP] = cce.stats[Stats.HP];
            cce.stats[Stats.INTELLIGENCE] = 6 + Rand.Instance.Roll(2, 6);
            cce.stats[Stats.AIM] = 6 + Rand.Instance.Roll(2, 6);
            cce.stats[Stats.STRENGTH] = 6 + Rand.Instance.Roll(2, 6);
            cce.stats[Stats.CHARISMA] = 6 + Rand.Instance.Roll(2, 6);
            cce.stats[Stats.DEXTERITY] = 6 + Rand.Instance.Roll(2, 6);
            cce.stats[Stats.WILLPOWER] = 6 + Rand.Instance.Roll(2, 6);
            return cce;
        }

        private static CrewConfigElement GenerateEngineer(int age, int level)
        {
            CrewConfigElement cce = new CrewConfigElement();
            cce.Class = CrewClasses.CLASS_ENGINEER;
            cce.Age = age;
            cce.Homeworld = "Asteroid Colony";
            cce.Description = "A regular looking engineer type person.";
            cce.Origin = "Origin?";
            cce.History = "dunno.. something";
            cce.level = level;
            cce.stats[Stats.HP] = 40 + level * 10;
            cce.stats[Stats.MAXHP] = cce.stats[Stats.HP];
            cce.stats[Stats.INTELLIGENCE] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.AIM] = 10 + Rand.Instance.NextInt(1, level * 4);
            cce.stats[Stats.STRENGTH] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.CHARISMA] = 10 + Rand.Instance.NextInt(1, level * 3);
            cce.stats[Stats.DEXTERITY] = 10 + Rand.Instance.NextInt(1, level * 3);
            cce.stats[Stats.WILLPOWER] = 10 + Rand.Instance.NextInt(1, level * 5);

            cce.weapon = WorldHub.Instance.MasterWeapons.GetWeapon("Pistol");
            /* tactics */
            cce.tactics.Add("Attack", GenericTactics.GENERIC_ATTACK); 
            cce.defaultTactic = "Attack";
            cce.Name = WorldHub.Instance.NameGenerator.GetRandomName();
            return cce;
        }

        private static CrewConfigElement GenerateScientist(int age, int level)
        {
            CrewConfigElement cce = new CrewConfigElement();
            cce.Class = CrewClasses.CLASS_SCIENTIST;
            cce.Age = age;
            cce.Homeworld = "Space Station";
            cce.Description = "A regular looking scientist type person.";
            cce.Origin = "Origin?";
            cce.History = "dunno.. something";
            cce.level = level;
            cce.stats[Stats.HP] = 40 + level * 10;
            cce.stats[Stats.MAXHP] = cce.stats[Stats.HP];
            cce.stats[Stats.INTELLIGENCE] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.AIM] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.STRENGTH] = 10 + Rand.Instance.NextInt(1, level * 3);
            cce.stats[Stats.CHARISMA] = 10 + Rand.Instance.NextInt(1, level * 3);
            cce.stats[Stats.DEXTERITY] = 10 + Rand.Instance.NextInt(1, level * 5);
            cce.stats[Stats.WILLPOWER] = 10 + Rand.Instance.NextInt(1, level * 4);

            cce.weapon = WorldHub.Instance.MasterWeapons.GetWeapon("Pistol");
            /* tactics */
            cce.tactics.Add("Attack", GenericTactics.GENERIC_ATTACK);
            cce.defaultTactic = "Attack";
            cce.Name = WorldHub.Instance.NameGenerator.GetRandomName();
            return cce;        
        }

        private static CrewConfigElement GenerateSecurity(int age, int level)
        {
            CrewConfigElement cce = new CrewConfigElement();
            cce.Class = CrewClasses.CLASS_SECURITY;
            cce.Age = age;
            cce.Homeworld = "High-G Planet";
            cce.Description = "A solid wall of protection.";
            cce.Origin = "Origin?";
            cce.History = "dunno.. something";
            cce.level = level;
            cce.stats[Stats.HP] = 50 + level * 10;
            cce.stats[Stats.MAXHP] = cce.stats[Stats.HP];
            cce.stats[Stats.INTELLIGENCE] = 10 + Rand.Instance.NextInt(1, level * 3);
            cce.stats[Stats.AIM] = 10 + Rand.Instance.NextInt(1, level * 5);
            cce.stats[Stats.STRENGTH] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.CHARISMA] = 10 + Rand.Instance.NextInt(1, level * 3);
            cce.stats[Stats.DEXTERITY] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.WILLPOWER] = 10 + Rand.Instance.NextInt(1, level * 4);

            cce.weapon = WorldHub.Instance.MasterWeapons.GetWeapon("Pistol");
            /* tactics */
            cce.tactics.Add("Attack", GenericTactics.GENERIC_ATTACK);
            cce.defaultTactic = "Attack";
            cce.Name = WorldHub.Instance.NameGenerator.GetRandomName();
            return cce;
        }

        private static CrewConfigElement GenerateNavigator(int age, int level)
        {
            CrewConfigElement cce = new CrewConfigElement();
            cce.Class = CrewClasses.CLASS_NAVIGATOR;
            cce.Age = age;
            cce.Homeworld = "Deep Space";
            cce.Description = "A thin, nimble chap. Fingers like knives and somewhat unstable.";
            cce.Origin = "Origin?";
            cce.History = "dunno.. something";
            cce.level = level;
            cce.stats[Stats.HP] = 40 + level * 10;
            cce.stats[Stats.MAXHP] = cce.stats[Stats.HP];
            cce.stats[Stats.INTELLIGENCE] = 10 + Rand.Instance.NextInt(1, level * 4);
            cce.stats[Stats.AIM] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.STRENGTH] = 10 + Rand.Instance.NextInt(1, level * 3);
            cce.stats[Stats.CHARISMA] = 10 + Rand.Instance.NextInt(1, level * 5);
            cce.stats[Stats.DEXTERITY] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.WILLPOWER] = 10 + Rand.Instance.NextInt(1, level * 3);

            cce.weapon = WorldHub.Instance.MasterWeapons.GetWeapon("Dirk");
            /* tactics */
            cce.tactics.Add("Attack", GenericTactics.GENERIC_ATTACK);
            cce.defaultTactic = "Attack";
            cce.Name = WorldHub.Instance.NameGenerator.GetRandomName();
            return cce;
        }

        private static CrewConfigElement GenerateSigInt(int age, int level)
        {
            CrewConfigElement cce = new CrewConfigElement();
            cce.Class = CrewClasses.CLASS_SIGINT;
            cce.Age = age;
            cce.Homeworld = "Blue Plant";
            cce.Description = "An excitable young person in a hat.";
            cce.Origin = "Origin?";
            cce.History = "dunno.. something";
            cce.level = level;
            cce.stats[Stats.HP] = 40 + level * 10;
            cce.stats[Stats.MAXHP] = cce.stats[Stats.HP];
            cce.stats[Stats.INTELLIGENCE] = 10 + Rand.Instance.NextInt(1, level * 5);
            cce.stats[Stats.AIM] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.STRENGTH] = 10 + Rand.Instance.NextInt(1, level * 3);
            cce.stats[Stats.CHARISMA] = 10 + Rand.Instance.NextInt(1, level * 6);
            cce.stats[Stats.DEXTERITY] = 10 + Rand.Instance.NextInt(1, level * 4);
            cce.stats[Stats.WILLPOWER] = 10 + Rand.Instance.NextInt(1, level * 3);

            cce.weapon = WorldHub.Instance.MasterWeapons.GetWeapon("Pistol");
            /* tactics */
            cce.tactics.Add("Attack", GenericTactics.GENERIC_ATTACK);
            cce.defaultTactic = "Attack";
            cce.Name = WorldHub.Instance.NameGenerator.GetRandomName();
            return cce;
        }
    }
}
