﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Creatures;

namespace Skipper.Data
{
    class CreatureFactory
    {
        public CreatureFactory(string datafile)
        {
            // load an xml data file and use it to create creatures.
            // might help to mimic the serialization output and then we can
            // use the existing XML constructor to deserialize
        }

        public AbstractCreature Spawn(string name)
        {
            return null;
        }

        public static AbstractCreature SpawnPolyp(int level)
        {
            CreatureConfiguration cc = new CreatureConfiguration();
            cc.stats[Stats.HP] = Rand.Instance.NextInt(20, 30) + (level * 5);
            cc.stats[Stats.MAXHP] = cc.stats[Stats.HP];
            cc.stats[Stats.STRENGTH] = 20;
            cc.stats[Stats.DEXTERITY] = 20;
            cc.stats[Stats.AIM] = 12;
            cc.stats[Stats.WILLPOWER] = 20;
            cc.stats[Stats.INTELLIGENCE] = 6;
            cc.stats[Stats.CHARISMA] = 1;
            cc.money = new Items.Money(0);
            cc.Name = "Flying Polyp";
            cc.tactics.Add("default", GenericTactics.RANDOM_ATTACK);
            cc.weapon = new Weapon("lightning ray", 10, 5, 5, 1.75f, Stats.DEXTERITY, new List<string> {"shoots", "blasts", "fries"});
            return new Creature(cc);
        }

        public static AbstractCreature SpawnInsectoid(int level)
        {
            CreatureConfiguration cc = new CreatureConfiguration();
            cc.stats[Stats.HP] = Rand.Instance.NextInt(15, 20) + (level * 5);
            cc.stats[Stats.MAXHP] = cc.stats[Stats.HP];
            cc.stats[Stats.STRENGTH] = 15;
            cc.stats[Stats.DEXTERITY] = 20;
            cc.stats[Stats.AIM] = 15;
            cc.stats[Stats.WILLPOWER] = 8;
            cc.stats[Stats.INTELLIGENCE] = 8;
            cc.stats[Stats.CHARISMA] = 2;
            cc.money = new Items.Money(0);
            cc.Name = "Insectoid";
            cc.tactics.Add("default", GenericTactics.RANDOM_ATTACK);
            IList<Weapon> insectList = WorldHub.Instance.CreatureWeapons.InsectWeapons;
            cc.weapon = insectList.ElementAt(Rand.Instance.NextInt(insectList.Count()));
            return new Creature(cc);
        }

        public static AbstractCreature SpawnWildebeeste(int level)
        {
            CreatureConfiguration cc = new CreatureConfiguration();
            cc.stats[Stats.HP] = Rand.Instance.NextInt(20, 30) + (level * 5);
            cc.stats[Stats.MAXHP] = cc.stats[Stats.HP];
            cc.stats[Stats.STRENGTH] = 25;
            cc.stats[Stats.DEXTERITY] = 8;
            cc.stats[Stats.AIM] = 8;
            cc.stats[Stats.WILLPOWER] = 15;
            cc.stats[Stats.INTELLIGENCE] = 4;
            cc.stats[Stats.CHARISMA] = 2;
            cc.money = new Items.Money(0);
            cc.Name = "Wildebeeste";
            cc.tactics.Add("default", GenericTactics.RANDOM_ATTACK);
            IList<string> verbs = new List<string> { "tear", "rip", "bite", "chomp" };
            Weapon starter = new Weapon("Fangs", 7, 3, 1, 1.5f, Stats.STRENGTH, verbs); // these should be in the master list! ... ahh just make weapon serializable would be better.
            cc.weapon = starter;
            return new Creature(cc);
        }
    }
}
