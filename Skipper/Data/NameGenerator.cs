﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Skipper.Data
{
    class NameGenerator
    {
       // private static NameGenerator instance = new NameGenerator();
        //public static NameGenerator Instance { get { return instance; } }

        /// <summary>
        /// name generator...
        /// </summary>

        // no need to keep in ram. seems silly to do so.
        private static string[] firstnamefiles = new string[] {"firstnames.txt"};
        private static string[] lastnamefiles = new string[] { "lastnames.txt" };

        private StreamReader firstNames;
        private StreamReader lastNames;


        public NameGenerator()
        {

        }

        public string GetRandomName()
        {
            int nameLength = 2;
            string name = "";
            for (int i = 0; i < nameLength; i++)
            {
                if (i == nameLength - 1)
                {
                    Console.Write("getting a last name...");
                    name += " " + GetLastName();
                }
                else
                {
                    name += " " + GetFirstName();
                }
            }
            return name.Trim();
        }

        private string GetLastName()
        {
            string line = GetRandomLine(lastnamefiles[Rand.Instance.NextInt(lastnamefiles.Count())]);
            line = ReCase(line);
            return line.Split(' ').ElementAt(0).Trim();
        }

        private string GetFirstName()
        {
            string line = GetRandomLine(firstnamefiles[Rand.Instance.NextInt(firstnamefiles.Count())]);
            line = ReCase(line);
            return line.Split(' ').ElementAt(0).Trim();
        }

        private string ReCase(string input)
        {
            input = input.ToLower();
            string firstLetter = input[0].ToString().ToUpper();
            input = firstLetter + input.Substring(1);
            return input;
        }

        private string GetRandomLine(string file)
        {
            StreamReader reader = new StreamReader(@"Data\\" + file);
            string chosen = null;
            int numberSeen = 0;
            var rng = new Random();
            string line = null;
            do
            {
                line = reader.ReadLine();
                if (line != null)
                {
                    if (Rand.Instance.NextInt(++numberSeen) == 0)
                    {
                        chosen = line;
                    }
                }
            } while (line != null);
            reader.Close();
            return chosen;
        }
    }
}
