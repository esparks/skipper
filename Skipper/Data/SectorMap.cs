﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using System.Xml.Linq;

namespace Skipper.Data
{
    class SectorMap : IPersistant
    {
        public int SizeX;// = 30;
        public int SizeY;// = 30;

        private Sector[,] grid;

        public SectorMap(XElement root)
        {
            Console.WriteLine("Loading Sectors...");
            SizeX = int.Parse(root.Attribute("SizeX").Value);
            SizeY = int.Parse(root.Attribute("SizeY").Value);
            grid = new Sector[SizeX, SizeY];
            //
            foreach (XElement sector in root.Elements("sector"))
            {
                XElement coords = sector.Element("coords");
                int x = int.Parse(coords.Attribute("x").Value);
                int y = int.Parse(coords.Attribute("y").Value);
                Console.WriteLine("Loading sector " + x + " : " + y);
                XElement encounter = sector.Element("encounter");
                grid[x, y] = new Sector(EncounterFactory.InstantiateEncounter(encounter, null));
            }
        }

        public SectorMap(int SizeX, int SizeY, Sector[,] grid)
        {
            this.SizeX = SizeX;
            this.SizeY = SizeY;
            this.grid = grid;
        }

        public Sector this[int x, int y]
        {
            get
            {
                return grid[x, y];
            }

            set
            {
                // don't allow.
            }
        }

        public void Serialize(ref XElement node)
        {
            node.SetAttributeValue("SizeX", SizeX);
            node.SetAttributeValue("SizeY", SizeY);
            for(int x = 0; x < SizeX; x++)
            {
                for (int y = 0; y < SizeY; y++)
                {
                    Sector s = grid[x, y];
                    XElement gridRef = new XElement("sector");
                    XElement coords = new XElement("coords");
                    coords.SetAttributeValue("x", x);
                    coords.SetAttributeValue("y", y);
                    gridRef.Add(coords);
                    XElement encounter = new XElement("encounter");
                    s.Serialize(ref encounter);
                    gridRef.Add(encounter);
                    node.Add(gridRef);
                }
            }
        }

    }
}
