﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skipper.Data
{
    class MasterWeaponsList
    {
        IList<string> smallLaserSidearmVerbs = new List<string>() { "zaps", "blasts", "shoots", "burns" };
        IList<string> mediumLaserSidearmVerbs = new List<string>() { "zaps", "blasts", "shoots", "burns" };
        IList<string> largeLaserSidearmVerbs = new List<string>() { "zaps", "blasts", "shoots", "burns" };

        IList<string> weakProjectileSidearmVerbs = new List<string>() { "shoots", "peppers", "attacks", "blasts" };
        //IList<string> mediumLaserSidearmVerbs = new List<string>() { "zaps", "blasts", "shoots", "burns" };
        //IList<string> largeLaserSidearmVerbs = new List<string>() { "zaps", "blasts", "shoots", "burns" };

        IList<string> weakProjectileRifleVerbs = new List<string>() { "fires at", "snipes", "shoots", "targets" };

        IList<string> smallBluntVerbs = new List<string>() { "whacks", "clubs", "beats", "thumps", "strikes" };
        IList<string> largeBluntVerbs = new List<string>() { "crushes", "belts", "strikes", "pummels", "whacks" };
        
        IList<string> smallBladeVerbs = new List<string>() { "stabs", "slices" };
        IList<string> largeBladeVerbs = new List<string>() { "slices", "crushes", "chops", "carves into", "hacks", "impales" };

        IDictionary<string, Weapon> masterList;

        IDictionary<string, Weapon> bladed;
        IDictionary<string, Weapon> blunt;
        IDictionary<string, Weapon> sideArms;
        IDictionary<string, Weapon> rifles;
        //explosives, rocketlaunchers, etc... 

        public MasterWeaponsList()
        {
            masterList = new Dictionary<string, Weapon>();
            GenerateSideArms();
            GenerateRifles();
            GenerateBladed();
            GenerateBlunt();
        }

        private void GenerateSideArms()
        {
            sideArms = new Dictionary<string, Weapon>();
            sideArms.Add("Pistol", new Weapon("Pistol", 3, 6, 5f, 2f, Stats.AIM, weakProjectileSidearmVerbs));
            AddToMaster(sideArms);

        }

        private void GenerateRifles()
        {
            rifles = new Dictionary<string, Weapon>();
            rifles.Add("Hunting Rifle", new Weapon("Hunting Rifle", 10, 4, 5f, 2f, Stats.AIM, weakProjectileRifleVerbs));
            AddToMaster(rifles);
        }

        private void GenerateBladed()
        {
            bladed = new Dictionary<string, Weapon>();
            bladed.Add("Dirk", new Weapon("Dirk", 5, 5, 5f, 2f, Stats.DEXTERITY, smallBladeVerbs));
            AddToMaster(bladed);
        }

        private void GenerateBlunt()
        {
            blunt = new Dictionary<string, Weapon>();
            blunt.Add("Cosh", new Weapon("Cosh", 5, 5, 10f, 1.5f, Stats.STRENGTH, smallBluntVerbs));
            AddToMaster(blunt);
        }

        private void AddToMaster(IDictionary<string, Weapon> chunk)
        {
            foreach (string s in chunk.Keys)
            {
                masterList.Add(s, chunk[s]);
            }
        }

        public Weapon GetWeapon(string name)
        {
            Weapon w = null;
            if (masterList.TryGetValue(name, out w))
            {
                return w;
            }
            return null;
        }

    }
}
