﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skipper.Encounters;
using Skipper.Encounters.Ruins;

namespace Skipper.Data
{
    class WorldData
    {
        //TODO: this has largely been used as a factory.. what else should it do.. hold sectors and other things?
        private Vector2i playerStartLocation;
        private SectorMap playerStartSector;

        // full list of sectors.
        // etc.

        public WorldData()
        {

        }

        // should do something else instead, other than return sectormap.
        // should, eg. populate a grid of SectorMap's.
        public SectorMap CreateSectors()
        {
            //TODO: no constants!
            int sizeX = 30;
            int sizeY = 30;

            Sector[,] grid = new Sector[sizeX, sizeY];
            // add space station somewhere.
            int spaceX = Rand.Instance.NextInt(0, sizeX);
            int spaceY = Rand.Instance.NextInt(0, sizeY);
            //debug
            Console.WriteLine("setup space station");
            AbstractEncounter p = null;
            grid[spaceX, spaceY] = new Sector(new SpaceStationEncounter(p));
            Vector2i playerStart = new Vector2i(spaceX, spaceY);
            playerStartLocation = playerStart;
            Console.WriteLine("setup sectors.");
            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    if (x == spaceX && y == spaceY)
                        continue;

                    // need to use some noise methods etc. to populate nicely.
                    AbstractEncounter enc = null;
                    int r = Rand.Instance.NextInt(0, 100);
                    switch (r)
                    {
                        case 0:
                            // TODO: Move the "this sector", "you enter" etc to their own places..
                            enc = new SystemEncounter("Star System", "This sector contains a star system", null);
                            break;
                        case 5:
                            enc = new AsteroidEncounter("Asteroid", "You enter a field of drifting asteroids", null);
                            break;
                        case 15:
                            enc = new RuinsEntryEncounter();
                            break;
                        case 20:
                        default:
                            enc = null;
                            break;
                    }
                    grid[x, y] = new Sector(enc);
                }
            }
            Console.WriteLine("copying sectors to map.");
            SectorMap sm = new SectorMap(sizeX, sizeY, grid);
            return sm;
        }

        public Vector2i GetPlayerStartLocation()
        {
            return playerStartLocation;
        }


        // deprecate unless ttesting! //
        public IList<AbstractEncounter> PopulateEncountersTable()
        {
            var encTable = new List<AbstractEncounter>();
            encTable.Add(new NavigationEncounter());
            encTable.Add(new SpaceStationEncounter());
            encTable.Add(new SystemEncounter("Star System", "This sectore contains a star system", null));
            encTable.Add(new AsteroidEncounter("Asteroid", "You enter a field of drifting asteroids", null));
            encTable.Add(new SystemEncounter("Star System", "This sectore contains a star system", null));
            encTable.Add(new BasicEncounter("Myserious Object", "This sector contains a very mysterious object.", "It's too mysterious, continuing on...", null));
      /*      // hardcode nasty shit //
            int count = 0;
            // 10 chances empty
            for (int i = 0; i < 5; i++)
                encTable.Add(new SpaceStationEncounter(null));

            for (int i = 0; i < 2; i++)
                encTable.Add(new BasicEncounter("Empty Space", "There is nothing here but space dust", "You cruise peacefully through the empty sector.", null));

            for (int i = 0; i < 1; i++)
                encTable.Add(new BasicEncounter("Pirates!", "Whoop! Whoop! Sirens!", "Do a pirate encounter here!!", null));

            for (int i = 0; i < 5; i++)
                encTable.Add(new AsteroidEncounter("Asteroid Field", "You come across a field of rocky asteroids", null));

            for (int i = 0; i < 5; i++)
                encTable.Add(new SystemEncounter("Star System", "This sector contains a star system.", null));

            for (int i = 0; i < 1; i++)
                encTable.Add(new BasicEncounter("Gas Cloud", "This sector contains a gas cloud. May be some resources.", "You could buy a cloud harvester?", null));

            for (int i = 0; i < 1; i++)
                encTable.Add(new BasicEncounter("Myserious Object", "This sector contains a very mysterious object.", "It's too mysterious, continuing on...", null));
            */
            return encTable;
        }
    }
}
